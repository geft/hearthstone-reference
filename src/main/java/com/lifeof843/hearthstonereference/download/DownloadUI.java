package com.lifeof843.hearthstonereference.download;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Messenger;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;
import com.lifeof843.hearthstonereference.GlobalState;
import com.lifeof843.hearthstonereference.R;

import java.io.IOException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DownloadUI extends Activity implements IDownloaderClient {
    private IStub downloadStub;

    private TextView textStatus;
    private TextView textSpeed;
    private TextView textTime;
    private TextView textPercentage;
    private TextView textProgress;

    private ProgressBar progressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_ui);

        createStub();
        assignViews();
        setButtonListener();
    }

    private void assignViews() {
        textStatus = (TextView) findViewById(R.id.textStatus);
        textSpeed = (TextView) findViewById(R.id.textSpeed);
        textTime = (TextView) findViewById(R.id.textTime);
        textPercentage = (TextView) findViewById(R.id.textPercentage);
        textProgress = (TextView) findViewById(R.id.textProgress);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void createStub() {
        downloadStub = DownloaderClientMarshaller.CreateStub(this, DownloaderServiceHelper.class);
    }

    @Override
    protected void onResume() {
        downloadStub.connect(this);
        super.onResume();
    }

    @Override
    protected void onStop() {
        downloadStub.disconnect(this);
        super.onStop();
    }

    @Override
    public void onServiceConnected(Messenger m) {
        IDownloaderService downloadService = DownloaderServiceMarshaller.CreateProxy(m);
        downloadService.onClientUpdated(downloadStub.getMessenger());
    }

    @Override
    public void onDownloadStateChanged(int newState) {
        if (newState == STATE_COMPLETED) {
            GlobalState.setZipResourceFile(initZipResourceFile());
            finish();
        }

        textStatus.setText(getStringFromNewState(newState));
    }

    private ZipResourceFile initZipResourceFile() {
        ZipResourceFile zipResourceFile = null;
        try {
            zipResourceFile = APKExpansionSupport.getAPKExpansionZipFile(
                    getApplicationContext(),
                    APKExpansionHelper.EXT_VERSION_MAIN,
                    APKExpansionHelper.EXT_VERSION_PATCH);
        } catch (IOException e) {
            Log.e(this.toString(), "Expansion file not found");
        }

        return zipResourceFile;
    }

    private String getStringFromNewState(int newState) {
        int stringResource = Helpers.getDownloaderStringResourceIDFromState(newState);
        return getResources().getString(stringResource);
    }

    private void setButtonListener() {
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
        textSpeed.setText(getSpeedString(progress));
        textPercentage.setText(getPercentageString(progress));
        textProgress.setText(getProgressString(progress));
        textTime.setText(getTimeString(progress));
        updateProgressBar(progress);
    }

    private void updateProgressBar(DownloadProgressInfo progress) {
        progressBar.setMax((int) progress.mOverallTotal);
        progressBar.setProgress((int) progress.mOverallProgress);
    }

    private String getTimeString(DownloadProgressInfo progress) {
        String time = Helpers.getTimeRemaining(progress.mTimeRemaining);
        return "Time Remaining: " + time;
    }

    private String getSpeedString(DownloadProgressInfo progress) {
        return "Speed: " + Helpers.getSpeedString(progress.mCurrentSpeed / 1000) + " MB/s";
    }

    private String getPercentageString(DownloadProgressInfo progress) {
        return Helpers.getDownloadProgressPercent(
                progress.mOverallProgress, progress.mOverallTotal);
    }

    private String getProgressString(DownloadProgressInfo progress) {
        return Helpers.getDownloadProgressString(
                progress.mOverallProgress, progress.mOverallTotal);
    }
}
