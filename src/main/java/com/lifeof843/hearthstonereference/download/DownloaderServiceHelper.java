package com.lifeof843.hearthstonereference.download;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

public class DownloaderServiceHelper extends DownloaderService {

    public static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzI" +
            "Ip2yvIVzC564h0HgoScsvWRcuzC34hIqdukm57e5CwlUVid/Vwspn3EYORMm2cGByrpejmoPo+73ZArO3kEbt" +
            "e0MTf45TGEzuhy2YtlBJZPz0ydlKk6YTHhILe0ZjCLpfLnQLR/bi4cYTbIwpkJxnIV/IxJO1Lw8x5dsOhL2ae" +
            "Nt66lrXopWxJV4thr2d0MQG0GWzD5Jl9MxbB/elCHOhzhMcDjN7QvKKYyD6T2N7sWLzcH6UEyN9COldmL/Oun" +
            "DG0WrXEn173lJrvVjI2qr/CdBHajF8D3X28nm94ou6TERrsod+Bh3wQUYLS2avzqs7bFOhHwFrgdFiRBTe5fw" +
            "IDAQAB";

    public static final byte[] SALT = new byte[]{55, 42, -12, -11, 54, 98,
            -100, -12, 43, 20, -81, -4, 9, 5, -56, -107, -12, 45, -50, 84
    };

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return DownloadAlarm.class.getName();
    }
}
