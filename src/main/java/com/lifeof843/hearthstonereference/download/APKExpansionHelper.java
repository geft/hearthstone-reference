package com.lifeof843.hearthstonereference.download;

import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;
import java.util.List;

public class APKExpansionHelper {

    public static final int EXT_VERSION_MAIN = 55;
    public static final int EXT_VERSION_PATCH = 46;
    private static final long EXT_SIZE_MAIN = 338430021L;
    private static final long EXT_SIZE_PATCH = 51822566L;

    private Context context;
    private PendingIntent pendingIntent;
    private List<XAPKFile> extFiles;

    public APKExpansionHelper(Context context, PendingIntent pendingIntent) {
        this.context = context;
        this.pendingIntent = pendingIntent;

        initXAPKS();
        checkExpansions();
    }

    private void initXAPKS() {
        XAPKFile main = new XAPKFile(true, EXT_VERSION_MAIN, EXT_SIZE_MAIN);
        XAPKFile patch = new XAPKFile(false, EXT_VERSION_PATCH, EXT_SIZE_PATCH);

        extFiles = new ArrayList<>();
        extFiles.add(main);
//        extFiles.add(patch);
    }

    private void checkExpansions() {
        if (!expansionFilesDelivered()) {
            if (isDownloadRequired(getStartDownloadResult())) {
                createNotifyToast();
            }
        }
    }

    private void createNotifyToast() {
        Toast.makeText(context,
                context.getString(R.string.apk_expansion_helper_download_required),
                Toast.LENGTH_SHORT)
                .show();
    }

    private boolean isDownloadRequired(int startResult) {
        return startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED;
    }

    private int getStartDownloadResult() {
        int startResult = 0;

        try {
            startResult = DownloaderClientMarshaller.startDownloadServiceIfRequired(
                    context.getApplicationContext(), pendingIntent, DownloaderServiceHelper.class);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return startResult;
    }

    private boolean expansionFilesDelivered() {
        for (XAPKFile file : extFiles) {
            String fileName = Helpers.getExpansionAPKFileName(context, file.mIsMain, file.mFileVersion);
            if (!Helpers.doesFileExist(context, fileName, file.mFileSize, true))
                return false;
        }
        return true;
    }

    private static class XAPKFile {
        public final boolean mIsMain;
        public final int mFileVersion;
        public final long mFileSize;

        XAPKFile(boolean isMain, int fileVersion, long fileSize) {
            mIsMain = isMain;
            mFileVersion = fileVersion;
            mFileSize = fileSize;
        }
    }
}
