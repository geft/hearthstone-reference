package com.lifeof843.hearthstonereference.viewer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.browser.CardInfoDialog;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.utility.FileManager;

public class CardViewerPageFragment extends Fragment {

    public static final String CARD_NAME = "cardName";

    private View view;
    private FileManager fileManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(
                R.layout.activity_card_viewer, container, false);
        this.fileManager = new FileManager(view.getContext());

        if (getArguments() != null) {
            initialize();
        }

        return view;
    }

    private void initialize() {
        String cardName = getArguments().getString(CARD_NAME);
        initClickableCardImage(cardName);
        displayCardInfo(cardName);
        displayRelatedCards(cardName);
    }

    private void displayRelatedCards(String cardName) {
        new RelatedViewer(getActivity(), cardName, view).displayRelatedImages();
    }

    private void initClickableCardImage(final String cardName) {
        ImageView imageView = getImageView();

        imageView.setOnClickListener(v -> {
            Intent intent = new Intent(view.getContext(), MediaViewer.class);
            intent.putExtra(CARD_NAME, cardName);
            startActivity(intent);
        });

        imageView.setOnLongClickListener(v -> {
            Intent intent = new Intent(getActivity(), CardInfoDialog.class);
            intent.putExtra(CARD_NAME, cardName);
            startActivity(intent);
            return true;
        });
    }

    private ImageView getImageView() {
        return (ImageView) view.findViewById(R.id.imageView);
    }

    private void displayCardInfo(String cardName) {
        CardDatabase cardDatabase = CardDatabase.getInstance(view.getContext());

        loadCardImage(cardName);
        loadFlavor(cardDatabase.getProperty(cardName, Card.PROP.FLAVOR));
        loadArtist(cardDatabase.getProperty(cardName, Card.PROP.ARTIST));
    }

    private void loadCardImage(String cardName) {
        Drawable cardImage = fileManager.getDrawableFromName(cardName, true);

        if (cardImage == null) {
            displayLoadError();
        }

        ImageView imageView = getImageView();
        imageView.setImageDrawable(cardImage);
    }

    private void loadFlavor(String flavor) {
        TextView textView = (TextView) view.findViewById(R.id.textFlavor);
        textView.setText(flavor);
    }

    private void loadArtist(String artist) {
        TextView textView = (TextView) view.findViewById(R.id.textArtist);

        if (textView != null) {
            textView.setText(artist);
        }
    }

    private void displayLoadError() {
        Toast.makeText(
                getActivity().getApplicationContext(),
                getString(R.string.failed_to_load_image),
                Toast.LENGTH_SHORT)
                .show();
    }
}
