package com.lifeof843.hearthstonereference.viewer;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CardViewer extends FragmentActivity {

    private int numPages;
    private ArrayList<String> cardList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_viewer_slider);

        initialize();
    }

    private void initialize() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            cardList = getIntent().getExtras().getStringArrayList("cardList");
        }

        if (cardList != null) {
            numPages = cardList.size();
        } else numPages = 0;

        setPagerAdapter();
    }

    private void setPagerAdapter() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter mAdapter = new CardViewerPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);

        setPagerPosition(viewPager);
    }

    private void setPagerPosition(ViewPager pager) {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            pager.setCurrentItem(extras.getInt("position"));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    private String getCardNameFromPosition(int position) {
        return cardList.get(position);
    }

    private class CardViewerPagerAdapter extends FragmentStatePagerAdapter {
        public CardViewerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            CardViewerPageFragment page = new CardViewerPageFragment();

            // send position data
            Bundle bundle = new Bundle();
            bundle.putString("cardName", getCardNameFromPosition(position));
            page.setArguments(bundle);

            return page;
        }

        @Override
        public int getCount() {
            return numPages;
        }
    }
}
