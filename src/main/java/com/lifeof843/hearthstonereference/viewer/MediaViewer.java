package com.lifeof843.hearthstonereference.viewer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.crashlytics.android.Crashlytics;
import com.lifeof843.hearthstonereference.GlobalState;
import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.Utils;
import com.lifeof843.hearthstonereference.cards.CardSound;
import com.lifeof843.hearthstonereference.utility.FileManager;
import com.lifeof843.hearthstonereference.utility.FormatManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MediaViewer extends Activity {

    private final String AUTHORITY = "com.lifeof843.hearthstonereference.utility.ZipFileContentProvider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    private FileManager fileManager;
    private String cardName;
    private List<AssetFileDescriptor> descriptors;
    private
    @NonNull
    List<MediaPlayer> sounds = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_viewer);

        init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            cardName = getIntent().getStringExtra("cardName");
        } else {
            cardName = savedInstanceState.getString("cardName");
        }

        fileManager = new FileManager(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        addAudioButtons();
        addCardAnimation();
    }

    private void addAudioButtons() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutSound);
        layout.removeAllViews();
        List<CardSound> list = fileManager.getAudioList(cardName);
        descriptors = new ArrayList<>();

        if (!list.isEmpty()) {
            for (final CardSound cardSound : list) {
                descriptors.add(cardSound.getDescriptor());

                ImageButton button = new ImageButton(this);
                button.setOnClickListener(v -> {
                    pauseSounds();
                    playSound(cardSound);
                });
                button.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_crystal_on));
                button.setBackgroundColor(Color.TRANSPARENT);
                button.setOnLongClickListener(v -> {
                    startSetRingtoneDialog(cardSound);
                    return true;
                });
                layout.addView(button);

                Crashlytics.log("Add audio: " + cardSound.getPath());
            }
        }
    }

    private void startSetRingtoneDialog(CardSound cardSound) {
        String filePath = cardSound.getPath();
        Intent intent = new Intent(this, SetRingtoneDialog.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable("uri", getURI(filePath));
        bundle.putString("id", cardName);
        bundle.putLong("length", cardSound.getDescriptor().getLength());
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void playSound(CardSound cardSound) {
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mp.setDataSource(
                    cardSound.getDescriptor().getFileDescriptor(),
                    cardSound.getDescriptor().getStartOffset(),
                    cardSound.getDescriptor().getLength());

            mp.prepare();
            sounds.add(mp);
        } catch (IOException e) {
            Log.e(this.toString(),
                    "Error starting " + cardSound.getPath());
            e.printStackTrace();
        }
    }

    private void pauseSounds() {
        for (MediaPlayer mp : sounds) {
            try {
                if (mp.isPlaying()) {
                    mp.pause();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    private void addCardAnimation() {
        String formattedCardName = (cardName == null) ? "" : FormatManager.formatCardName(cardName);
        String filePath = "card_gold/" + formattedCardName + ".mp4";

        if (doesAnimationExist(filePath)) {
            Uri uri = getURI(filePath);
            playAnimation(uri);
        } else {
            Utils.showToast(this, getString(R.string.animation_does_not_exist));
            finish();
        }
    }

    private boolean doesAnimationExist(String filePath) {
        ZipResourceFile zipResourceFile = GlobalState.getZipResourceFile();
        AssetFileDescriptor afd = null;

        if (zipResourceFile != null) {
            afd = zipResourceFile.getAssetFileDescriptor(filePath);
        }

        return afd != null;
    }

    private Uri getURI(String filePath) {
        return Uri.parse(CONTENT_URI + File.separator + filePath);
    }

    private void playAnimation(final Uri uri) {
        final VideoView videoView = getAnimationView();
        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);

                try {
                    mp.start();
                } catch (RuntimeException e) {
                    Fabric.getLogger().e(this.getClass().getName(), uri.toString());
                    e.printStackTrace();
                    Utils.showToast(getBaseContext(), "Error playing media");
                }
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
            }
        });
        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return true;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("cardName", cardName);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        pauseAnimation();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeAnimation();
    }

    @Override
    protected void onStop() {
        releaseAnimation();
        closeDescriptors();
        super.onStop();
    }

    private void pauseAnimation() {
        VideoView videoView = getAnimationView();
        if (videoView != null) {
            videoView.pause();
        }
    }

    private void resumeAnimation() {
        VideoView videoView = getAnimationView();
        if (videoView != null) {
            videoView.resume();
        }
    }

    private void releaseAnimation() {
        if (getAnimationView().isPlaying()) {
            getAnimationView().stopPlayback();
        }
    }

    private VideoView getAnimationView() {
        return (VideoView) findViewById(R.id.videoView);
    }

    private void closeDescriptors() {
        for (AssetFileDescriptor descriptor : descriptors) {
            try {
                descriptor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
