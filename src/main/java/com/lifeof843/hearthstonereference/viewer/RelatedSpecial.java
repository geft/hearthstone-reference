package com.lifeof843.hearthstonereference.viewer;

import android.content.Context;

import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.viewer.list.ListJadeGolem;
import com.lifeof843.hearthstonereference.viewer.list.ListKazukus;
import com.lifeof843.hearthstonereference.viewer.list.ListRafaam;
import com.lifeof843.hearthstonereference.viewer.list.ListSparePart;

import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class RelatedSpecial {
    private static final String SPARE_PART = "Spare Part";
    private static final String JADE_GOLEM = "Jade Golem";
    private static final String RAFAAM = "Arch-Thief Rafaam";
    private static final String KAZAKUS = "Kazakus";

    private CardDatabase database;
    private String cardName;
    private List<CardInfo> relatedList;

    public RelatedSpecial(Context context, String cardName, List<CardInfo> relatedList) {
        this.database = CardDatabase.getInstance(context);
        this.cardName = cardName;
        this.relatedList = relatedList;
    }

    public void addSpecial() {
        if (useSpareParts()) addToRelatedList(new ListSparePart().getList());
        if (useJadeGolem()) addToRelatedList(new ListJadeGolem().getList());
        if (isRafaam()) addToRelatedList(new ListRafaam().getList());
        if (isKazukus()) addToRelatedList(new ListKazukus().getList());
    }

    private boolean isRafaam() {
        return matchName(RAFAAM);
    }

    private boolean isKazukus() {
        return matchName(KAZAKUS);
    }

    private boolean useSpareParts() {
        return matchText(SPARE_PART);
    }

    private boolean useJadeGolem() {
        return matchText(JADE_GOLEM);
    }

    private boolean matchName(String matcher) {
        return cardName.contains(matcher);
    }

    private boolean matchText(String matcher) {
        String text = database.getProperty(cardName, Card.PROP.TEXT);
        return text.contains(matcher);
    }

    private void addToRelatedList(List<String> cards) {
        for (String card : cards) {
            CardInfo cardInfo = new CardInfo();
            cardInfo.name = card;

            relatedList.add(cardInfo);
        }
    }
}
