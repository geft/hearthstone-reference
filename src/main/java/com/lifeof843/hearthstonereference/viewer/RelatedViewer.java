package com.lifeof843.hearthstonereference.viewer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.utility.FileManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RelatedViewer {
    private Context context;
    private String cardName;
    private View view;

    RelatedViewer(Context context, String cardName, View view) {
        this.context = context;
        this.cardName = cardName;
        this.view = view;
    }

    void displayRelatedImages() {
        LinearLayout layout = getImageLayout();
        FileManager fileManager = new FileManager(context);

        getRelatedCardList()
                .flatMap(cardInfos -> Observable.from(cardInfos)
                        .map(cardInfo -> {
                            Drawable drawable = fileManager.getDrawableFromName(cardInfo.name, true);
                            ImageView imageView = getRelatedImageView(drawable);
                            imageView.setOnClickListener(v -> startMediaViewer(cardInfo));

                            return imageView;
                        })
                )
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(layout::addView);
    }

    private void startMediaViewer(CardInfo cardInfo) {
        Intent intent = new Intent(context, MediaViewer.class);
        intent.putExtra("cardName", cardInfo.name);
        context.startActivity(intent);
    }

    private Observable<List<CardInfo>> getRelatedCardList() {
        HashMap<String, CardInfo> hashMap = new RelatedHashMap().getHashMap();
        final List<CardInfo> relatedList = new ArrayList<>();

        return Observable.from(hashMap.entrySet())
                .map(entry -> {
                    RelatedAdder adder = new RelatedAdder();
                    adder.setCardName(cardName);
                    adder.setKey(entry.getKey());
                    adder.setValue(entry.getValue());
                    adder.addToRelatedList(relatedList);

                    return adder;
                })
                .toList()
                .map(relatedAdders -> {
                    new RelatedSpecial(context, cardName, relatedList).addSpecial();
                    return sortRelatedList(relatedList);
                });
    }

    private List<CardInfo> sortRelatedList(List<CardInfo> list) {
        Collections.sort(list, (lhs, rhs) -> lhs.name.compareToIgnoreCase(rhs.name));
        return list;
    }

    private LinearLayout getImageLayout() {
        return (LinearLayout) view.findViewById(R.id.imageLayout);
    }

    private ImageView getRelatedImageView(Drawable drawable) {
        ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(drawable);
        imageView.setLayoutParams(getRelatedImageViewLayout());
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setAdjustViewBounds(true);
        return imageView;
    }

    private LinearLayout.LayoutParams getRelatedImageViewLayout() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 20);
        return layoutParams;
    }
}
