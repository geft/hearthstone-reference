package com.lifeof843.hearthstonereference.viewer;

import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class RelatedAdder {

    private String cardName;
    private String key;
    private CardInfo value;

    private boolean doesKeyMatchCardName() {
        boolean equals = getKey().equals(getCardName());
        boolean startsWith = getKey().startsWith(getCardName());
        boolean lengthPlusOne = getKey().length() == getCardName().length() + 1;

        return equals || (startsWith && lengthPlusOne);
    }

    List<CardInfo> addToRelatedList(List<CardInfo> relatedList) {
        if (doesKeyMatchCardName()) {
            relatedList.add(getValue());
        }

        return relatedList;
    }

    String getCardName() {
        return cardName;
    }

    void setCardName(String cardName) {
        this.cardName = cardName;
    }

    String getKey() {
        return key;
    }

    void setKey(String key) {
        this.key = key;
    }

    CardInfo getValue() {
        return value;
    }

    void setValue(CardInfo value) {
        this.value = value;
    }
}
