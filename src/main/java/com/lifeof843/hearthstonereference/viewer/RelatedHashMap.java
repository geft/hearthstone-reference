package com.lifeof843.hearthstonereference.viewer;

import java.util.HashMap;

class RelatedHashMap {

    HashMap<String, CardInfo> getHashMap() {
        return assignHashMap();
    }

    private void addToMap(HashMap<String, CardInfo> map, String key, String name) {
        CardInfo cardInfo = new CardInfo();
        cardInfo.name = name;

        map.put(key, cardInfo);
    }

    private HashMap<String, CardInfo> assignHashMap() {
        HashMap<String, CardInfo> map = new HashMap<>();

        // Free
        addToMap(map, "Animal Companion1", "Huffer");
        addToMap(map, "Animal Companion2", "Leokk");
        addToMap(map, "Animal Companion3", "Misha");
        addToMap(map, "Dragonling Mechanic", "Mechanical Dragonling");
        addToMap(map, "Hex", "Frog");
        addToMap(map, "Mirror Image", "Mirror Image1");
        addToMap(map, "Murloc Tidehunter", "Murloc Scout");
        addToMap(map, "Polymorph", "Sheep");
        addToMap(map, "Razorfen Hunter", "Boar");
        addToMap(map, "Wild Growth", "Excess Mana");

        // Common
        addToMap(map, "Alleycat", "Tabbycat");
        addToMap(map, "Anodized Robo Cub1", "Attack Mode");
        addToMap(map, "Anodized Robo Cub2", "Tank Mode");
        addToMap(map, "Arathi Weaponsmith", "Battle Axe");
        addToMap(map, "Arcanosmith", "Animated Shield");
        addToMap(map, "Bear Trap", "Ironfur Grizzly");
        addToMap(map, "Big Time Racketeer", "Little Friend");
        addToMap(map, "Bilefin Tidehunter", "Ooze");
        addToMap(map, "Curse of Rafaam", "Cursed!");
        addToMap(map, "Deadly Fork", "Sharp Fork");
        addToMap(map, "Defias Ringleader", "Defias Bandit");
        addToMap(map, "Druid of the Claw1", "Bear Form");
        addToMap(map, "Druid of the Claw2", "Cat Form");
        addToMap(map, "Druid of the Claw3", "Druid of the Claw1");
        addToMap(map, "Druid of the Claw4", "Druid of the Claw2");
        addToMap(map, "Druid of the Fang", "Druid of the Fang1");
        addToMap(map, "Druid of the Flame1", "Druid of the Flame1");
        addToMap(map, "Druid of the Flame2", "Druid of the Flame2");
        addToMap(map, "Druid of the Flame3", "Firecat Form");
        addToMap(map, "Druid of the Flame4", "Fire Hawk Form");
        addToMap(map, "Druid of the Saber1", "Lion Form");
        addToMap(map, "Druid of the Saber2", "Panther Form");
        addToMap(map, "Druid of the Saber3", "Sabertooth Lion");
        addToMap(map, "Druid of the Saber4", "Sabertooth Panther");
        addToMap(map, "Feral Rage1", "Evolve Scales");
        addToMap(map, "Feral Rage2", "Evolve Spines");
        addToMap(map, "Harvest Golem", "Damaged Golem");
        addToMap(map, "Haunted Creeper", "Spectral Spider");
        addToMap(map, "Imp Gang Boss", "Imp");
        addToMap(map, "Infested Tauren", "Slime1");
        addToMap(map, "Kabal Chemist1", "Potion of Madness");
        addToMap(map, "Kabal Chemist2", "Pint-Size Potion");
        addToMap(map, "Kabal Chemist3", "Dragonfire Potion");
        addToMap(map, "Kabal Chemist4", "Volcanic Potion");
        addToMap(map, "Kabal Chemist5", "Potion of Polymorph");
        addToMap(map, "Kabal Chemist6", "Bloodfury Potion");
        addToMap(map, "Kabal Chemist7", "Felfire Potion");
        addToMap(map, "Kara Kazham!1", "Candle");
        addToMap(map, "Kara Kazham!2", "Broom");
        addToMap(map, "Kara Kazham!3", "Teapot");
        addToMap(map, "Kindly Grandmother", "Big Bad Wolf");
        addToMap(map, "Living Roots1", "Living Roots1");
        addToMap(map, "Living Roots2", "Living Roots2");
        addToMap(map, "Living Roots3", "Sapling");
        addToMap(map, "Mark of Nature1", "Mark of Nature1");
        addToMap(map, "Mark of Nature2", "Mark of Nature2");
        addToMap(map, "N'Zoth's First Mate", "Rusty Hook");
        addToMap(map, "Nightbane Templar", "Whelp4");
        addToMap(map, "Noble Sacrifice", "Defender");
        addToMap(map, "On the Hunt", "Mastiff");
        addToMap(map, "Pantry Spider", "Cellar Spider");
        addToMap(map, "Poison Seeds", "Treant1");
        addToMap(map, "Possessed Village", "Shadowbeast");
        addToMap(map, "Power of the Wild1", "Leader of the Pack");
        addToMap(map, "Power of the Wild2", "Panther");
        addToMap(map, "Power of the Wild3", "Summon a Panther");
        addToMap(map, "Sense Demons", "Worthless Imp");
        addToMap(map, "Silver Hand Knight", "Squire");
        addToMap(map, "Stand Against Darkness", "Silver Hand Recruit");
        addToMap(map, "Soul of the Forest", "Treant1");
        addToMap(map, "Unleash the Hounds", "Hound");
        addToMap(map, "Wrath1", "Wrath1");
        addToMap(map, "Wrath2", "Wrath2");

        // Rare
        addToMap(map, "Ball of Spiders", "Webspinner");
        addToMap(map, "Blood to Ichor", "Slime4");
        addToMap(map, "Cat Trick", "Cat in a Hat");
        addToMap(map, "Cutpurse", "The Coin");
        addToMap(map, "Dragon Egg", "Black Whelp");
        addToMap(map, "Feral Spirit", "Spirit Wolf");
        addToMap(map, "Forbidden Ritual", "Icky Tentacle");
        addToMap(map, "Gnomish Experimenter", "Chicken");
        addToMap(map, "Grove Tender1", "Gift of Mana");
        addToMap(map, "Grove Tender2", "Gift of Cards");
        addToMap(map, "Imp Master", "Imp");
        addToMap(map, "Imp-losion", "Imp1");
        addToMap(map, "Infested Wolf", "Spider");
        addToMap(map, "Jade Idol1", "Jade Idol1");
        addToMap(map, "Jade Idol2", "Jade Idol2");
        addToMap(map, "Keeper of the Grove1", "Moonfire1");
        addToMap(map, "Keeper of the Grove2", "Dispel");
        addToMap(map, "Light of the Naaru", "Lightwarden");
        addToMap(map, "Mire Keeper1", "Y'Shaarj's Strength");
        addToMap(map, "Mire Keeper2", "Yogg-Saron's Magic");
        addToMap(map, "Mire Keeper3", "Slime2");
        addToMap(map, "Muster for Battle1", "Sliver Hand Recruit");
        addToMap(map, "Muster for Battle2", "Light's Justice");
        addToMap(map, "Nerubian Egg", "Nerubian");
        addToMap(map, "Nourish1", "Nourish1");
        addToMap(map, "Nourish2", "Nourish2");
        addToMap(map, "Polymorph: Boar", "Boar");
        addToMap(map, "Potion of Polymorph", "Sheep1");
        addToMap(map, "Protect the King!", "Pawn");
        addToMap(map, "Savannah Highmane", "Hyena");
        addToMap(map, "Sludge Belcher", "Slime");
        addToMap(map, "Starfall1", "Starfall1");
        addToMap(map, "Starfall2", "Starfall2");
        addToMap(map, "Upgrade!", "Heavy Axe");
        addToMap(map, "Violet Teacher", "Violet Apprentice");

        // Epic
        addToMap(map, "Ancient of Lore2", "Ancient Secrets");
        addToMap(map, "Ancient of Lore1", "Ancient Teachings");
        addToMap(map, "Ancient of War1", "Rooted");
        addToMap(map, "Ancient of War2", "Uproot");
        addToMap(map, "Beneath the Grounds1", "Ambush!");
        addToMap(map, "Beneath the Grounds2", "Nerubian");
        addToMap(map, "Blood of the Ancient One", "The Ancient One");
        addToMap(map, "Call of the Wild1", "Misha");
        addToMap(map, "Call of the Wild2", "Leokk");
        addToMap(map, "Call of the Wild3", "Huffer");
        addToMap(map, "Charged Hammer", "Lightning Jolt");
        addToMap(map, "Dark Wispers1", "Dark Wispers1");
        addToMap(map, "Dark Wispers2", "Dark Wispers2");
        addToMap(map, "Dark Wispers3", "Wisp");
        addToMap(map, "Force of Nature", "Treant1");
        addToMap(map, "Hammer of Twilight", "Twilight Elemental");
        addToMap(map, "Kodorider", "War Kodo");
        addToMap(map, "Mana Geode", "Crystal");
        addToMap(map, "Mindgames", "Shadow of Nothing");
        addToMap(map, "Piranha Launcher", "Piranha");
        addToMap(map, "Recruiter", "Squire");
        addToMap(map, "Quartermaster", "Silver Hand Recruit");
        addToMap(map, "Rat Pack", "Rat");
        addToMap(map, "Snake Trap", "Snake");
        addToMap(map, "Spellbender", "Spellbender1");
        addToMap(map, "Twilight Summoner", "Faceless Destroyer");
        addToMap(map, "Vilefin Inquisitor", "The Tidal Hand");
        addToMap(map, "Wisp of the Old Gods1", "Many Wisps");
        addToMap(map, "Wisp of the Old Gods2", "Big Wisps");
        addToMap(map, "Wisp of the Old Gods3", "Wisp2");

        // Legendary
        addToMap(map, "Anub'arak", "Nerubian");
        addToMap(map, "Archmage Antonidas", "Fireball");
        addToMap(map, "Cairne Bloodhoof", "Baine Bloodhoof");
        addToMap(map, "Cenarius1", "Demigod's Favor");
        addToMap(map, "Cenarius2", "Shan'do's Lesson");
        addToMap(map, "Cenarius3", "Treant3");
        addToMap(map, "Dr. Boom", "Boom Bot");
        addToMap(map, "Elise Starseeker1", "Map to the Golden Monkey");
        addToMap(map, "Elise Starseeker2", "Golden Monkey");
        addToMap(map, "Elite Tauren Chieftain1", "Murloc");
        addToMap(map, "Elite Tauren Chieftain2", "I Am Murloc");
        addToMap(map, "Elite Tauren Chieftain3", "Power of the Horde");
        addToMap(map, "Elite Tauren Chieftain4", "Rogues Do It...");
        addToMap(map, "Fandral Staghelm1", "Druid of the Claw3");
        addToMap(map, "Fandral Staghelm2", "Druid of the Flame3");
        addToMap(map, "Fandral Staghelm3", "Tiger Form");
        addToMap(map, "Feugen1", "Stalagg");
        addToMap(map, "Feugen2", "Thaddius");
        addToMap(map, "Gelbin Mekkatorque1", "Chicken1");
        addToMap(map, "Gelbin Mekkatorque2", "Emboldener 3000");
        addToMap(map, "Gelbin Mekkatorque3", "Homing Chicken");
        addToMap(map, "Gelbin Mekkatorque4", "Poultryizer");
        addToMap(map, "Gelbin Mekkatorque5", "Repair Bot");
        addToMap(map, "Hogger", "Gnoll");
        addToMap(map, "Hogger, Doom of Elwynn", "Gnoll");
        addToMap(map, "Illidan Stormrage", "Flame of Azzinoth");
        addToMap(map, "Iron Juggernaut", "Burrowing Mine");
        addToMap(map, "Justicar Trueheart1", "Ballista Shot");
        addToMap(map, "Justicar Trueheart2", "Dire Shapeshift");
        addToMap(map, "Justicar Trueheart3", "Fireblast Rank 2");
        addToMap(map, "Justicar Trueheart4", "Heal");
        addToMap(map, "Justicar Trueheart5", "Poisoned Daggers");
        addToMap(map, "Justicar Trueheart6", "Soul Tap");
        addToMap(map, "Justicar Trueheart7", "Tank Up");
        addToMap(map, "Justicar Trueheart8", "The Silver Hand");
        addToMap(map, "Justicar Trueheart9", "Totemic Slam");
        addToMap(map, "King Mukla", "Bananas1");
        addToMap(map, "Kun the Forgotten King1", "Forgotten Armor");
        addToMap(map, "Kun the Forgotten King2", "Forgotten Mana");
        addToMap(map, "Mukla, Tyrant of the Vale", "Bananas1");
        addToMap(map, "Leeroy Jenkins", "Whelp");
        addToMap(map, "Lord Jaraxxus1", "Blood-fury");
        addToMap(map, "Lord Jaraxxus2", "Infernal");
        addToMap(map, "Lord Jaraxxus3", "INFERNO!");
        addToMap(map, "Lord Jaraxxus4", "Lord Jaraxxus1");
        addToMap(map, "Majordomo Executus1", "Ragnaros, the Firelord");
        addToMap(map, "Majordomo Executus2", "Ragnaros, the Firelord1");
        addToMap(map, "Majordomo Executus3", "DIE, INSECT!");
        addToMap(map, "Medivh, the Guardian", "Atiesh");
        addToMap(map, "Mekgineer Thermaplugg", "Leper Gnome");
        addToMap(map, "Mimiron's Head", "V-07-TR-0N");
        addToMap(map, "Moroes", "Steward");
        addToMap(map, "Onyxia", "Whelp");
        addToMap(map, "Rhonin", "Arcane Missiles");
        addToMap(map, "Stalagg1", "Feugen");
        addToMap(map, "Stalagg2", "Thaddius");
        addToMap(map, "The Beast", "Finkle Einhorn");
        addToMap(map, "Tinkmaster Overspark1", "Devilsaur");
        addToMap(map, "Tinkmaster Overspark2", "Squirrel");
        addToMap(map, "Tirion Fordring", "Ashbringer");
        addToMap(map, "Trade Prince Gallywix", "Gallywix's Coin");
        addToMap(map, "Troggzor the Earthinator", "Burly Rockjaw Trogg");
        addToMap(map, "Twin Emperor Vek'lor", "Twin Emperor Vek'nilash");
        addToMap(map, "White Eyes", "The Storm Guardian");
        addToMap(map, "Xaril, Poisoned Mind1", "Bloodthistle Toxin");
        addToMap(map, "Xaril, Poisoned Mind2", "Briartorn Toxin");
        addToMap(map, "Xaril, Poisoned Mind3", "Fadeleaf Toxin");
        addToMap(map, "Xaril, Poisoned Mind4", "Firebloom Toxin");
        addToMap(map, "Xaril, Poisoned Mind5", "Kingsblood Toxin");
        addToMap(map, "Ysera1", "Dream");
        addToMap(map, "Ysera2", "Emerald Drake");
        addToMap(map, "Ysera3", "Laughing Sister");
        addToMap(map, "Ysera4", "Nightmare");
        addToMap(map, "Ysera5", "Ysera Awakens");

        // C'Thun
        addToMap(map, "Ancient Shieldbearer", "C'Thun");
        addToMap(map, "Blade of C'Thun", "C'Thun");
        addToMap(map, "Beckoner of Evil", "C'Thun");
        addToMap(map, "C'Thun's Chosen", "C'Thun");
        addToMap(map, "Crazed Worshipper", "C'Thun");
        addToMap(map, "Cult Sorcerer", "C'Thun");
        addToMap(map, "Dark Arrakoa", "C'Thun");
        addToMap(map, "Disciple of C'Thun", "C'Thun");
        addToMap(map, "Doomcaller", "C'Thun");
        addToMap(map, "Hooded Acolyte", "C'Thun");
        addToMap(map, "Klaxxi Amber-Weaver", "C'Thun");
        addToMap(map, "Skeram Cultist", "C'Thun");
        addToMap(map, "Twilight Darkmender", "C'Thun");
        addToMap(map, "Twilight Elder", "C'Thun");
        addToMap(map, "Twilight Geomancer", "C'Thun");
        addToMap(map, "Usher of Souls", "C'Thun");

        return map;
    }
}
