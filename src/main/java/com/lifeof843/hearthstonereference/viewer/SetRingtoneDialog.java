package com.lifeof843.hearthstonereference.viewer;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.lifeof843.hearthstonereference.GlobalState;
import com.lifeof843.hearthstonereference.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SetRingtoneDialog extends Activity {

    private Uri uri;
    private String id;
    private long length;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_ringtone_dialog);

        setBackgroundTransparent();
        initialize(savedInstanceState);
        initUI();
    }

    private void setBackgroundTransparent() {
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void initialize(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            uri = savedInstanceState.getParcelable("uri");
            id = savedInstanceState.getString("id");
            length = savedInstanceState.getLong("length");
        } else {
            Bundle bundle = getIntent().getExtras();
            uri = bundle.getParcelable("uri");
            id = bundle.getString("id");
            length = bundle.getLong("length");
        }
    }

    private void initUI() {
        setButton(R.id.buttonRingtone, RingtoneManager.TYPE_RINGTONE);
        setButton(R.id.buttonNotification, RingtoneManager.TYPE_NOTIFICATION);
        setButton(R.id.buttonAlarm, RingtoneManager.TYPE_ALARM);
    }

    private void setButton(int res, final int soundType) {
        Button button = (Button) findViewById(res);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExternalStorageWritable()) {
                    setRingtone(uri, id, length, soundType);
                } else {
                    makeToast("External storage not available");
                }
                finish();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable("uri", uri);
        outState.putString("id", id);
        outState.putLong("length", length);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setRingtone(Uri uri, String id, long length, int soundType) {
        Uri baseUri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        File file = InputStreamWriter(getInputStreamFromUri(uri), uri.getLastPathSegment());
        deleteDataIfExists(baseUri, file);
        ContentValues values = getRingtoneContentValues(file, id, length);
        Uri newUri = getContentResolver().insert(baseUri, values);

        try {
            RingtoneManager.setActualDefaultRingtoneUri(
                    getApplicationContext(), soundType, newUri);
            makeToast("Default notification sound set successfully");
        } catch (Exception e) {
            e.printStackTrace();
            makeToast("Failed to set notification sound");
        }
    }

    private void deleteDataIfExists(Uri baseUri, File file) {
        int deletedRows = getContentResolver().delete(baseUri, MediaStore.MediaColumns.DATA + "=\""
                + file.getAbsolutePath() + "\"", null);

        if (deletedRows > 0) {
            Log.v(this.toString(), file.getAbsolutePath() + "removed from ContentResolver");
        }
    }

    private void makeToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    private ContentValues getRingtoneContentValues(File file, String id, long length) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, id);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/ogg");
        values.put(MediaStore.MediaColumns.SIZE, length);
        values.put(MediaStore.Audio.Media.ARTIST, "Blizzard Entertainment");
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_ALARM, true);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
        return values;
    }

    private InputStream getInputStreamFromUri(Uri uri) {
        String path = "card_sounds/" + uri.getLastPathSegment();
        InputStream inputStream = null;

        try {
            inputStream = GlobalState.getZipResourceFile().getInputStream(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return inputStream;
    }

    private File InputStreamWriter(InputStream input, String name) {
        File file = null;

        try {
            file = new File(getExternalFilesDir(null), name);
            OutputStream output = new FileOutputStream(file);
            final byte[] buffer = new byte[1024];
            int read;

            while ((read = input.read(buffer)) != -1)
                output.write(buffer, 0, read);

            output.flush();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

}
