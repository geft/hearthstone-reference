package com.lifeof843.hearthstonereference.viewer.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class ListRafaam implements RelatedList {
    @Override
    public List<String> getList() {
        List<String> list = new ArrayList<>();

        list.add("Hakkari Blood Goblet");
        list.add("Pit Snake");
        list.add("Medivh's Locket");
        list.add("Unstable Portal");
        list.add("Khadgar's Pipe");
        list.add("Ysera's Tear");
        list.add("Lantern of Power");
        list.add("Timepiece of Horror");
        list.add("Mirror of Doom");
        list.add("Mummy Zombie");
        list.add("Benediction Splinter");
        list.add("Putress' Vial");
        list.add("Lothar's Left Greave");
        list.add("Shard of Sulfuras");
        list.add("Eye of Orsis");
        list.add("Crown of Kael'thas");

        return list;
    }
}
