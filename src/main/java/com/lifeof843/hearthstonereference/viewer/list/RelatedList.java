package com.lifeof843.hearthstonereference.viewer.list;

import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public interface RelatedList {
    List<String> getList();
}
