package com.lifeof843.hearthstonereference.viewer.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class ListKazukus implements RelatedList {
    @Override
    public List<String> getList() {
        List<String> list = new ArrayList<>();

        list.add("Lesser Potion");
        list.add("Greater Potion");
        list.add("Superior Potion");
        list.add("Goldthorn");
        list.add("Goldthorn1");
        list.add("Goldthorn2");
        list.add("Felbloom");
        list.add("Felbloom1");
        list.add("Felbloom2");
        list.add("Icecap");
        list.add("Icecap1");
        list.add("Icecap2");
        list.add("Kingsblood");
        list.add("Kingsblood1");
        list.add("Kingsblood2");
        list.add("Mystic Wool");
        list.add("Mystic Wool1");
        list.add("Netherbloom");
        list.add("Netherbloom1");
        list.add("Netherbloom2");
        list.add("Shadow Oil");
        list.add("Shadow Oil1");
        list.add("Shadow Oil2");
        list.add("Stonescale Oil");
        list.add("Stonescale Oil1");
        list.add("Stonescale Oil2");
        list.add("Ichor of Undeath");
        list.add("Ichor of Undeath1");
        list.add("Ichor of Undeath2");
        list.add("Heart of Fire");
        list.add("Heart of Fire1");
        list.add("Heart of Fire2");
        list.add("Sheep1");
        list.add("Kabal Demon");
        list.add("Kabal Demon1");
        list.add("Kabal Demon2");
        list.add("Kazakus Potion");
        list.add("Kazakus Potion1");
        list.add("Kazakus Potion2");

        return list;
    }
}
