package com.lifeof843.hearthstonereference.viewer.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class ListJadeGolem implements RelatedList {

    @Override
    public List<String> getList() {
        List<String> list = new ArrayList<>();

        list.add("Jade Golem01");
        list.add("Jade Golem02");
        list.add("Jade Golem03");
        list.add("Jade Golem04");
        list.add("Jade Golem05");
        list.add("Jade Golem06");
        list.add("Jade Golem07");
        list.add("Jade Golem08");
        list.add("Jade Golem09");
        list.add("Jade Golem10");
        list.add("Jade Golem11");
        list.add("Jade Golem12");
        list.add("Jade Golem13");
        list.add("Jade Golem14");
        list.add("Jade Golem15");
        list.add("Jade Golem16");
        list.add("Jade Golem17");
        list.add("Jade Golem18");
        list.add("Jade Golem19");
        list.add("Jade Golem20");
        list.add("Jade Golem21");
        list.add("Jade Golem22");
        list.add("Jade Golem23");
        list.add("Jade Golem24");
        list.add("Jade Golem25");
        list.add("Jade Golem26");
        list.add("Jade Golem27");
        list.add("Jade Golem28");
        list.add("Jade Golem29");
        list.add("Jade Golem30");

        return list;
    }
}
