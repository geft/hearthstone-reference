package com.lifeof843.hearthstonereference.viewer.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerry on 01/12/2016.
 */

public class ListSparePart implements RelatedList {
    @Override
    public List<String> getList() {
        List<String> list = new ArrayList<>();

        list.add("Armor Plating");
        list.add("Time Rewinder");
        list.add("Rusty Horn");
        list.add("Finicky Cloakfield");
        list.add("Emergency Coolant");
        list.add("Reversing Switch");
        list.add("Whirling Blades");

        return list;
    }
}
