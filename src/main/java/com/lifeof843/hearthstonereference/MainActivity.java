package com.lifeof843.hearthstonereference;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.lifeof843.hearthstonereference.browser.CardBrowser;
import com.lifeof843.hearthstonereference.decks.DeckManager;
import com.lifeof843.hearthstonereference.donate.DonateActivity;
import com.lifeof843.hearthstonereference.download.APKExpansionHelper;
import com.lifeof843.hearthstonereference.download.DownloadUI;
import com.lifeof843.hearthstonereference.gallery.Gallery;

import org.codechimp.apprater.AppRater;

import java.util.Calendar;


public class MainActivity extends FragmentActivity {

    public static final int PERMISSION_STORAGE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initStoragePermission();
        initButtons();
        initAppRater();
        initDonateNotice();
        initAppVersion();
    }

    private void initAppVersion() {
        TextView appVersion = (TextView) findViewById(R.id.app_version);

        if (appVersion != null) {
            appVersion.setText(getString(R.string.copyright_juans, BuildConfig.VERSION_NAME));
        }
    }

    private void initDonateNotice() {
        PrefHandler prefHandler = new PrefHandler(this);

        if (!prefHandler.isNoticeShown() && isBeforeMarch2017()) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.donate_notice_title))
                    .setMessage(getString(R.string.donate_notice_content))
                    .setNeutralButton(getString(R.string.donate_notice_action),
                            (dialogInterface, i) -> {
                                prefHandler.setNoticeShown(true);
                                dialogInterface.dismiss();
                            })
                    .create()
                    .show();
        }
    }

    private boolean isBeforeMarch2017() {
        Calendar limit = Calendar.getInstance();
        limit.set(Calendar.MONTH, Calendar.MARCH);
        limit.set(Calendar.YEAR, 2017);

        Calendar today = Calendar.getInstance();

        return today.before(limit);
    }

    private void initStoragePermission() {
        if (GlobalState.isStoragePermissionGranted(this)) {
            initAPKExpansion();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
                initAPKExpansion();
            } else {
                Toast.makeText(
                        this,
                        getString(R.string.state_failed_cancelled_storage_denied),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initAppRater() {
        AppRater.app_launched(this);
        AppRater.setDontRemindButtonVisible(false);
        AppRater.setNumLaunchesForRemindLater(5);
        AppRater.setCancelable(false);
    }

    private void initAPKExpansion() {
        PendingIntent pendingIntent = createPendingIntent();

        try {
            new APKExpansionHelper(this, pendingIntent);

            if (GlobalState.getZipResourceFile() == null) {
                GlobalState.setZipResourceFile(APKExpansionSupport.getAPKExpansionZipFile(
                        getApplicationContext(),
                        APKExpansionHelper.EXT_VERSION_MAIN,
                        APKExpansionHelper.EXT_VERSION_PATCH));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        GlobalState.initZipEntries();
    }

    private PendingIntent createPendingIntent() {
        Intent notifierIntent = new Intent(this, DownloadUI.class);
        notifierIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return PendingIntent.getActivity(
                getApplicationContext(), 0, notifierIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void initButtons() {
        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button4);

        ButtonHelper(button1, CardBrowser.class);
        ButtonHelper(button2, DeckManager.class);
        ButtonHelper(button3, Gallery.class);
        ButtonHelper(button4, DonateActivity.class);
    }

    void ButtonHelper(final Button button, final Class<?> toClass) {
        button.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, toClass)));
    }
}
