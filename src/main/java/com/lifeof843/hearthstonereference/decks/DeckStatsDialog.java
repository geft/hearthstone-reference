package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeckStatsDialog extends Activity {

    private float pixelPerCount;
    private float paddingPixels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_stats_dialog);

        // set background transparent
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        // get pixel density multiplier
        float scale = getResources().getDisplayMetrics().density;
        pixelPerCount = getPixelPerCount(scale);
        paddingPixels = getPaddingPixels(scale);

        int[] countArray;
        if (getIntent() != null && getIntent().getExtras() != null) {
            countArray = getIntent().getExtras().getIntArray("countArray");
        } else {
            countArray = new int[8];
        }

        drawStats(countArray);
    }

    private float getPaddingPixels(float scale) {
        return 15 * scale + 0.5f;
    }

    private float getPixelPerCount(float scale) {
        return ((88 - 15) * scale + 0.5f) / 7;
    }

    private void drawStats(int[] countArray) {

        DrawBar(findViewById(R.id.y0), findViewById(R.id.count0), countArray[0]);
        DrawBar(findViewById(R.id.y1), findViewById(R.id.count1), countArray[1]);
        DrawBar(findViewById(R.id.y2), findViewById(R.id.count2), countArray[2]);
        DrawBar(findViewById(R.id.y3), findViewById(R.id.count3), countArray[3]);
        DrawBar(findViewById(R.id.y4), findViewById(R.id.count4), countArray[4]);
        DrawBar(findViewById(R.id.y5), findViewById(R.id.count5), countArray[5]);
        DrawBar(findViewById(R.id.y6), findViewById(R.id.count6), countArray[6]);
        DrawBar(findViewById(R.id.y7), findViewById(R.id.count7), countArray[7]);

        ImageView imageView = (ImageView) findViewById(R.id.imageStats);
        imageView.setImageResource(R.drawable.bg_deck_stats);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DrawBar(View bar, View text, int count) {

        // only display text for positive count
        if (count != 0) {
            ((TextView) text).setText(String.format(Locale.US, "%d", count));
        }

        // alignment issue with double digits
        if (count >= 10) {
            text.setPadding(0, 3, 0, 0);
        }

        // limit bar height to container size
        if (count >= 7) {
            count = 7;
        }

        bar.getLayoutParams().height = (int) (count * pixelPerCount + paddingPixels);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
