package com.lifeof843.hearthstonereference.decks;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lifeof843.hearthstonereference.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeckManager extends FragmentActivity {

    private DeckDatabase deckDatabase;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_manager);

        initialize();
    }

    private void initialize() {
        listView = (ListView) findViewById(R.id.deck_listView);
        deckDatabase = DeckDatabase.getInstance(this);

        setBackgroundBlack();
        initNewDeckButton();
        createDeckAdapter();
    }

    private void initNewDeckButton() {
        findViewById(R.id.buttonNewDeck).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DeckManager.this, AddNewDeckDialog.class));
            }
        });
    }

    private void setBackgroundBlack() {
        getWindow().setBackgroundDrawableResource(android.R.color.black);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Refresh();
    }

    private void Refresh() {
        DeckAdapter adapter = (DeckAdapter) listView.getAdapter();
        adapter.changeCursor(deckDatabase.GetDeckCursor());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void createDeckAdapter() {
        Cursor cursor = deckDatabase.GetDeckCursor();
        String[] from = new String[]{"deckName"};
        int[] to = new int[]{R.id.textQuest};

        createListView(new DeckAdapter(this, cursor, from, to));

        cursor.close();
    }

    private void createListView(DeckAdapter adapter) {
        listView.setAdapter(adapter);

        // remove animation
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
