package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.utility.FileManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CardViewerDialog extends Activity {

    private String cardName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_card_viewer);

        cardName = getIntent().getStringExtra("cardName");

        setBackgroundTransparent();
        drawCard();
    }

    private void setBackgroundTransparent() {
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void drawCard() {
        Drawable cardImage = new FileManager(this).getDrawableFromName(cardName, true);

        if (cardImage == null) {
            displayCardNotFound();
        } else {
            displayCardImage(cardImage);
        }
    }

    private void displayCardImage(Drawable cardImage) {
        ImageView imageView = (ImageView) findViewById(R.id.cardImage);
        imageView.setImageDrawable(cardImage);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void displayCardNotFound() {
        TextView textView = (TextView) findViewById(R.id.textNotFound);
        textView.setText("Card Not Found");
        textView.setVisibility(View.VISIBLE);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
