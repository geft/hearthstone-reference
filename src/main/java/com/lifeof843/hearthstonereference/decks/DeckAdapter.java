package com.lifeof843.hearthstonereference.decks;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.utility.FileManager;

public class DeckAdapter extends SimpleCursorAdapter {

    private ViewHolder holder;

    public DeckAdapter(Context context, Cursor c, String[] from, int[] to) {
        super(context, R.layout.deck_decklist, c, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = super.newView(context, cursor, parent);

        holder = new ViewHolder();
        holder.name = (TextView) view.findViewById(R.id.textQuest);
        holder.portrait = (ImageView) view.findViewById(R.id.imagePortrait);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(@NonNull View view, final Context context, @NonNull Cursor cursor) {

        super.bindView(view, context, cursor);

        holder = (ViewHolder) view.getTag();

        FileManager fileManager = new FileManager(context);

        final String deckName = holder.name.getText().toString();

        holder.portrait.setImageDrawable(fileManager.GetDeckPortrait(deckName));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DeckViewer.class);
                intent.putExtra("deckName", deckName);
                context.startActivity(intent);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(context, DeckLongClickDialog.class);
                intent.putExtra("deckName", deckName);
                context.startActivity(intent);

                return false;
            }
        });
    }

    static class ViewHolder {
        TextView name;
        ImageView portrait;
    }
}
