package com.lifeof843.hearthstonereference.decks;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.browser.CardBrowser;
import com.lifeof843.hearthstonereference.utility.FileManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeckViewer extends FragmentActivity {

    private DeckDatabase deckDatabase;
    private String deckName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_viewer);

        if (savedInstanceState == null) {
            deckName = getIntent().getStringExtra("deckName");
        } else {
            deckName = savedInstanceState.getString("deckName");
        }

        initialize();
    }

    private void initialize() {
        deckDatabase = DeckDatabase.getInstance(this);

        initDeckPortraits();
        initStatsButton();
        initHintButton();
        setDeckTitle();
    }

    private void setDeckTitle() {
        TextView textView = (TextView) findViewById(R.id.deckName);

        if (textView != null) {
            textView.setText(deckName);
        }
    }

    private void initHintButton() {
        Button button = (Button) findViewById(R.id.buttonHint);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityWithDeckName();
            }
        });
    }

    private void startActivityWithDeckName() {
        Intent intent = new Intent(this, CardBrowser.class);
        intent.putExtra("deckName", deckName);
        startActivity(intent);
    }

    private void initStatsButton() {
        Button buttonStats = (Button) findViewById(R.id.buttonStats);
        buttonStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] countArray = new int[8];

                for (int i = 0; i < countArray.length; i++) {
                    countArray[i] = deckDatabase.GetCostCount(deckName, i);
                }

                Bundle bundle = new Bundle();
                bundle.putIntArray("countArray", countArray);

                Intent intent = new Intent(DeckViewer.this, DeckStatsDialog.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void initDeckPortraits() {
        ImageView imagePortrait = (ImageView) findViewById(R.id.imagePortrait);

        if (imagePortrait != null) {
            FileManager fileManager = new FileManager(this);
            imagePortrait.setImageDrawable(fileManager.GetDeckPortrait(deckName));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        populateListView();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putString("deckName", deckName);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateDeckCounter();
        updateDeckList();
    }

    private void updateDeckCounter() {
        int deckCount = deckDatabase.GetDeckCount(deckName);
        TextView textCount = (TextView) findViewById(R.id.textCountTotal);
        textCount.setText(Integer.toString(deckCount) + "/30");
    }

    private void updateDeckList() {
        ListView listView = (ListView) findViewById(R.id.deckList);

        DeckViewerAdapter adapter = (DeckViewerAdapter) listView.getAdapter();
        if (adapter != null) {
            adapter.changeCursor(deckDatabase.GetCardCursor(deckName));
        } else {
            populateListView();
        }
    }

    private void populateListView() {
        Cursor cursor = deckDatabase.GetCardCursor(deckName);

        String[] from = new String[]{"cardName", "count"};
        int[] to = new int[]{R.id.textQuest, R.id.textCount};
        DeckViewerAdapter adapter = new DeckViewerAdapter(this, cursor, from, to);

        if (!deckDatabase.GetListOfCards(deckName).isEmpty()) {
            ListView listView = (ListView) findViewById(R.id.deckList);
            listView.setAdapter(adapter);

            setListViewListeners(listView);
        }

        cursor.close();
    }

    private void setListViewListeners(ListView listView) {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startCardCountChangerDialog(view);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                startCardViewerDialog(view);
                return true;
            }
        });
    }

    private void startCardCountChangerDialog(View view) {
        Intent intent = new Intent(this, CardCountChangerDialog.class);
        intent.putExtra("cardName",
                ((TextView) view.findViewById(R.id.textQuest)).getText().toString());
        intent.putExtra("deckName", deckName);
        startActivity(intent);
    }

    private void startCardViewerDialog(View view) {
        Intent intent = new Intent(this, CardViewerDialog.class);
        intent.putExtra("cardName",
                ((TextView) view.findViewById(R.id.textQuest)).getText().toString());
        startActivity(intent);
    }
}
