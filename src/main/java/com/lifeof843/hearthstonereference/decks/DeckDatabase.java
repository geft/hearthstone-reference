package com.lifeof843.hearthstonereference.decks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.crashlytics.android.Crashlytics;
import com.lifeof843.hearthstonereference.Utils;
import com.lifeof843.hearthstonereference.cards.Card;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class DeckDatabase extends SQLiteOpenHelper {

    private static DeckDatabase instance;
    private final String MAIN_TABLE_NAME = "DECK_LIST";
    private SQLiteDatabase database;

    private DeckDatabase(Context context) {
        super(context, "decks.db", null, 1);

        OpenDatabase();
    }

    public static DeckDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new DeckDatabase(context.getApplicationContext());
        }

        return instance;
    }

    private void OpenDatabase() {
        database = getWritableDatabase();
    }

    public Cursor GetCardCursor(String deckName) {
        return database.rawQuery("SELECT * FROM [" + deckName + "] ORDER BY cost ASC", null);
    }

    public Cursor GetDeckCursor() {
        return database.rawQuery("SELECT * FROM [" + MAIN_TABLE_NAME + "]", null);
    }

    public int GetDeckCount(String deckName) {
        Cursor cursor = database.rawQuery(
                "SELECT * FROM [" + deckName + "]", null);

        int count = 0;

        while (cursor.moveToNext()) {
            count += cursor.getInt(cursor.getColumnIndex("count"));
        }
        cursor.close();

        return count;
    }

    public boolean doesCardExist(String deckName, String cardName) {
        if (deckName == null || cardName == null) {
            return false;
        }

        Cursor cursor = database.rawQuery(
                "SELECT * FROM [" + deckName + "] WHERE cardName=?", new String[]{cardName});

        boolean isSuccess = cursor.moveToFirst();

        cursor.close();

        return isSuccess;
    }

    public void AddCardToDeck(String deckName, String cardName, int count, int cost) {
        ContentValues values = new ContentValues();
        values.put("cardName", cardName);
        values.put("cost", cost);

        if (doesCardExist(deckName, cardName)) {
            values.put("count", 2);
            database.update(UseBrackets(deckName), values, "cardName=?", new String[]{cardName});
        } else {
            values.put("count", count);
            database.insert(UseBrackets(deckName), null, values);
        }
    }

    private String UseBrackets(String string) {
        return ("[" + string + "]");
    }

    public String getPlayerClassFromDeck(String deckName) {
        String playerClass = null;

        Cursor cursor = database.rawQuery(
                "SELECT playerClass FROM [" + MAIN_TABLE_NAME + "] WHERE deckName=?", new String[]{deckName});

        if (cursor.moveToFirst()) {
            playerClass = cursor.getString(0);
        }

        cursor.close();

        return playerClass;
    }

    public void AddNewDeck(String deckName, String playerClass) {

        ContentValues values = new ContentValues();
        values.put("deckName", deckName);
        values.put("playerClass", playerClass);

        database.insert(MAIN_TABLE_NAME, null, values);
        database.execSQL(
                "CREATE TABLE IF NOT EXISTS [" + deckName +
                        "] (_id INTEGER PRIMARY KEY AUTOINCREMENT, cardName TEXT, count INTEGER, cost INTEGER);");
    }

    public void RenameDeck(Context baseContext, String oldName, String newName) {

        ContentValues values = new ContentValues();
        values.put("deckName", newName);

        database.update(MAIN_TABLE_NAME, values, "deckName=?", new String[]{oldName});

        try {
            database.execSQL("ALTER TABLE [" + oldName + "] RENAME TO [" + newName + "];");
        } catch (SQLException e) {
            Fabric.getLogger().e(this.getClass().getName(), "Error renaming deck " + oldName + " to " + newName);
            Utils.showToast(baseContext, "Error renaming deck. Does deck already exists?");
        }
    }

    public void RemoveDeck(String deckName) {
        database.delete(MAIN_TABLE_NAME, "deckName=?", new String[]{deckName});
        database.execSQL("DROP TABLE [" + deckName + "];");
    }

    public void SetCardCount(String deckName, String cardName, int count) {
        ContentValues values = new ContentValues();
        values.put("count", count);

        database.update(UseBrackets(deckName), values, "cardName=?", new String[]{cardName});
    }

    public void RemoveCardFromDeck(String deckName, String cardName) {
        database.delete(UseBrackets(deckName), "cardName=?", new String[]{cardName});
    }

    public int getCardCount(String deckName, String cardName) {
        if (cardName == null) {
            return 0;
        }

        int currCount = 0;

        Cursor cursor = database.rawQuery(
                "SELECT count FROM [" + deckName + "] WHERE cardName=?", new String[]{cardName});

        if (cursor.moveToFirst()) {
            currCount = cursor.getInt(0);
        }

        cursor.close();

        return currCount;
    }

    public ArrayList<String> GetListOfDecks() {
        Cursor cursor = database.rawQuery("SELECT * FROM [" + MAIN_TABLE_NAME + "]", null);

        return GetListHelper(cursor, "deckName");
    }

    public ArrayList<String> GetListOfCards(String deckName) {
        Cursor cursor = database.rawQuery("SELECT * FROM [" + deckName + "]", null);

        try {
            return GetListHelper(cursor, "cardName");
        } catch (Exception e) {
            Crashlytics.log("GetListHelper crash with deckName = " + deckName);
            Crashlytics.logException(e);
        }

        return new ArrayList<>();
    }

    private ArrayList<String> GetListHelper(Cursor cursor, String column) {

        ArrayList<String> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                String item = cursor.getString(cursor.getColumnIndex(column));
                list.add(item);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return list;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS [" + MAIN_TABLE_NAME +
                "] (_id INTEGER PRIMARY KEY AUTOINCREMENT, deckName TEXT, playerClass TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int GetCostCount(String deckName, int cost) {
        int count = 0;
        Cursor cursor;

        if (cost >= 7) {
            cursor = database.rawQuery("SELECT * FROM [" + deckName + "] " +
                    "WHERE " + Card.PROP.COST.toString() + ">= 7", null);
        } else {
            cursor = database.rawQuery("SELECT * FROM [" + deckName + "] " +
                    "WHERE " + Card.PROP.COST.toString() + "=" + cost, null);
        }

        while (cursor.moveToNext()) {
            if (Integer.parseInt(cursor.getString(cursor.getColumnIndex("count"))) == 2) {
                count += 2;
            } else {
                count++;
            }
        }

        cursor.close();

        return count;
    }

    public boolean doesDeckExist(String deckName) {
        Cursor cursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name=?",
                new String[]{deckName});

        boolean doesExist = cursor.moveToFirst();
        cursor.close();

        return doesExist;
    }
}
