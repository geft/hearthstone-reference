package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.CardDatabase;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CardCountChangerDialog extends Activity {

    private CardDatabase databaseCard;
    private DeckDatabase databaseDeck;
    private String cardName, deckName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_long_click_dialog);

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        if (savedInstanceState == null) {
            cardName = getIntent().getStringExtra("cardName");
            deckName = getIntent().getStringExtra("deckName");
        } else {
            cardName = savedInstanceState.getString("cardName");
            deckName = savedInstanceState.getString("deckName");
        }

        databaseCard = CardDatabase.getInstance(this);
        databaseDeck = DeckDatabase.getInstance(this);

        SetTitle();
        InitButtons();
    }

    private void SetTitle() {
        TextView textView = (TextView) findViewById(R.id.textQuest);
        textView.setText(cardName);
    }

    private void InitButtons() {

        final TextView textCount = (TextView) findViewById(R.id.textCount);

        // initialise count value
        int currCount = databaseDeck.getCardCount(deckName, cardName);
        textCount.setText(getCountAsString(currCount));

        Button buttonLeft = (Button) findViewById(R.id.buttonLeft);
        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int currCount = Integer.parseInt(textCount.getText().toString());

                if (currCount > 0) {
                    textCount.setText(getCountAsString(currCount - 1));
                }
            }
        });

        Button buttonRight = (Button) findViewById(R.id.buttonRight);
        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int currCount = Integer.parseInt(textCount.getText().toString());

                if (currCount == 0) {
                    textCount.setText(getCountAsString(currCount + 1));

                } else if (currCount == 1) {
                    if (!databaseCard.isCardLegendary(cardName)) {
                        textCount.setText(getCountAsString(currCount + 1));
                    }
                }
            }
        });

        Button change = (Button) findViewById(R.id.buttonChange);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currCount = Integer.parseInt(textCount.getText().toString());

                CardCountChangerDialog.this.ChangeCount(deckName, cardName, currCount);

                finish();
            }
        });
    }

    private String getCountAsString(int currCount) {
        return Integer.toString(currCount);
    }

    private void ChangeCount(String deckName, String cardName, int count) {

        if (count == 0) {
            databaseDeck.RemoveCardFromDeck(deckName, cardName);
        } else {
            databaseDeck.SetCardCount(deckName, cardName, count);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putString("cardName", cardName);
        outState.putString("deckName", deckName);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
