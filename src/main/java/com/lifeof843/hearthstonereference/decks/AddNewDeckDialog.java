package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.utility.CustomSpinnerAdapter;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddNewDeckDialog extends Activity {

    private Spinner playerClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_deck_dialog);

        try {
            InitClass();
            InitPositive();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitPositive() {
        findViewById(R.id.buttonAddDeck).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeckDatabase db = DeckDatabase.getInstance(AddNewDeckDialog.this);

                EditText editName = (EditText) findViewById(R.id.editName);
                String deckName = editName.getText().toString();

                if (deckName.isEmpty()) {
                    editName.setHint(getString(R.string.add_new_deck_dialog_name_blank));
                } else if (!isAlphaNumeric(deckName)) {
                    editName.setText("");
                    editName.setHint(getString(R.string.add_new_deck_dialog_invalid_characters));
                } else if (db.doesDeckExist(deckName)) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Deck already exists",
                            Toast.LENGTH_SHORT).show();
                } else {
                    db.AddNewDeck(deckName, playerClass.getSelectedItem().toString());
                    finish();
                }
            }
        });
    }

    private boolean isAlphaNumeric(String deckName) {
        char[] chars = deckName.toCharArray();

        for (char c : chars) {
            if (!Character.isDigit(c) && !Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }

    private void InitClass() {
        playerClass = (Spinner) findViewById(R.id.spinnerClass);

        ArrayList<String> list = new ArrayList<>();

        for (Card.CLASS c : Card.CLASS.values()) {

            if (!c.toString().equals(Card.CLASS.NEUTRAL.toString())) {
                list.add(c.toString());
            }
        }

        SpinnerAdapter adapter = new CustomSpinnerAdapter(
                this, R.layout.spinner_dropdown, R.id.textView, list);
        playerClass.setAdapter(adapter);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
