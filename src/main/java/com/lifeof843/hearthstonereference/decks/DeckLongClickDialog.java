package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeckLongClickDialog extends Activity {

    private String deckName;
    private DeckDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_long_click_dialog);

        if (savedInstanceState == null) {
            deckName = getIntent().getStringExtra("deckName");
        } else {
            deckName = savedInstanceState.getString("deckName");
        }

        database = DeckDatabase.getInstance(this);

        // set title
        ((TextView) findViewById(R.id.textTitle)).setText(deckName);

        InitButtons();
    }

    private void InitButtons() {
        final EditText newName = (EditText) findViewById(R.id.editNewName);
        final Button rename = (Button) findViewById(R.id.buttonRename);
        final Button delete = (Button) findViewById(R.id.buttonDelete);

        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String string = newName.getText().toString();
                if (string.isEmpty()) {
                    newName.setHint(getString(R.string.deck_long_click_dialog_rename_blank));
                } else {
                    database.RenameDeck(getBaseContext(), deckName, newName.getText().toString());
                    finish();
                }
            }
        });

        // pressing once transforms the button into a prompt
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String prompt = getString(R.string.deck_long_click_dialog_delete_prompt);

                if (delete.getText().toString().equals(prompt)) {
                    database.RemoveDeck(deckName);
                    finish();
                } else {
                    delete.setText(prompt);
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putString("deckName", deckName);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
