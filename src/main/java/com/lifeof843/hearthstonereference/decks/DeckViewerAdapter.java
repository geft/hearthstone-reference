package com.lifeof843.hearthstonereference.decks;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.utility.FileManager;

public class DeckViewerAdapter extends SimpleCursorAdapter {

    private final float scale;
    private CardDatabase databaseCard;
    private ViewHolder holder;
    private Context context;

    public DeckViewerAdapter(Context context, Cursor cursor, String[] from, int[] to) {
        super(context, R.layout.deck_cardlist, cursor, from, to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        this.databaseCard = CardDatabase.getInstance(context);
        this.context = context;

        scale = context.getResources().getDisplayMetrics().density;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = super.newView(context, cursor, parent);

        holder = new ViewHolder();
        holder.cost = (TextView) view.findViewById(R.id.textCost);
        holder.count = (TextView) view.findViewById(R.id.textCount);
        holder.cardImage = (ImageView) view.findViewById(R.id.imageCard);
        holder.star = (ImageView) view.findViewById(R.id.imageStar);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(@NonNull View view, final Context context, @NonNull Cursor cursor) {
        super.bindView(view, context, cursor);

        final String cardName = cursor.getString(cursor.getColumnIndex("cardName"));

        holder = (ViewHolder) view.getTag();
        AssignCost(holder.cost, cardName);
        AssignImage(holder.cardImage, cardName);
        AssignCount(holder.star, holder.count, holder.cardImage, cardName);
    }

    /*
    Change the count to a star if the card is legendary, and remove the view if it is not
     */
    private void AssignCount(ImageView star, TextView count, ImageView image, String cardName) {

        int pixel3 = (int) (3 * scale + 0.5f);

        // default state
        star.setVisibility(View.INVISIBLE);
        count.setVisibility(View.VISIBLE);
        image.setPadding(0, pixel3, pixel3 * 10, pixel3);

        String rarity = databaseCard.getProperty(cardName, Card.PROP.RARITY);

        if (count.getText().toString().equals("1")) {

            image.setPadding(0, pixel3, pixel3, pixel3);

            if (rarity.equals(Card.RARITY.LEGENDARY.toString())) {
                star.setVisibility(View.VISIBLE);
                count.setText("");
            } else {
                count.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void AssignCost(TextView view, String cardName) {
        String cost = databaseCard.getProperty(cardName, Card.PROP.COST);
        view.setText(cost);
    }

    private void AssignImage(ImageView cardImage, String cardName) {
        FileManager fileManager = new FileManager(context);
        cardImage.setImageDrawable(fileManager.getDrawableFromName(cardName, false));
    }

    static class ViewHolder {
        TextView cost;
        TextView count;
        ImageView cardImage;
        ImageView star;
    }
}
