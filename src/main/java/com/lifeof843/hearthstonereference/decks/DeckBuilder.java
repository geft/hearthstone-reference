package com.lifeof843.hearthstonereference.decks;

import android.app.Activity;
import android.content.Context;
import android.widget.RadioButton;
import android.widget.Toast;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;

public class DeckBuilder {
    private CardDatabase cardDatabase;
    private DeckDatabase deckDatabase;
    private String deckName;
    private String cardName;
    private Context context;

    public DeckBuilder(Context context, String deckName, String cardName) {
        this.deckName = deckName;
        this.cardName = cardName;
        this.cardDatabase = CardDatabase.getInstance(context);
        this.deckDatabase = DeckDatabase.getInstance(context);
        this.context = context;
    }

    public boolean quickAdd() {
        if (!isCardUsableInDeck(getPlayerClass(), deckName)) {
            makeToast(cardName + " is not the right class");
        } else if (deckDatabase.getCardCount(deckName, cardName) == 2) {
            makeToast(deckName + " already has two copies of " + cardName);
        } else if (isCardLegendary(cardName)) {
            if (deckDatabase.getCardCount(deckName, cardName) == 1) {
                makeToast("A deck may only have a copy of " + cardName);
            } else {
                return addOneCard();
            }
        } else if (isCountSetToOne()) {
            return addOneCard();
        } else if (deckDatabase.doesCardExist(deckName, cardName)) {
            if (deckDatabase.getCardCount(deckName, cardName) == 1) {
                addOneCard();
            }
        } else {
            return addTwoCards();
        }

        return false;
    }

    private boolean addTwoCards() {
        addCardToDeck(deckName, cardName, 2);
        makeToast("Two copies of " + cardName + " added to " + deckName);
        return true;
    }

    private boolean addOneCard() {
        addCardToDeck(deckName, cardName, 1);
        makeToast("A copy of " + cardName + " added to " + deckName);
        return true;
    }

    private boolean isCountSetToOne() {
        RadioButton radio1 = (RadioButton) ((Activity) context).findViewById(R.id.radio1);
        return radio1.isChecked();
    }

    private void makeToast(String text) {
        Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    public String getPlayerClass() {
        return cardDatabase.getProperty(cardName, Card.PROP.PLAYER_CLASS);
    }

    public boolean isCardUsableInDeck(String playerClass, String deckName) {
        return deckDatabase.getPlayerClassFromDeck(deckName).equals(playerClass) ||
                playerClass.equalsIgnoreCase(Card.CLASS.NEUTRAL.toString());
    }

    public void addCardToDeck(String deckName, String cardName, int count) {
        try {
            int cost = Integer.parseInt(cardDatabase.getProperty(cardName, Card.PROP.COST));
            deckDatabase.AddCardToDeck(deckName, cardName, count, cost);
        } catch (Exception e) {
            Toast.makeText(context, "Error adding card to deck", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isCardLegendary(String cardName) {
        return cardDatabase.isCardLegendary(cardName);
    }
}
