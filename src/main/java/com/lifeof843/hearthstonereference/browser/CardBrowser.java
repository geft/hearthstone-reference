package com.lifeof843.hearthstonereference.browser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.decks.DeckDatabase;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CardBrowser extends FragmentActivity implements ICardList, ISearchResults {

    private CardDatabase cardDatabase;
    private ArrayList<String> tempList, fullList;
    private int listPosition;

    private EnabledClassFilter classManager;
    private ArrayList<String> toggleStates;

    private String deckName;
    private boolean isToggleSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_browser);

        if (savedInstanceState != null) {
            listPosition = savedInstanceState.getInt("listPosition");
            toggleStates = savedInstanceState.getStringArrayList("toggleStates");
            deckName = savedInstanceState.getString("deckName");
        } else {
            deckName = getIntent().getStringExtra("deckName");
            listPosition = 0;
        }

        initialize();
        initDeckMode();
    }

    private void initDeckMode() {
        if (deckName != null) {
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            radioGroup.setVisibility(View.VISIBLE);
            RadioButton radio2 = (RadioButton) findViewById(R.id.radio1);
            radio2.setChecked(true);

            Toast.makeText(
                    getApplicationContext(),
                    getString(R.string.card_browser_deck_mode),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void initialize() {
        isToggleSet = false;
        cardDatabase = CardDatabase.getInstance(this);
        styleHorizontalScrollView();
        populateList();
        initSearch();
        initAdvancedSearch();
        initSearchText();
        initSortButton();
        generateToggles();
    }

    private void initSortButton() {
        CheckBox checkBox = (CheckBox) findViewById(R.id.browser_sort);
        checkBox.setChecked(false);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> generateListViewItems());
    }

    private void styleHorizontalScrollView() {
        HorizontalScrollView view = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        view.setHorizontalScrollBarEnabled(false);
        view.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setListPosition();
    }

    private void setListPosition() {
        findViewById(R.id.listView_browser).setVerticalScrollbarPosition(listPosition);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle bundle) {
        saveScrollPositionInBundle(bundle);
        saveEnabledClassesInBundle(bundle);
        bundle.putString("deckName", deckName);

        super.onSaveInstanceState(bundle);
    }

    private void saveEnabledClassesInBundle(Bundle bundle) {
        toggleStates = classManager.GetEnabledClasses();
        bundle.putStringArrayList("toggleStates", toggleStates);
    }

    private void saveScrollPositionInBundle(Bundle bundle) {
        bundle.putInt("listPosition", ((ListView) findViewById(R.id.listView_browser))
                .getFirstVisiblePosition());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initSearchText() {
        getSearchTextCheckBox().setOnCheckedChangeListener(
                (buttonView, isChecked) -> ResetAll()
        );
    }

    private void updateListView(boolean useFullList) {
        if (useFullList) copyFullList();

        saveEnabledClassStates();
        generateToggles();
        generateListViewItems();
        restoreToggleAllState(saveToggleAllState());
    }

    private boolean saveToggleAllState() {
        return getClassToggleAllById().isChecked();
    }

    private void restoreToggleAllState(boolean toggleAllState) {
        getClassToggleAllById().setChecked(toggleAllState);
    }

    private void saveEnabledClassStates() {
        if (classManager != null) {
            toggleStates = classManager.GetEnabledClasses();
        }
    }

    private CheckBox getClassToggleAllById() {
        return (CheckBox) findViewById(R.id.classToggleAll);
    }

    private void generateToggles() {
        if (deckName != null && !isToggleSet) {
            DeckDatabase deckDatabase = DeckDatabase.getInstance(this);
            toggleStates = new ArrayList<>();
            toggleStates.add(deckDatabase.getPlayerClassFromDeck(deckName));
            toggleStates.add(Card.CLASS.NEUTRAL.toString());
            isToggleSet = true;
        }

        classManager = new EnabledClassFilter(this, tempList, fullList, toggleStates);
    }

    private void generateListViewItems() {
        tempList = new ListViewManager(this, deckName)
                .Generate(tempList, toggleStates);
    }

    private void initAdvancedSearch() {
        ImageButton button = (ImageButton) findViewById(R.id.browser_advanced_search);

        button.setOnClickListener(v ->
                startActivityForResult(
                        new Intent(CardBrowser.this, AdvancedSearchDialog.class), 0));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && resultCode == RESULT_OK) {
            AdvancedSearchData result = data.getExtras().getParcelable("result");
            filterByAdvancedSearch(result);
        }
    }

    private void populateList() {
        fullList = cardDatabase.getFullList();
        tempList = new ArrayList<>(fullList);

        updateListView(true);
    }

    private void initSearch() {
        new BrowserSearchView(this, getSearchView());
    }

    @Override
    public String getQuery() {
        return getSearchView().getQuery().toString();
    }

    @Override
    public void displaySearchResults(ArrayList<String> list) {
        tempList = list;
        updateListView(false);
    }

    @Override
    public ArrayList<String> getSearchResults(String query) {
        return new SearchByQuery(this, query, isSearchTextEnabled()).getResult();
    }

    public boolean isSearchTextEnabled() {
        return getSearchTextCheckBox().isChecked();
    }

    public SearchView getSearchView() {
        return (SearchView) findViewById(R.id.browser_search);
    }

    public CheckBox getSearchTextCheckBox() {
        return (CheckBox) findViewById(R.id.browser_checkBox);
    }

    private void ResetAll() {
        resetQuery();
        updateListView(true);
    }

    private void copyFullList() {
        tempList = new ArrayList<>(fullList);
    }

    private void resetQuery() {
        getSearchView().setQuery("", false);
    }

    private void filterByAdvancedSearch(AdvancedSearchData searchData) {
        if (searchData.getIncludeSpells()) {
            tempList = cardDatabase.getFullList();
        } else {
            tempList = cardDatabase.removeSpellsFromList(fullList);
        }

        FilterList filterList = new FilterList(this, searchData);
        tempList = filterList.filterByProperty(tempList, Card.PROP.RARITY, searchData.getRarity());
        tempList = filterList.filterByProperty(tempList, Card.PROP.CARD_SET, searchData.getCardSet());
        tempList = filterList.filterListByRange(tempList, Card.PROP.COST.toString());
        tempList = filterList.filterListByRange(tempList, Card.PROP.ATTACK.toString());
        tempList = filterList.filterListByRange(tempList, Card.PROP.HEALTH.toString());

        updateListView(false);
    }

    @Override
    public void updateCardListTemp(ArrayList<String> cardList) {
        tempList = cardList;
        updateListView(false);
    }

    @Override
    public void updateCardListFull() {
        tempList = fullList;
        updateListView(true);
    }
}
