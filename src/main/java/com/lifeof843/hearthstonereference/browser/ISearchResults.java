package com.lifeof843.hearthstonereference.browser;

import java.util.ArrayList;

public interface ISearchResults {
    ArrayList<String> getSearchResults(String query);

    void displaySearchResults(ArrayList<String> list);

    String getQuery();
}
