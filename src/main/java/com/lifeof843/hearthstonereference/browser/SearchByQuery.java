package com.lifeof843.hearthstonereference.browser;

import android.content.Context;

import com.lifeof843.hearthstonereference.cards.CardDatabase;

import java.util.ArrayList;

public class SearchByQuery {
    private CardDatabase cardDatabase;
    private ArrayList<String> cardList;
    private String query;
    private boolean searchText;

    public SearchByQuery(
            Context context, String query, boolean searchText) {
        this.query = query;
        this.searchText = searchText;
        this.cardDatabase = CardDatabase.getInstance(context);

        cardList = performSearch();
    }

    private ArrayList<String> performSearch() {
        if (query.trim().isEmpty()) {
            return cardDatabase.getFullList();
        }

        ArrayList<String> list;
        if (searchText) {
            list = filterWithSearchText(query);
        } else {
            list = filterByName(query);
        }

        return list;
    }

    private ArrayList<String> filterWithSearchText(String query) {
        return cardDatabase.filterByNameAndTextAndRace(query);
    }

    private ArrayList<String> filterByName(String query) {
        ArrayList<String> searchResult = new ArrayList<>();

        for (String name : cardDatabase.getFullList()) {
            if (getFormattedCardName(name).contains(query)) {
                searchResult.add(name);
            }
        }

        return searchResult;
    }

    private String getFormattedCardName(String name) {
        return name.toLowerCase().replaceAll("[-'!]", "");
    }

    public ArrayList<String> getResult() {
        return cardList;
    }
}
