package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.utility.AddNewCardDialog;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CardInfoDialog extends Activity {

    private CardDatabase database;
    private String cardName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser_long_click_dialog);

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        cardName = getIntent().getStringExtra("cardName");
        database = CardDatabase.getInstance(this);

        InitButton();
        AssignText();
        AssignDust();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        outState.putString("cardName", cardName);

        super.onSaveInstanceState(outState);
    }

    private void InitButton() {

        findViewById(R.id.layout_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.buttonAddCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String playerClass = database.getProperty(cardName, Card.PROP.PLAYER_CLASS);
                String rarity = database.getProperty(cardName, Card.PROP.RARITY);

                Intent intent = new Intent(CardInfoDialog.this, AddNewCardDialog.class);
                intent.putExtra("cardName", cardName);
                intent.putExtra("playerClass", playerClass);
                intent.putExtra("rarity", rarity);

                startActivity(intent);
                finish();
            }
        });
    }

    private void AssignDust() {
        String dust = database.getProperty(cardName, Card.PROP.DUST);

        String[] dustArray = new String[4];

        if (dust != null && !dust.equalsIgnoreCase("null")) {
            dustArray = dust.split(",");
        } else {
            for (int i = 0; i < dustArray.length; i++) {
                dustArray[i] = "N/A";
            }
        }

        ((TextView) findViewById(R.id.textDust)).setText(dustArray[0]);
        ((TextView) findViewById(R.id.textDustG)).setText(dustArray[1]);
        ((TextView) findViewById(R.id.textDust2)).setText(dustArray[2]);
        ((TextView) findViewById(R.id.textDustG2)).setText(dustArray[3]);
    }

    private void AssignText() {
        String cardSet = database.getProperty(cardName, Card.PROP.CARD_SET);
        ((TextView) findViewById(R.id.textTitle)).setText(Card.getSetName(cardSet));

        String howToGet = database.getProperty(cardName, Card.PROP.HOW_TO_EARN);
        String howToGetGold = database.getProperty(cardName, Card.PROP.HOW_TO_EARN_GOLDEN);

        TextView textHTG = (TextView) findViewById(R.id.textHTG);
        TextView textHTGG = (TextView) findViewById(R.id.textHTGG);

        if (howToGet.equals("NULL")) {
            textHTG.setVisibility(View.GONE);
        } else {
            textHTG.setText(howToGet);
        }

        if (howToGetGold.equals("NULL")) {
            textHTGG.setVisibility(View.GONE);
        } else {
            textHTGG.setText(howToGetGold);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
