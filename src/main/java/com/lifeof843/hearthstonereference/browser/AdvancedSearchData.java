package com.lifeof843.hearthstonereference.browser;

import android.os.Parcel;
import android.os.Parcelable;

import com.lifeof843.hearthstonereference.cards.Card;

class AdvancedSearchData implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AdvancedSearchData> CREATOR = new Parcelable.Creator<AdvancedSearchData>() {
        @Override
        public AdvancedSearchData createFromParcel(Parcel in) {
            return new AdvancedSearchData(in);
        }

        @Override
        public AdvancedSearchData[] newArray(int size) {
            return new AdvancedSearchData[size];
        }
    };
    private String cardSet;
    private String rarity;
    private int maxCost;
    private int minCost;
    private int maxAttack;
    private int minAttack;
    private int maxHealth;
    private int minHealth;
    private boolean includeSpells;

    protected AdvancedSearchData(Parcel in) {
        cardSet = in.readString();
        rarity = in.readString();
        maxCost = in.readInt();
        minCost = in.readInt();
        maxAttack = in.readInt();
        minAttack = in.readInt();
        maxHealth = in.readInt();
        minHealth = in.readInt();
        includeSpells = in.readByte() != 0x00;
    }

    public void Initialise() {
        maxCost = 7;
        minCost = 0;
        maxAttack = 7;
        minAttack = 0;
        maxHealth = 7;
        minHealth = 0;

        cardSet = Card.CARD_SET.ALL.toString();
        rarity = Card.RARITY.ALL.toString();

        includeSpells = true;
    }

    public String getCardSet() {
        return cardSet;
    }

    public void setCardSet(String cardSet) {
        this.cardSet = cardSet;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public int getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(int maxCost) {
        this.maxCost = maxCost;
    }

    public int getMinCost() {
        return minCost;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public int getMaxAttack() {
        return maxAttack;
    }

    public void setMaxAttack(int maxAttack) {
        this.maxAttack = maxAttack;
    }

    public int getMinAttack() {
        return minAttack;
    }

    public void setMinAttack(int minAttack) {
        this.minAttack = minAttack;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getMinHealth() {
        return minHealth;
    }

    public void setMinHealth(int minHealth) {
        this.minHealth = minHealth;
    }

    public boolean getIncludeSpells() {
        return includeSpells;
    }

    public void setIncludeSpells(boolean includeSpells) {
        this.includeSpells = includeSpells;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cardSet);
        dest.writeString(rarity);
        dest.writeInt(maxCost);
        dest.writeInt(minCost);
        dest.writeInt(maxAttack);
        dest.writeInt(minAttack);
        dest.writeInt(maxHealth);
        dest.writeInt(minHealth);
        dest.writeByte((byte) (includeSpells ? 0x01 : 0x00));
    }
}