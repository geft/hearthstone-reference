package com.lifeof843.hearthstonereference.browser;

import android.content.Context;

import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;

import java.util.ArrayList;

public class FilterList {

    private int maxCost = 0;
    private int maxAttack = 0;
    private int maxHealth = 0;

    private CardDatabase cardDatabase;
    private AdvancedSearchData searchData;

    public FilterList(Context context, AdvancedSearchData searchData) {
        this.searchData = searchData;
        this.cardDatabase = CardDatabase.getInstance(context);
    }

    public ArrayList<String> FilterListByEnabledClass(ArrayList<String> cardList, ArrayList<String> enabledClass) {

        ArrayList<String> allEnabledCards = cardDatabase.filterByEnabledClass(enabledClass);
        ArrayList<String> tempList = new ArrayList<>();

        for (String cardName : cardList) {
            if (allEnabledCards.contains(cardName)) {
                tempList.add(cardName);
            }
        }

        return tempList;
    }

    public ArrayList<String> filterByProperty(ArrayList<String> cardList, Card.PROP property, String selection) {
        ArrayList<String> newList = new ArrayList<>();

        if (isAllSelected(selection)) {
            return cardList;
        } else {
            ArrayList<String> fullList = cardDatabase.getCardsByPropertyValue(property, selection);

            for (String cardName : cardList) {
                if (fullList.contains(cardName)) {
                    newList.add(cardName);
                }
            }
        }

        return newList;
    }

    private boolean isPropertyMatched(String match, String property) {
        return property.equals(match);
    }

    private boolean isAllSelected(String selected) {
        return selected.equals(Card.RARITY.ALL.toString());
    }

    public ArrayList<String> filterListByRange(ArrayList<String> cardList, String property) {
        getMaxValues();

        int min;
        int max;
        ArrayList<String> fullRangeList = new ArrayList<>();
        ArrayList<String> newList = new ArrayList<>();

        if (isPropertyMatched(Card.PROP.COST.toString(), property)) {
            min = searchData.getMinCost();
            max = (searchData.getMaxCost() == 7) ? maxCost : searchData.getMaxCost();
            fullRangeList = cardDatabase.filterByIntRange(Card.PROP.COST, min, max);
        } else if (isPropertyMatched(Card.PROP.ATTACK.toString(), property)) {
            min = searchData.getMinAttack();
            max = (searchData.getMaxAttack() == 7) ? maxAttack : searchData.getMaxAttack();
            fullRangeList = cardDatabase.filterByIntRange(Card.PROP.ATTACK, min, max);
        } else if (isPropertyMatched(Card.PROP.HEALTH.toString(), property)) {
            min = searchData.getMinHealth();
            max = (searchData.getMaxHealth() == 7) ? maxHealth : searchData.getMaxHealth();
            fullRangeList = cardDatabase.filterByIntRange(Card.PROP.HEALTH, min, max);
        }

        for (String cardName : cardList) {
            if (fullRangeList.contains(cardName)) {
                newList.add(cardName);
            }
        }

        return newList;
    }

    private void getMaxValues() {
        maxCost = cardDatabase.getMaxPropertyValue(Card.PROP.COST.toString());
        maxAttack = cardDatabase.getMaxPropertyValue(Card.PROP.ATTACK.toString());
        maxHealth = cardDatabase.getMaxPropertyValue(Card.PROP.HEALTH.toString());
    }
}
