package com.lifeof843.hearthstonereference.browser;

import java.util.ArrayList;

public interface ICardList {
    void updateCardListTemp(ArrayList<String> cardList);
    void updateCardListFull();
}
