package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;

import java.util.ArrayList;

class EnabledClassFilter {

    private Activity activity;
    private ArrayList<ImageView> iconArray;
    private ArrayList<String> toggleStates;
    private ArrayList<String> listTemp;
    private ArrayList<String> listFull;
    private ICardList callback;

    public EnabledClassFilter(Activity activity, ArrayList<String> listTemp, ArrayList<String> listFull,
                              ArrayList<String> toggleStates) {

        this.activity = activity;
        this.listTemp = listTemp;
        this.listFull = listFull;
        this.toggleStates = toggleStates;
        callback = (ICardList) activity;

        assignImagesToArray();
        makeClassButtons();
        assignToggleAll();
        restoreToggleStates();
    }

    private void assignToggleAll() {
        CheckBox checkBox = (CheckBox) activity.findViewById(R.id.classToggleAll);
        checkBox.setChecked(false);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleClassButtonAlpha(!((CheckBox) v).isChecked());
            }
        });
    }

    private void assignImagesToArray() {
        iconArray = new ArrayList<>();

        iconArray.add((ImageView) activity.findViewById(R.id.icon_druid));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_hunter));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_mage));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_paladin));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_priest));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_rogue));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_shaman));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_warrior));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_warlock));
        iconArray.add((ImageView) activity.findViewById(R.id.icon_neutral));
    }

    private void makeClassButtons() {
        for (final ImageView imageButton : iconArray) {
            setImageOpaque(imageButton);

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isImageOpaque(imageButton)) {
                        setImageTransparent(imageButton);
                        listTemp = removeExistingClassCards(
                                listTemp, GetClassFromId(imageButton.getId()));

                    } else {
                        setImageOpaque(imageButton);
                        listTemp = addMissingClassCards(
                                listTemp, GetClassFromId(imageButton.getId()));
                    }

                    callback.updateCardListTemp(listTemp);
                }
            });
        }
    }

    private void setImageOpaque(ImageView imageView) {
        imageView.setImageAlpha(255);
    }

    private void setImageTransparent(ImageView imageView) {
        imageView.setImageAlpha(100);
    }

    private boolean isImageOpaque(ImageView imageView) {
        return imageView.getImageAlpha() == 255;
    }

    private ArrayList<String> removeExistingClassCards(ArrayList<String> list, String playerClass) {
        CardDatabase cardDatabase = CardDatabase.getInstance(activity.getApplicationContext());
        list.removeAll(cardDatabase.getPlayerClassCards(playerClass));
        return list;
    }

    private ArrayList<String> addMissingClassCards(ArrayList<String> list, String playerClass) {
        CardDatabase cardDatabase = CardDatabase.getInstance(activity.getApplicationContext());
        list.addAll(cardDatabase.getPlayerClassCards(playerClass));
        return list;
    }

    private String GetClassFromId(int id) {

        String playerClass = null;

        switch (id) {
            case R.id.icon_druid:
                playerClass = Card.CLASS.DRUID.toString();
                break;
            case R.id.icon_hunter:
                playerClass = Card.CLASS.HUNTER.toString();
                break;
            case R.id.icon_mage:
                playerClass = Card.CLASS.MAGE.toString();
                break;
            case R.id.icon_paladin:
                playerClass = Card.CLASS.PALADIN.toString();
                break;
            case R.id.icon_priest:
                playerClass = Card.CLASS.PRIEST.toString();
                break;
            case R.id.icon_rogue:
                playerClass = Card.CLASS.ROGUE.toString();
                break;
            case R.id.icon_shaman:
                playerClass = Card.CLASS.SHAMAN.toString();
                break;
            case R.id.icon_warrior:
                playerClass = Card.CLASS.WARRIOR.toString();
                break;
            case R.id.icon_warlock:
                playerClass = Card.CLASS.WARLOCK.toString();
                break;
            case R.id.icon_neutral:
                playerClass = Card.CLASS.NEUTRAL.toString();
                break;
        }

        return playerClass;
    }

    public void ToggleClassButtonAlpha(boolean enable) {
        int alpha = (enable) ? 255 : 100;

        for (ImageView icon : iconArray) {
            icon.setImageAlpha(alpha);
        }

        ArrayList<String> list = enable ? new ArrayList<>(listFull) : new ArrayList<String>();

        callback.updateCardListTemp(list);
    }

    public ArrayList<String> GetEnabledClasses() {

        ArrayList<String> list = new ArrayList<>();

        for (ImageView icon : iconArray) {
            if (isImageOpaque(icon)) {
                list.add(GetClassFromId(icon.getId()));
            }
        }

        return list;
    }

    public void restoreToggleStates() {
        if (toggleStates == null) {
            return;
        }

        setToggleOpacityFromToggleState(toggleStates);
    }

    private void setToggleOpacityFromToggleState(ArrayList<String> toggleStates) {
        for (ImageView i : iconArray) {
            if (toggleStates.contains(GetClassFromId(i.getId()))) {
                setImageOpaque(i);
            } else {
                setImageTransparent(i);
            }
        }
    }
}
