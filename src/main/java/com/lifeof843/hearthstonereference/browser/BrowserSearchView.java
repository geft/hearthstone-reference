package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.SearchView;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxSearchView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;

public class BrowserSearchView {

    private Context context;
    private SearchView searchView;
    private ISearchResults callback;

    public BrowserSearchView(Context context, SearchView searchView) {
        this.context = context;
        this.searchView = searchView;
        this.callback = (ISearchResults) context;

        initialize();
    }

    private void initialize() {
        configSearchView();
        styleSearchViewText(searchView);
        setSearchViewListener(searchView);
    }

    private void configSearchView() {
        SearchManager searchManager = (SearchManager) context.getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(((Activity) context).getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(false);
    }

    private void setSearchViewListener(final SearchView searchView) {
        RxSearchView.queryTextChanges(searchView)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(charSequence -> {
                    if (charSequence != null) {
                        ArrayList<String> list = callback.getSearchResults(charSequence.toString());
                        callback.displaySearchResults(list);
                    }
                });
    }

    private void styleSearchViewText(SearchView searchView) {
        int id = searchView.getContext().getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/belwe.otf"));
    }
}
