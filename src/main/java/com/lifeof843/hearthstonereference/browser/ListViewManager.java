package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.decks.DeckBuilder;
import com.lifeof843.hearthstonereference.utility.AddNewCardDialog;
import com.lifeof843.hearthstonereference.viewer.CardViewer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListViewManager {

    private ListView listView;
    private Context context;
    private ISearchResults callback;
    private String deckName;

    public ListViewManager(Context context, String deckName) {
        this.listView = (ListView) ((Activity) context).findViewById(R.id.listView_browser);
        this.context = context;
        this.callback = (ISearchResults) context;
        this.deckName = deckName;
    }

    public ArrayList<String> Generate(ArrayList<String> tempList, ArrayList<String> enabledClass) {
        tempList = filterByEnabledClass(tempList, enabledClass);
        ArrayList<String> searchList = callback.getSearchResults(callback.getQuery());
        tempList = filterListBySearch(tempList, searchList);
        tempList = sortList(tempList);
        generateListViewAdapter(tempList);
        setListViewOnClickListener(tempList);
        setListViewOnLongClickListener(tempList);

        return tempList;
    }

    private ArrayList<String> sortList(ArrayList<String> tempList) {
        CheckBox checkBox = getSortCheckbox();

        Collections.sort(tempList);

        if (checkBox.isChecked()) {
            tempList = sortByCost(tempList);
        }

        return tempList;
    }

    private ArrayList<String> sortByCost(ArrayList<String> tempList) {
        CardDatabase cardDatabase = CardDatabase.getInstance(context);
        return cardDatabase.getListSortedByCost(tempList);
    }

    private CheckBox getSortCheckbox() {
        return (CheckBox) ((Activity) context).findViewById(R.id.browser_sort);
    }

    private ArrayList<String> filterListBySearch(List<String> list, final ArrayList<String> searchList) {
        ArrayList<String> newList = new ArrayList<>();

        for (String cardName : list) {
            if (searchList.contains(cardName)) {
                newList.add(cardName);
            }
        }

        return newList;
    }

    private ArrayList<String> filterByEnabledClass(
            ArrayList<String> tempList, ArrayList<String> enabledClass) {
        return new FilterList(context, null).FilterListByEnabledClass(tempList, enabledClass);
    }


    private void generateListViewAdapter(ArrayList<String> listItems) {
        BrowserAdapter adapter = new BrowserAdapter(context, listItems, listView);
        listView.setAdapter(adapter);
    }

    private void startCardViewer(ArrayList<String> listItems, int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putStringArrayList("cardList", listItems);

        context.startActivity(new Intent(context, CardViewer.class).putExtras(bundle));
        ((Activity) context).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private void setListViewOnClickListener(final ArrayList<String> listItems) {
        if (deckName == null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startCardViewer(listItems, position);
                }
            });
        } else {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    quickAddCardToDeck(position, listItems);
                }
            });
        }
    }

    private void quickAddCardToDeck(int position, ArrayList<String> listItems) {
        DeckBuilder deckBuilder = new DeckBuilder(context, deckName, listItems.get(position));
        deckBuilder.quickAdd();
    }

    private void setListViewOnLongClickListener(final ArrayList<String> listItems) {
        if (deckName == null) {
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    startNewCardDialogFromList(position, listItems);
                    return true;
                }
            });
        } else {
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    startCardViewer(listItems, position);
                    return true;
                }
            });
        }
    }

    private void startNewCardDialogFromList(int position, ArrayList<String> listItems) {
        Intent intent = new Intent(context, AddNewCardDialog.class);
        intent.putExtra("cardName", listItems.get(position));
        intent.putExtra("deckName", deckName);
        context.startActivity(intent);
    }
}
