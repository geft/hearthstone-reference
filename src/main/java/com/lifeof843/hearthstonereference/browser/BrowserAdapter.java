package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.utility.FileManager;
import com.lifeof843.hearthstonereference.utility.ResourceUtil;

import java.util.List;

class BrowserAdapter extends ArrayAdapter<String> {
    private List<String> cardList;
    private CardDatabase cardDatabase;
    private FileManager fileManager;
    private Context context;
    private boolean isScrolling;
    private int firstVisiblePosition;
    private int visibleCount;

    private Drawable common, rare, epic, legendary;

    public BrowserAdapter(Context context, List<String> cardList, ListView listView) {
        super(context, R.layout.browser_list, R.id.itemText, cardList);

        init(context, cardList);
        initDrawables();
        setScrollingBehavior(listView);
    }

    private void setScrollingBehavior(final ListView listView) {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                isScrolling = scrollState == SCROLL_STATE_FLING;

                if (scrollState == SCROLL_STATE_IDLE) {
                    listView.invalidateViews();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                firstVisiblePosition = firstVisibleItem;
                visibleCount = visibleItemCount;
            }
        });
    }

    private void initDrawables() {
        common = ResourceUtil.getDrawable(getContext(), R.drawable.icon_rarity_common);
        rare = ResourceUtil.getDrawable(getContext(), R.drawable.icon_rarity_rare);
        epic = ResourceUtil.getDrawable(getContext(), R.drawable.icon_rarity_epic);
        legendary = ResourceUtil.getDrawable(getContext(), R.drawable.icon_rarity_legendary);
    }

    private void init(Context context, List<String> cardList) {
        this.cardList = cardList;
        this.cardDatabase = CardDatabase.getInstance(context);
        this.fileManager = new FileManager(context);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.browser_list, parent, false);

            viewHolder = new ViewHolder();
            bindViewHolderToResource(convertView, viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String cardName = cardList.get(position);
        assignResourceValues(viewHolder, cardName, position);

        return convertView;
    }

    private void bindViewHolderToResource(View convertView, ViewHolder viewHolder) {
        viewHolder.cost = (TextView) convertView.findViewById(R.id.itemCost);
        viewHolder.text = (TextView) convertView.findViewById(R.id.itemText);
        viewHolder.icon = (ImageView) convertView.findViewById(R.id.itemIcon);
        viewHolder.rarity = (ImageView) convertView.findViewById(R.id.itemRarity);
        viewHolder.color = (ImageView) convertView.findViewById(R.id.imageColor);
        convertView.setTag(viewHolder);
    }

    private void assignResourceValues(ViewHolder viewHolder, String cardName, int position) {
        viewHolder.cost.setText(getCardCost(cardName));
        viewHolder.text.setText(cardName);

        new AsyncResourceAssigner().execute(new ViewHolderObject(viewHolder, cardName, position));
    }

    private void assignBackground(ViewHolder viewHolder, ColorDrawable colorDrawable) {
        viewHolder.color.setBackground(colorDrawable);
    }

    private void assignRarity(ViewHolder viewHolder, int rarity) {
        viewHolder.rarity.setImageDrawable(getRarityDrawable(rarity));
    }

    private void assignIcon(ViewHolder viewHolder, Drawable drawable) {
        viewHolder.icon.setImageDrawable(drawable);
    }

    private Drawable getRarityDrawable(int resId) {
        Drawable drawable = null;

        switch (resId) {
            case R.drawable.icon_rarity_common:
                drawable = common;
                break;
            case R.drawable.icon_rarity_rare:
                drawable = rare;
                break;
            case R.drawable.icon_rarity_epic:
                drawable = epic;
                break;
            case R.drawable.icon_rarity_legendary:
                drawable = legendary;
                break;
        }

        return drawable;
    }

    private String getCardCost(String cardName) {
        int cost = cardDatabase.getIntProperty(cardName, Card.PROP.COST);
        return Integer.toString(cost);
    }

    private class ViewHolderObject {
        private int position;
        private ViewHolder viewHolder;
        private String name;
        private Drawable icon;
        private ColorDrawable color;
        private int rarity;

        ViewHolderObject(ViewHolder viewHolder, String name, int position) {
            this.viewHolder = viewHolder;
            this.name = name;
            this.position = position;
        }
    }

    private class AsyncResourceAssigner extends AsyncTask<ViewHolderObject, Void, ViewHolderObject> {

        @Override
        protected ViewHolderObject doInBackground(ViewHolderObject... params) {
            String cardName = params[0].name;

            if (firstVisiblePosition == 0 ||
                    (params[0].position >= firstVisiblePosition &&
                            params[0].position < firstVisiblePosition + visibleCount)) {
                params[0].icon = fileManager.getDrawableFromName(cardName, false);
                params[0].color = cardDatabase.GetColor(cardName);
                params[0].rarity = cardDatabase.getRarityIcon(cardName);
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(ViewHolderObject viewHolderObject) {
            ViewHolder viewHolder = viewHolderObject.viewHolder;

            if (!isScrolling || firstVisiblePosition == 0 ||
                    (viewHolderObject.position >= firstVisiblePosition &&
                            viewHolderObject.position < firstVisiblePosition + visibleCount)) {
                assignIcon(viewHolder, viewHolderObject.icon);
                assignBackground(viewHolder, viewHolderObject.color);
                assignRarity(viewHolder, viewHolderObject.rarity);
            }
        }
    }

    private class ViewHolder {
        TextView cost;
        TextView text;
        ImageView icon;
        ImageView rarity;
        ImageView color;
    }
}