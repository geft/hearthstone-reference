package com.lifeof843.hearthstonereference.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card;
import com.lifeof843.hearthstonereference.utility.CustomSpinnerAdapter;
import com.lifeof843.hearthstonereference.utility.RangeSeekBar;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdvancedSearchDialog extends Activity {

    private AdvancedSearchData _searchData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advanced_search_dialog);

        _searchData = new AdvancedSearchData(Parcel.obtain());
        _searchData.Initialise();

        GenerateViewComponents();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void GenerateViewComponents() {

        GenerateSpinnerData(Card.PROP.RARITY.toString());
        GenerateSpinnerData(Card.PROP.CARD_SET.toString());
        GenerateRangeSeekBar(Card.PROP.COST.toString());
        GenerateRangeSeekBar(Card.PROP.ATTACK.toString());
        GenerateRangeSeekBar(Card.PROP.HEALTH.toString());
        GenerateSearchButton();

        // include spell functionality
        CheckBox checkBox = (CheckBox) findViewById(R.id.advanced_search_checkBox_spell);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                _searchData.setIncludeSpells(isChecked);
            }
        });
    }

    // implement Search button
    private void GenerateSearchButton() {
        Button button = (Button) findViewById(R.id.buttonOk);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("result", _searchData);

                Intent intent = new Intent();
                intent.putExtras(bundle);

                // return intent data
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void GenerateSpinnerData(final String string) {

        ArrayList<String> list = new ArrayList<>();
        Spinner spinner = null;

        switch (string) {
            case "rarity":
                spinner = (Spinner) findViewById(R.id.advanced_search_spinner_rarity);
                for (Card.RARITY item : Card.RARITY.values()) {
                    list.add(item.toString());
                }
                break;
            case "cardSet":
                spinner = (Spinner) findViewById(R.id.advanced_search_spinner_cardSet);
                for (Card.CARD_SET item : Card.CARD_SET.values()) {
                    list.add(Card.getSetName(item.toString()));
                }
                break;
        }

        SpinnerAdapter adapter = new CustomSpinnerAdapter(
                this, R.layout.spinner_dropdown, R.id.textView, list);

        assert spinner != null;
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (string) {
                    case "rarity":
                        _searchData.setRarity(parent.getSelectedItem().toString());
                        break;
                    case "cardSet":
                        String setName = parent.getSelectedItem().toString();
                        _searchData.setCardSet(Card.getSetCode(setName));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing
            }
        });
    }

    private void GenerateRangeSeekBar(final String string) {
        int id = 0;
        int idLeft = 0;
        int idRight = 0;

        // determine seek bar values to generate
        switch (string) {
            case "cost":
                id = R.id.advanced_search_layout_cost;
                idLeft = R.id.cost_min;
                idRight = R.id.cost_max;
                break;
            case "attack":
                id = R.id.advanced_search_layout_power;
                idLeft = R.id.attack_min;
                idRight = R.id.attack_max;
                break;
            case "health":
                id = R.id.advanced_search_layout_health;
                idLeft = R.id.health_min;
                idRight = R.id.health_max;
                break;
        }

        // edit text to the left of seek bar
        final TextView leftText = (TextView) findViewById(idLeft);
        leftText.setText("0");

        // edit text to the right of seek bar
        final TextView rightText = (TextView) findViewById(idRight);
        rightText.setText("7+");

        final RangeSeekBar<Integer> seekBar;

        try {
            // generate seek bar
            seekBar = new RangeSeekBar<>(0, 7, this);

            // adjust values according to thumb positions
            seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                @Override
                public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {

                    minValue = bar.getSelectedMinValue().intValue();
                    maxValue = bar.getSelectedMaxValue().intValue();

                    // set text to 7+ when upper range is 7
                    String max = (maxValue == 7) ? "7+" : maxValue.toString();

                    // handle changed range values
                    leftText.setText(minValue.toString());
                    rightText.setText(max);

                    // set search data values
                    switch (string) {
                        case "cost":
                            _searchData.setMaxCost(maxValue);
                            _searchData.setMinCost(minValue);
                            break;
                        case "attack":
                            _searchData.setMaxAttack(maxValue);
                            _searchData.setMinAttack(minValue);
                            break;
                        case "health":
                            _searchData.setMaxHealth(maxValue);
                            _searchData.setMinHealth(minValue);
                            break;
                    }
                }
            });

            // live display of min and max values
            seekBar.setNotifyWhileDragging(true);

            // add views to view group
            ViewGroup vg = (ViewGroup) findViewById(id);
            vg.addView(seekBar);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Seek bar failed to generate");
        }
    }
}