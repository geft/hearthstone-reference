package com.lifeof843.hearthstonereference.cards;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

public class Card {
    public static String getSetName(String setCode) {
        HashMap<String, String> map = getSetMap();
        return map.get(setCode);
    }

    public static String getSetCode(String setName) {
        HashMap<String, String> map = getSetMap();

        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue().equals(setName))
                return entry.getKey().toString();
        }

        return CARD_SET.ALL.toString();
    }

    @NonNull
    private static HashMap<String, String> getSetMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put(CARD_SET.ALL.toString(), "All");
        map.put(CARD_SET.CORE.toString(), "Basic");
        map.put(CARD_SET.BRM.toString(), "Blackrock Mountain");
        map.put(CARD_SET.EXPERT1.toString(), "Classic");
        map.put(CARD_SET.NAXX.toString(), "Curse of Naxxramas");
        map.put(CARD_SET.GVG.toString(), "Goblins vs Gnomes");
        map.put(CARD_SET.TGT.toString(), "The Grand Tournament");
        map.put(CARD_SET.LOE.toString(), "League of Explorers");
        map.put(CARD_SET.REWARDS.toString(), "Reward");
        map.put(CARD_SET.PROMO.toString(), "Promotion");
        map.put(CARD_SET.OG.toString(), "Whispers of the Old Gods");
        map.put(CARD_SET.KARA.toString(), "One Night in Karazhan");
        map.put(CARD_SET.GANGS.toString(), "Mean Streets of Gadgetzan");

        return map;
    }

    public enum CARD_SET {
        ALL("All"),
        CORE("CORE"),
        BRM("BRM"),
        EXPERT1("EXPERT1"),
        NAXX("NAXX"),
        GVG("GVG"),
        TGT("TGT"),
        LOE("LOE"),
        REWARDS("REWARDS"),
        PROMO("PROMO"),
        OG("OG"),
        KARA("KARA"),
        GANGS("GANGS");

        final String text;

        CARD_SET(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum RARITY {
        ALL("All"),
        FREE("Free"),
        COMMON("Common"),
        RARE("Rare"),
        EPIC("Epic"),
        LEGENDARY("Legendary");

        final String text;

        RARITY(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum CLASS {
        DRUID("DRUID"),
        HUNTER("HUNTER"),
        MAGE("MAGE"),
        PALADIN("PALADIN"),
        PRIEST("PRIEST"),
        ROGUE("ROGUE"),
        SHAMAN("SHAMAN"),
        WARRIOR("WARRIOR"),
        WARLOCK("WARLOCK"),
        NEUTRAL("NEUTRAL");

        final String text;

        CLASS(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

    }

    public enum PROP {
        CARD_SET("cardSet"),
        ID("id"),
        NAME("name"),
        TYPE("type"),
        COST("cost"),
        ATTACK("attack"),
        HEALTH("health"),
        MECHANICS("mechanics"),
        TEXT("text"),
        PLAYER_CLASS("playerClass"),
        RARITY("rarity"),
        DUST("dust"),
        HOW_TO_EARN("howToEarn"),
        HOW_TO_EARN_GOLDEN("howToEarnGolden"),
        FACTION("faction"),
        FLAVOR("flavor"),
        ARTIST("artist"),
        COLLECTIBLE("collectible"),
        RACE("race"),
        COLOR("color"),
        RARITY_ICON("rarityIcon");

        final String text;

        PROP(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }
}