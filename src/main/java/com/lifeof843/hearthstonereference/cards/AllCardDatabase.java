package com.lifeof843.hearthstonereference.cards;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.lifeof843.hearthstonereference.Utils;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class AllCardDatabase extends SQLiteAssetHelper {
    public static final int DB_VERSION = 14;
    public static final String DB_NAME = "cards.db";
    public static final String TABLE_NAME = "cards";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_COST = "cost";

    private static AllCardDatabase instance;
    private SQLiteDatabase database;

    public AllCardDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        setForcedUpgrade();
        database = getReadableDatabase();
    }

    public static AllCardDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new AllCardDatabase(context.getApplicationContext());
        }

        return instance;
    }

    @NonNull
    public String getCardId(String cardName) {
        cardName = Utils.removeLastCharacterIfNumeric(cardName);

        String cardId = "";

        Cursor cursor = database.query(
                TABLE_NAME,
                new String[]{COLUMN_ID},
                COLUMN_NAME + "=? AND " + COLUMN_COST + "!='NULL'",
                new String[]{cardName},
                null, null, null);

        if (cursor.moveToFirst()) {
            cardId = cursor.getString(0);
        }

        cursor.close();

        return cardId;
    }
}
