package com.lifeof843.hearthstonereference.cards;

import android.content.res.AssetFileDescriptor;

public class CardSound {
    private String path;
    private AssetFileDescriptor descriptor;

    public CardSound(String name, AssetFileDescriptor descriptor) {
        this.path = name;
        this.descriptor = descriptor;
    }

    public String getPath() {
        return path;
    }

    public AssetFileDescriptor getDescriptor() {
        return descriptor;
    }
}
