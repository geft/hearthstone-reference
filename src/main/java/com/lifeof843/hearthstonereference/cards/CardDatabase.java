package com.lifeof843.hearthstonereference.cards;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.Card.PROP;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class CardDatabase extends SQLiteAssetHelper {

    private static final int DATABASE_VERSION = 21;
    private static final String FILE_NAME = "cards.collectible.db";
    private static final String TABLE_NAME = "cards";
    private static CardDatabase instance;
    private SQLiteDatabase database;

    private CardDatabase(Context context) {
        super(context, FILE_NAME, null, DATABASE_VERSION);

        setForcedUpgrade();
        Initialize(context);
    }

    public static CardDatabase getInstance(Context context) {
        if (instance == null) {
            instance = new CardDatabase(context.getApplicationContext());
        }
        return instance;
    }

    private void Initialize(Context context) {
        OpenDatabase();
        AssignColors(context);
        AssignRarityIcons();
    }

    private void AssignRarityIcons() {
        ContentValues values = new ContentValues();

        Cursor cursor = getSelectAllCursor();

        addColumnToTable(cursor, PROP.RARITY_ICON);

        AssignRarityIconsHelper(
                values, Card.RARITY.COMMON.toString(), R.drawable.icon_rarity_common);
        AssignRarityIconsHelper(
                values, Card.RARITY.RARE.toString(), R.drawable.icon_rarity_rare);
        AssignRarityIconsHelper(
                values, Card.RARITY.EPIC.toString(), R.drawable.icon_rarity_epic);
        AssignRarityIconsHelper(
                values, Card.RARITY.LEGENDARY.toString(), R.drawable.icon_rarity_legendary);

        cursor.close();
    }

    private void addColumnToTable(Cursor cursor, PROP prop) {
        if (cursor.getColumnIndex(prop.toString()) == -1) {
            database.execSQL("ALTER TABLE " + TABLE_NAME +
                    " ADD COLUMN " + prop.toString() + " INTEGER;");
        }
    }

    private Cursor getSelectAllCursor() {
        return database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    private void AssignRarityIconsHelper(ContentValues values, String rarity, int rarityIcon) {
        values.clear();
        values.put(PROP.RARITY_ICON.toString(), rarityIcon);

        database.update(TABLE_NAME, values,
                PROP.RARITY.toString() + "=? AND " + PROP.CARD_SET.toString() + "!=?",
                new String[]{rarity, Card.CARD_SET.CORE.toString()});
    }

    private void AssignColors(Context context) {
        ContentValues values = new ContentValues();

        Cursor cursor = getSelectAllCursor();

        addColumnToTable(cursor, PROP.COLOR);

        AssignColorsHelper(context, values, Card.CLASS.DRUID.toString(), R.color.druid);
        AssignColorsHelper(context, values, Card.CLASS.MAGE.toString(), R.color.mage);
        AssignColorsHelper(context, values, Card.CLASS.HUNTER.toString(), R.color.hunter);
        AssignColorsHelper(context, values, Card.CLASS.PALADIN.toString(), R.color.paladin);
        AssignColorsHelper(context, values, Card.CLASS.PRIEST.toString(), R.color.priest);
        AssignColorsHelper(context, values, Card.CLASS.ROGUE.toString(), R.color.rogue);
        AssignColorsHelper(context, values, Card.CLASS.SHAMAN.toString(), R.color.shaman);
        AssignColorsHelper(context, values, Card.CLASS.WARLOCK.toString(), R.color.warlock);
        AssignColorsHelper(context, values, Card.CLASS.WARRIOR.toString(), R.color.warrior);
        AssignColorsHelper(context, values, Card.CLASS.NEUTRAL.toString(),
                android.R.color.transparent);

        cursor.close();
    }

    private void AssignColorsHelper(
            Context context, ContentValues values, String playerClass, int color) {
        values.clear();
        values.put(PROP.COLOR.toString(), ContextCompat.getColor(context, color));

        database.update(TABLE_NAME, values, PROP.PLAYER_CLASS.toString() + "=?",
                new String[]{playerClass});
    }

    private void OpenDatabase() {
        database = getWritableDatabase();
    }

    public ArrayList<String> getFullList() {
        Cursor cursor = database.rawQuery(
                "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                        " ORDER BY " + PROP.NAME.toString() + " ASC", null);

        return getArrayList(cursor);
    }

    @NonNull
    private ArrayList<String> getArrayList(Cursor cursor) {
        ArrayList<String> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();
        return list;
    }

    public ArrayList<String> getCardsByPropertyValue(PROP prop, String value) {
        Cursor cursor;
        if (value.equals(Card.RARITY.COMMON.toString())) {
            cursor = database.rawQuery(
                    "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                            " WHERE " + prop.toString() + "=? AND " + PROP.CARD_SET.toString() + "!=?",
                    new String[]{value, Card.CARD_SET.CORE.toString()});
        } else {
            cursor = database.rawQuery(
                    "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                            " WHERE " + prop.toString() + "=?", new String[]{value});
        }

        return getArrayList(cursor);
    }

    public ArrayList<String> filterByNameAndTextAndRace(String query) {
        query = query.replace("\'", "");
        query = query.replace("\"", "");
        query = query.replace("\\", "");

        Cursor cursor = database.rawQuery(
                "select name from cards where name like '%"
                        + query + "%' or race like '%"
                        + query + "%' or text like '%"
                        + query + "%'", null);

        return getArrayList(cursor);
    }

    public ArrayList<String> filterByEnabledClass(ArrayList<String> enabledClass) {
        ArrayList<String> list = new ArrayList<>();

        if (enabledClass == null) {
            return getFullList();
        } else {
            Cursor cursor = getSelectAllCursor();
            while (cursor.moveToNext()) {
                if (isPlayerClassEnabled(enabledClass, cursor)) {
                    list.add(getStringFromColumnIndex(cursor, PROP.NAME));
                }
            }
            cursor.close();
        }
        return list;
    }

    public ArrayList<String> filterByIntRange(PROP property, int min, int max) {
        ArrayList<String> list = new ArrayList<>();

        Cursor cursor = database.rawQuery(
                "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                        " WHERE (" + property.toString() + ">=? AND " + property.toString() + "<=?)" +
                        " OR " + property.toString() + "='NULL'",
                new String[]{Integer.toString(min), Integer.toString(max)});

        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public ArrayList<String> removeSpellsFromList(ArrayList<String> list) {
        ArrayList<String> listCopy = new ArrayList<>(list);

        Cursor cursor = database.rawQuery(
                "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                        " WHERE " + PROP.ATTACK.toString() + "='NULL'", null);

        while (cursor.moveToNext()) {
            listCopy.remove(cursor.getString(0));
        }
        cursor.close();

        return listCopy;
    }

    private boolean isPlayerClassEnabled(ArrayList<String> enabledClass, Cursor cursor) {
        return enabledClass.contains(
                getStringFromColumnIndex(cursor, PROP.PLAYER_CLASS)) ||
                (enabledClass.contains(Card.CLASS.NEUTRAL.toString()) &&
                        getStringFromColumnIndex(cursor, PROP.PLAYER_CLASS).equalsIgnoreCase("NULL"));
    }

    private String getStringFromColumnIndex(Cursor cursor, PROP property) {
        return cursor.getString(cursor.getColumnIndex(property.toString()));
    }

    public boolean isCardLegendary(String cardName) {
        String rarity = getProperty(cardName, PROP.RARITY);

        return rarity.equalsIgnoreCase(Card.RARITY.LEGENDARY.toString());
    }

    /**
     * Obtain a property pre-defined by the global enum PROP declared in this class.
     * The result is only obtained if the card returned is collectible and has a valid cost.
     *
     * @param cardName Name of the card to be searched.
     * @param propEnum Property to be obtained through enum PROP
     * @return The resulting property value. All values are Strings.
     */
    public String getProperty(String cardName, PROP propEnum) {
        if (cardName == null) {
            return "";
        }

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + TABLE_NAME + " WHERE name=?", new String[]{cardName});

        String result = null;
        String property = propEnum.toString();

        if (cursor.moveToNext()) {
            result = getStringFromProperty(propEnum, cursor, property);
        }
        cursor.close();

        return result;
    }

    public int getIntProperty(String cardName, PROP prop) {
        int result = 0;
        Cursor cursor = database.rawQuery(
                "SELECT " + prop.toString() + " FROM " + TABLE_NAME + " WHERE name=?",
                new String[]{cardName});

        if (cursor.moveToFirst()) {
            result = cursor.getInt(0);
        }
        cursor.close();

        return result;
    }

    private String getStringFromProperty(PROP propEnum, Cursor cursor, String property) {
        String result;

        if (property.equals(PROP.PLAYER_CLASS.toString())) {
            String currClass = cursor.getString(cursor.getColumnIndex(property));
            result = getStringFromPlayerClassProperty(currClass);
        } else if (property.equals(PROP.FLAVOR.toString())) {
            result = getStringFromFlavorProperty(propEnum, cursor);
        } else {
            result = cursor.getString(cursor.getColumnIndex(property));
        }
        return result;
    }

    private String getStringFromFlavorProperty(PROP propEnum, Cursor cursor) {
        return getStringFromColumnIndex(cursor, propEnum).replaceAll("(<i>|</i>)", "");
    }

    private String getStringFromPlayerClassProperty(String currClass) {
        return currClass.equals("NULL") ? Card.CLASS.NEUTRAL.toString() : currClass;
    }

    public ColorDrawable GetColor(String cardName) {
        ColorDrawable colorDrawable = null;

        Cursor cursor = database.rawQuery("SELECT " + PROP.COLOR.toString() +
                " FROM " + TABLE_NAME +
                " WHERE " + PROP.NAME.toString() + "=?", new String[]{cardName});

        if (cursor.moveToFirst()) {
            colorDrawable = new ColorDrawable(cursor.getInt(0));
        }
        cursor.close();

        return colorDrawable;
    }

    public int getRarityIcon(String cardName) {
        int result = 0;

        Cursor cursor = database.rawQuery("SELECT " + PROP.RARITY_ICON.toString() +
                " FROM " + TABLE_NAME +
                " WHERE " + PROP.NAME.toString() + "=?", new String[]{cardName});
        if (cursor.moveToFirst()) {
            result = cursor.getInt(0);
        }
        cursor.close();

        return result;
    }

    public int getMaxPropertyValue(String property) {
        Cursor cursor = database.rawQuery("SELECT " + property + " FROM " + TABLE_NAME +
                " ORDER BY " + property + " DESC", null);

        int maxValue = 0;

        while (cursor.moveToNext()) {
            if (cursor.getInt(0) != 0) {
                maxValue = cursor.getInt(0);
                break;
            }
        }
        cursor.close();

        return maxValue;
    }

    public ArrayList<String> getListSortedByCost(ArrayList<String> currList) {
        ArrayList<String> list = new ArrayList<>();

        Cursor cursor = database.rawQuery(
                "SELECT " + PROP.NAME + "," + PROP.COST + " FROM " + TABLE_NAME +
                        " ORDER BY " + PROP.COST + " ASC", null);

        while (cursor.moveToNext()) {
            String name = cursor.getString(0);
            if (currList.contains(name)) {
                list.add(name);
            }
        }
        cursor.close();

        return list;
    }

    public ArrayList<String> getPlayerClassCards(String playerClass) {
        ArrayList<String> list = new ArrayList<>();

        Cursor cursor;

        if (playerClass.equals(Card.CLASS.NEUTRAL.toString())) {
            cursor = database.rawQuery(
                    "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                            " WHERE " + PROP.PLAYER_CLASS + "='NULL'", null);
        } else {
            cursor = database.rawQuery(
                    "SELECT " + PROP.NAME.toString() + " FROM " + TABLE_NAME +
                            " WHERE " + PROP.PLAYER_CLASS + "=?", new String[]{playerClass});
        }

        while (cursor.moveToNext()) {
            list.add(cursor.getString(0));
        }
        cursor.close();

        return list;
    }

    public String getCardID(String cardName) {
        Cursor cursor = database.rawQuery(
                "SELECT " + PROP.ID.toString() + " FROM " + TABLE_NAME +
                        " WHERE " + PROP.NAME.toString() + "=?", new String[]{cardName});

        String id = "";
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        } else {
            Log.e(this.toString(), "Card ID for " + cardName + " not found");
        }
        cursor.close();

        return id;
    }
}
