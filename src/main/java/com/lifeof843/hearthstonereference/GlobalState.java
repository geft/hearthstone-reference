package com.lifeof843.hearthstonereference;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.util.Log;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.instabug.library.IBGInvocationEvent;
import com.instabug.library.Instabug;
import com.lifeof843.hearthstonereference.cards.CardDatabase;
import com.lifeof843.hearthstonereference.decks.DeckDatabase;
import com.lifeof843.hearthstonereference.download.APKExpansionHelper;

import java.io.File;
import java.io.IOException;
import java.util.TreeSet;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class GlobalState extends Application {

    private static ZipResourceFile zipResourceFile;
    private static TreeSet<String> zipEntries;

    public static ZipResourceFile getZipResourceFile() {
        return zipResourceFile;
    }

    public static void setZipResourceFile(ZipResourceFile file) {
        zipResourceFile = file;
    }

    public static TreeSet<String> getZipEntries() {
        if (zipEntries == null) zipEntries = new TreeSet<>();

        return zipEntries;
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return permissionCheck == PermissionChecker.PERMISSION_GRANTED;
    }

    public static void initZipEntries() {
        if (zipEntries == null) {
            zipEntries = new TreeSet<>();

            if (zipResourceFile != null) {
                ZipResourceFile.ZipEntryRO[] entryROs = zipResourceFile.getAllEntries();
                for (ZipResourceFile.ZipEntryRO entryRO : entryROs) {
                    zipEntries.add(entryRO.mFileName);
                }
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initFabric();
        initInstabug();
        initCustomFont();
        initDatabases();
        initObbClear();
        initExpansionFiles();
    }

    private void initInstabug() {
        if (BuildConfig.DEBUG) {
            new Instabug.Builder(this, "bb4a723eb87b5b4fa73c846f1efa8fcf")
                    .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                    .build();
        } else {
            new Instabug.Builder(this, "47a87f7d649cec41a668bc0ff24c00d9")
                    .setInvocationEvent(IBGInvocationEvent.IBGInvocationEventShake)
                    .build();
        }
    }

    private void initFabric() {
        CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build());
    }

    private void initObbClear() {
        String path = Environment.getExternalStorageDirectory().getPath()
                + "/Android/obb/"
                + getPackageName();

        String expPath = path
                + "/main."
                + APKExpansionHelper.EXT_VERSION_MAIN
                + "."
                + getPackageName()
                + ".obb";

        File dir = new File(path);

        try {
            if (dir.exists()) {
                File[] files = dir.listFiles();

                if (files != null) {
                    for (File file : files) {
                        if (!file.getPath().equalsIgnoreCase(expPath)) {
                            file.delete();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }
    }

    private void initDatabases() {
        CardDatabase.getInstance(getApplicationContext());
        DeckDatabase.getInstance(getApplicationContext());
    }

    private void initCustomFont() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/belwe.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    private void initExpansionFiles() {
        try {
            zipResourceFile = APKExpansionSupport.getAPKExpansionZipFile(
                    getApplicationContext(),
                    APKExpansionHelper.EXT_VERSION_MAIN,
                    APKExpansionHelper.EXT_VERSION_PATCH);

        } catch (IOException e) {
            Log.e(this.toString(), "Expansion file not found");
        }
    }
}
