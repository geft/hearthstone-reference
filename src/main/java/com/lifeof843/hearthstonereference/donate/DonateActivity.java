package com.lifeof843.hearthstonereference.donate;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.databinding.ActivityDonateBinding;

/**
 * Created by Gerry on 07/02/2017.
 */

public class DonateActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityDonateBinding binding;
    private DonateBilling billing;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_donate);
        binding.setOnClick(this);

        billing = new DonateBilling(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billing.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(binding.donateAction)) {
            performDonation();
        }
    }

    @Override
    protected void onDestroy() {
        billing.release();

        super.onDestroy();
    }

    private void performDonation() {
        billing.purchase(this);
    }
}
