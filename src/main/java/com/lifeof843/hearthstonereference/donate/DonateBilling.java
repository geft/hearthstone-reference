package com.lifeof843.hearthstonereference.donate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.lifeof843.hearthstonereference.R;

/**
 * Created by Gerry on 08/02/2017.
 */

class DonateBilling implements BillingProcessor.IBillingHandler {

    private static final String ID_DONATION = "donation";

    private Context context;
    private BillingProcessor processor;

    DonateBilling(Context context) {
        this.context = context;
        this.processor = new BillingProcessor(context, context.getString(R.string.donate_api), this);
    }

    void purchase(Activity activity) {
        if (isPlayStoreSupported()) {
            processor.purchase(activity, ID_DONATION);
        } else {
            showUnsupportedError();
        }
    }

    private boolean isPlayStoreSupported() {
        return BillingProcessor.isIabServiceAvailable(context) && processor.isOneTimePurchaseSupported();
    }

    private void showUnsupportedError() {
        Toast.makeText(context, context.getString(R.string.donate_unsupported), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        processor.consumePurchase(ID_DONATION);

        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.donate_dialog_title))
                .setMessage(context.getString(R.string.donate_dialog_content))
                .setNeutralButton(context.getString(R.string.donate_dialog_action),
                        (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    @Override
    public void onPurchaseHistoryRestored() {
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
    }

    @Override
    public void onBillingInitialized() {
    }

    boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return processor.handleActivityResult(requestCode, resultCode, data);
    }

    void release() {
        if (processor != null) processor.release();
    }
}
