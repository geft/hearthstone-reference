package com.lifeof843.hearthstonereference;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Gerry on 09/02/2017.
 */

public class PrefHandler {

    private static final String PREF_NAME = "MAIN";
    private static final String ID_NOTICE = "NOTICE";

    private Context context;

    public PrefHandler(Context context) {
        this.context = context;
    }

    private SharedPreferences getPref() {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        return getPref().edit();
    }

    public boolean isNoticeShown() {
        return getPref().getBoolean(ID_NOTICE, false);
    }

    public void setNoticeShown(boolean isShown) {
        getEditor().putBoolean(ID_NOTICE, isShown).apply();
    }
}
