package com.lifeof843.hearthstonereference.gallery;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.utility.FormatManager;

/**
 * Created on 05/05/2016.
 */
public class GalleryCreditsPageFragment extends GalleryCardBackPageFragment {

    String creditName;

    @Override
    protected void getBundle() {
        if (getArguments() != null) {
            creditName = getArguments().getString("creditName");
        }
    }

    @Override
    protected void displayContent() {
        Drawable drawable = fileManager.getDrawableFromPath(FormatManager.getCreditsPath(creditName));
        ImageView imageView = (ImageView) view.findViewById(R.id.imageCardBack);
        imageView.setImageDrawable(drawable);
    }
}
