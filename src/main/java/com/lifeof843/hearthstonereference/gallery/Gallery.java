package com.lifeof843.hearthstonereference.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.gallery.adventure.GalleryBRM;
import com.lifeof843.hearthstonereference.gallery.adventure.GalleryKAR;
import com.lifeof843.hearthstonereference.gallery.adventure.GalleryLOE;
import com.lifeof843.hearthstonereference.gallery.adventure.GalleryNAXX;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Gallery extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        initScrollView();
        initButtons();
    }

    private void initScrollView() {
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        scrollView.setVerticalScrollBarEnabled(false);
    }

    private void initButtons() {
        initCredits();
        initTutorial();
        initCardBacks();
        initNaxx();
        initNaxxHeroic();
        initBRM();
        initBRMHeroic();
        initLOE();
        initLOEHeroic();
        initKAR();
        initKARHeroic();
    }

    private void initCardBacks() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonCardBacks);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, GalleryCardBackBrowser.class);
                startActivity(intent);
            }
        });
    }

    private void initLOE() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonLOE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryLOE(Gallery.this, false);
            }
        });
    }

    private void initLOEHeroic() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonLOEH);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryLOE(Gallery.this, true);
            }
        });
    }

    private void initKAR() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonKAR);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GalleryKAR(Gallery.this, false);
            }
        });
    }

    private void initKARHeroic() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonKARH);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GalleryKAR(Gallery.this, true);
            }
        });
    }

    private void initBRM() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonBRM);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryBRM(Gallery.this, false);
            }
        });
    }

    private void initBRMHeroic() {
        ImageButton button = (ImageButton) findViewById(R.id.buttonBRMH);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryBRM(Gallery.this, true);
            }
        });
    }

    private void initNaxx() {
        ImageButton button = getButtonFromId(R.id.buttonNAXX);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryNAXX(Gallery.this, false);
            }
        });
    }

    private void initNaxxHeroic() {
        ImageButton button = getButtonFromId(R.id.buttonNAXXH);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryNAXX(Gallery.this, true);
            }
        });
    }

    private void initTutorial() {
        ImageButton button = getButtonFromId(R.id.buttonTutorial);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GalleryTutorial(Gallery.this);
            }
        });
    }

    private void initCredits() {
        ImageButton button = getButtonFromId(R.id.buttonCredits);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, GalleryCredits.class);
                startActivity(intent);
            }
        });
    }

    private ImageButton getButtonFromId(int resId) {
        return (ImageButton) findViewById(resId);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
