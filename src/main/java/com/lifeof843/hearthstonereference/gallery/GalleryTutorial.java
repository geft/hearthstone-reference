package com.lifeof843.hearthstonereference.gallery;

import android.content.Context;

import com.lifeof843.hearthstonereference.gallery.adventure.Boss;
import com.lifeof843.hearthstonereference.gallery.adventure.BossGallery;

import java.util.ArrayList;

public class GalleryTutorial extends BossGallery {

    public static final String HOGGER = "Hogger";
    public static final String MILLHOUSE_MANASTORM = "Millhouse Manastorm";
    public static final String LOREWALKER_CHO = "Lorewalker Cho";
    public static final String KING_MUKLA = "King Mukla";
    public static final String HEMET_NESINGWARY = "Hemet Nesingwary";
    public static final String ILLIDAN_STORMRAGE = "Illidan Stormrage";

    public GalleryTutorial(Context context) {
        super(context, false);
    }

    @Override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(1, HOGGER));
        list.add(getBoss(2, MILLHOUSE_MANASTORM));
        list.add(getBoss(3, LOREWALKER_CHO));
        list.add(getBoss(4, KING_MUKLA));
        list.add(getBoss(5, HEMET_NESINGWARY));
        list.add(getBoss(6, ILLIDAN_STORMRAGE));
    }

    @Override
    protected void setRelatedConditions(String name, ArrayList<String> list) {
        switch (name) {
            case HOGGER:
                list.add("Gnoll");
                list.add("Hogger SMASH!");
                list.add("Massive Gnoll");
                list.add("Riverpaw Gnoll");
                break;
            case KING_MUKLA:
                list.add("Bananas");
                list.add("Barrel");
                list.add("Barrel Toss");
                list.add("Crazy Monkey");
                list.add("Hidden Gnome");
                list.add("Mukla's Big Brother");
                list.add("Stomp");
                list.add("Will of Mukla");
                break;
            case HEMET_NESINGWARY:
                list.add("Crazed Hunter");
                break;
            case ILLIDAN_STORMRAGE:
                list.add("Dual Warglaives");
                list.add("Flame Burst");
                list.add("Flame of Azzinoth");
                list.add("Naga Myrmidon");
                list.add("Warglaive of Azzinoth");
                break;
            case LOREWALKER_CHO:
                list.add("Brewmaster");
                list.add("Legacy of the Emperor");
                list.add("Pandaren Scout");
                list.add("Shado-Pan Monk");
                list.add("Transcendence");
                break;
        }
    }
}
