package com.lifeof843.hearthstonereference.gallery;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;

public class GalleryViewer extends FragmentActivity {

    private int numPages;
    private ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_viewer_slider);

        if (savedInstanceState != null) {
            list = savedInstanceState.getStringArrayList("list");
        }

        initialize();
    }

    private void initialize() {
        list = getIntent().getExtras().getStringArrayList("list");

        if (list != null) {
            numPages = list.size();
        } else {
            numPages = 0;
        }

        setPagerAdapter();
    }

    private void setPagerAdapter() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new GalleryViewerPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setFadeAnimation();
    }

    private void setFadeAnimation() {
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private String getIdFromPosition(int position) {
        return list.get(position);
    }

    private Fragment getPageWithPosition(int position, GalleryViewerPageFragment page) {
        Bundle bundle = new Bundle();
        bundle.putString("id", getIdFromPosition(position));
        page.setArguments(bundle);

        return page;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putStringArrayList("list", list);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private class GalleryViewerPagerAdapter extends FragmentStatePagerAdapter {
        public GalleryViewerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            GalleryViewerPageFragment page = new GalleryViewerPageFragment();
            return getPageWithPosition(position, page);
        }

        @Override
        public int getCount() {
            return numPages;
        }
    }
}
