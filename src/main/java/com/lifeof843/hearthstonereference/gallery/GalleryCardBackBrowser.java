package com.lifeof843.hearthstonereference.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GalleryCardBackBrowser extends AppCompatActivity {

    private CardBackManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_card_back_browser);

        initScrollView();
        initButtons();
    }

    private void initScrollView() {
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

        if (scrollView != null) {
            scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            scrollView.setVerticalScrollBarEnabled(false);
        }
    }

    private void initButtons() {
        initCardBackManager();
        init2014();
        init2015();
        init2016();
        initMoney();
        initPromo();
        initRewards();
    }

    private void initCardBackManager() {
        manager = new CardBackManager();
    }

    private void init2014() {
        initCardBackButton(R.id.button2014, manager.get2014());
    }

    private void init2015() {
        initCardBackButton(R.id.button2015, manager.get2015());
    }

    private void init2016() {
        initCardBackButton(R.id.button2016, manager.get2016());
    }

    private void initMoney() {
        initCardBackButton(R.id.buttonMoney, manager.getMoney());
    }

    private void initPromo() {
        initCardBackButton(R.id.buttonPromo, manager.getPromo());
    }

    private void initRewards() {
        initCardBackButton(R.id.buttonRewards, manager.getRewards());
    }

    private void initCardBackButton(int buttonId, ArrayList<CardBack> cardBacks) {
        ImageButton button = getButtonFromId(buttonId);
        setCardBackButtonListener(cardBacks, button);
    }

    private void setCardBackButtonListener(final ArrayList<CardBack> cardBacks, ImageButton button) {
        button.setOnClickListener(v -> {
            Intent intent = new Intent(GalleryCardBackBrowser.this, GalleryCardBack.class);
            intent.putParcelableArrayListExtra("cardBacks", cardBacks);
            startActivity(intent);
        });
    }

    private ImageButton getButtonFromId(int resId) {
        return (ImageButton) findViewById(resId);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
