package com.lifeof843.hearthstonereference.gallery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GalleryCredits extends FragmentActivity {

    int numPages;
    private List<String> names;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_card_back);

        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setFadeAnimation();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init() {
        names = getNames();
        numPages = names.size();
        setPagerAdapter();
    }

    private void setPagerAdapter() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new CreditsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    private void setFadeAnimation() {
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    protected List<String> getNames() {
        List<String> names = new ArrayList<>();
        names.add("andy-brock");
        names.add("becca-abel");
        names.add("ben-brode");
        names.add("ben-thompson");
        names.add("beomki-hong");
        names.add("bob-fitch");
        names.add("brian-birmingham");
        names.add("brian-schwab");
        names.add("bryan-chang");
        names.add("cameron-chrisman");
        names.add("christopher-yim");
        names.add("dean-ayala");
        names.add("derek-sakamoto");
        names.add("elizabeth-cho");
        names.add("eric-del-priore");
        names.add("eric-dodds");
        names.add("hamilton-chu");
        names.add("henry-ho");
        names.add("he-rim-woo");
        names.add("jason-chayes");
        names.add("jason-macallister");
        names.add("jay-baxter");
        names.add("jc-park");
        names.add("jeremy-cranford");
        names.add("jerry-mascho");
        names.add("jomaro-kindred");
        names.add("jonas-laster");
        names.add("keith-landes");
        names.add("kyle-harrison");
        names.add("max-ma");
        names.add("max-mccall");
        names.add("michael-schweitzer");
        names.add("mike-donais");
        names.add("rachelle-davis");
        names.add("ricardo-robaina");
        names.add("robin-fredericksen");
        names.add("rob-pardo");
        names.add("ryan-chew");
        names.add("ryan-masterson");
        names.add("seyil-yoon");
        names.add("steven-gabriel");
        names.add("tim-erskine");
        names.add("walter-kong");
        names.add("yong-woo");
        names.add("zwick");

        return names;
    }

    private Fragment getPageWithPosition(int position, GalleryCardBackPageFragment page) {
        Bundle bundle = new Bundle();
        bundle.putString("creditName", names.get(position));
        page.setArguments(bundle);

        return page;
    }

    private class CreditsAdapter extends FragmentStatePagerAdapter {
        public CreditsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            GalleryCreditsPageFragment page = new GalleryCreditsPageFragment();
            return getPageWithPosition(position, page);
        }

        @Override
        public int getCount() {
            return numPages;
        }

    }
}
