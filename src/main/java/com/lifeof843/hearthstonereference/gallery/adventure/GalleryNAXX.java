package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;

import java.util.ArrayList;

public class GalleryNAXX extends BossGallery {

    public static final String ANUBREKHAN = "Anub'rekhan";
    public static final String GRAND_WIDOW_FAERLINA = "Grand Widow Faerlina";
    public static final String MAEXXNA = "Maexxna";
    public static final String NOTH_THE_PLAGUEBRINGER = "Noth the Plaguebringer";
    public static final String HEIGAN_THE_UNCLEAN = "Heigan the Unclean";
    public static final String LOATHEB = "Loatheb";
    public static final String INSTRUCTOR_RAZUVIOUS = "Instructor Razuvious";
    public static final String GOTHIK_THE_HARVESTER = "Gothik the Harvester";
    public static final String THE_FOUR_HORSEMEN = "The Four Horsemen";
    public static final String PATCHWERK = "Patchwerk";
    public static final String GROBBULUS = "Grobbulus";
    public static final String GLUTH = "Gluth";
    public static final String THADDIUS = "Thaddius";
    public static final String SAPPHIRON = "Sapphiron";
    public static final String KELTHUZAD = "Kel'thuzad";

    public static final String THE_ARACHNID_QUARTER = "The Arachnid Quarter";
    public static final String THE_PLAGUE_QUARTER = "The Plague Quarter";
    public static final String THE_MILITARY_QUARTER = "The Military Quarter";
    public static final String THE_CONSTRUCT_QUARTER = "The Construct Quarter";
    public static final String FROSTWYRM_LAIR = "Frostwyrm Lair";

    public GalleryNAXX(Context context, boolean isHeroic) {
        super(context, isHeroic);
    }

    @Override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(THE_ARACHNID_QUARTER, 1, ANUBREKHAN + HEROIC));
        list.add(getBoss(THE_ARACHNID_QUARTER, 2, GRAND_WIDOW_FAERLINA + HEROIC));
        list.add(getBoss(THE_ARACHNID_QUARTER, 3, MAEXXNA + HEROIC));

        list.add(getBoss(THE_PLAGUE_QUARTER, 4, NOTH_THE_PLAGUEBRINGER + HEROIC));
        list.add(getBoss(THE_PLAGUE_QUARTER, 5, HEIGAN_THE_UNCLEAN + HEROIC));
        list.add(getBoss(THE_PLAGUE_QUARTER, 6, LOATHEB + HEROIC));

        list.add(getBoss(THE_MILITARY_QUARTER, 7, INSTRUCTOR_RAZUVIOUS + HEROIC));
        list.add(getBoss(THE_MILITARY_QUARTER, 8, GOTHIK_THE_HARVESTER + HEROIC));
        list.add(getBoss(THE_MILITARY_QUARTER, 9, THE_FOUR_HORSEMEN + HEROIC));

        list.add(getBoss(THE_CONSTRUCT_QUARTER, 10, PATCHWERK + HEROIC));
        list.add(getBoss(THE_CONSTRUCT_QUARTER, 11, GROBBULUS + HEROIC));
        list.add(getBoss(THE_CONSTRUCT_QUARTER, 12, GLUTH + HEROIC));
        list.add(getBoss(THE_CONSTRUCT_QUARTER, 13, THADDIUS + HEROIC));

        list.add(getBoss(FROSTWYRM_LAIR, 14, SAPPHIRON + HEROIC));
        list.add(getBoss(FROSTWYRM_LAIR, 15, KELTHUZAD + HEROIC));
    }

    @Override
    protected void setRelatedConditions(String name, ArrayList<String> list) {
        if (HEROIC.isEmpty()) {
            /*
            NORMAL
             */
            switch (name) {
                case ANUBREKHAN:
                    list.add("Nerubian1");
                    list.add("Locust Swarm");
                    break;
                case GRAND_WIDOW_FAERLINA:
                    list.add("Worshipper");
                    list.add("Necroknight");
                    break;
                case MAEXXNA:
                    list.add("Necrotic Poison");
                    break;
                case NOTH_THE_PLAGUEBRINGER:
                    list.add("Skeleton2");
                    list.add("Plague");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case HEIGAN_THE_UNCLEAN:
                    list.add("Mindpocalypse");
                    break;
                case LOATHEB:
                    list.add("Deathbloom");
                    list.add("Spore");
                    list.add("Sporeburst");
                    break;
                case INSTRUCTOR_RAZUVIOUS:
                    list.add("Understudy");
                    list.add("Massive Runeblade");
                    list.add("Mind Control Crystal");
                    list.add("Necroknight");
                    break;
                case GOTHIK_THE_HARVESTER:
                    list.add("Spectral Trainee");
                    list.add("Spectral Warrior");
                    list.add("Spectral Rider");
                    list.add("Unrelenting Trainee");
                    list.add("Unrelenting Warrior");
                    list.add("Unrelenting Rider");
                    break;
                case THE_FOUR_HORSEMEN:
                    list.add("Lady Blaumeux");
                    list.add("Thane Korth'azz");
                    list.add("Sir Zeliek");
                    list.add("Runeblade");
                    list.add("Mark of the Horsemen");
                    break;
                case PATCHWERK:
                    list.add("Hook");
                    break;
                case GROBBULUS:
                    list.add("Fallout Slime");
                    list.add("Mutating Injection");
                    break;
                case GLUTH:
                    list.add("Jaws");
                    list.add("Enrage");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case THADDIUS:
                    list.add("Supercharge");
                    list.add("Feugen");
                    list.add("Stalagg");
                    break;
                case SAPPHIRON:
                    list.add("Frozen Champion");
                    list.add("Pure Cold");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case KELTHUZAD:
                    list.add("Guardian of Icecrown");
                    list.add("Mr. Bigglesworth");
                    list.add("Skeletal Smith");
                    break;
            }
        } else {
            name = name.replace(HEROIC, "");
            /*
            HEROIC
             */
            switch (name) {
                case ANUBREKHAN:
                    list.add("Nerubian2");
                    list.add("Locust Swarm");
                    break;
                case GRAND_WIDOW_FAERLINA:
                    list.add("Worshipper1");
                    list.add("Necroknight");
                    break;
                case MAEXXNA:
                    list.add("Necrotic Poison");
                    break;
                case NOTH_THE_PLAGUEBRINGER:
                    list.add("Skeleton");
                    list.add("Plague");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case HEIGAN_THE_UNCLEAN:
                    list.add("Mindpocalypse");
                    break;
                case LOATHEB:
                    list.add("Deathbloom");
                    list.add("Spore");
                    list.add("Sporeburst");
                    break;
                case INSTRUCTOR_RAZUVIOUS:
                    list.add("Understudy");
                    list.add("Massive Runeblade1");
                    list.add("Mind Control Crystal");
                    list.add("Necroknight");
                    break;
                case GOTHIK_THE_HARVESTER:
                    list.add("Spectral Trainee");
                    list.add("Spectral Warrior");
                    list.add("Spectral Rider");
                    list.add("Unrelenting Trainee");
                    list.add("Unrelenting Warrior");
                    list.add("Unrelenting Rider");
                    break;
                case THE_FOUR_HORSEMEN:
                    list.add("Lady Blaumeux1");
                    list.add("Thane Korth'azz1");
                    list.add("Sir Zeliek1");
                    list.add("Runeblade1");
                    list.add("Mark of the Horsemen");
                    break;
                case PATCHWERK:
                    list.add("Hook1");
                    break;
                case GROBBULUS:
                    list.add("Fallout Slime");
                    list.add("Mutating Injection");
                    break;
                case GLUTH:
                    list.add("Jaws1");
                    list.add("Enrage");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case THADDIUS:
                    list.add("Supercharge");
                    list.add("Feugen");
                    list.add("Stalagg");
                    break;
                case SAPPHIRON:
                    list.add("Frozen Champion");
                    list.add("Pure Cold");
                    list.add("Skeletal Smith");
                    list.add("Necroknight");
                    break;
                case KELTHUZAD:
                    list.add("Guardian of Icecrown1");
                    list.add("Mr. Bigglesworth");
                    list.add("Skeletal Smith");
                    break;
            }
        }
    }
}
