package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;

import java.util.ArrayList;

public class GalleryBRM extends BossGallery {

    public static final String COREN_DIREBREW = "Coren Direbrew";
    public static final String GRIMSTONE = "High Justice Grimstone";
    public static final String THAURISSAN = "Emperor Thaurissan";
    public static final String GARR = "Garr";
    public static final String BARON_GEDDON = "Baron Geddon";
    public static final String MAJORDOMO = "Majordomo Executus";
    public static final String OMOKK = "Highlord Omokk";
    public static final String DRAKKISATH = "General Drakkisath";
    public static final String REND_BLACKHAND = "Rend Blackhand";
    public static final String RAZORGORE = "Razorgore the Untamed";
    public static final String VAELASTRASZ = "Vaelastrasz the Corrupt";
    public static final String CHROMAGGUS = "Chromaggus";
    public static final String VICTOR_NEFARIUS = "Lord Victor Nefarius";
    public static final String OMNOTRON = "Omnotron Defense System";
    public static final String MALORIAK = "Maloriak";
    public static final String ATRAMEDES = "Atramedes";
    public static final String NEFARIAN = "Nefarian";

    public static final String BLACKROCK_DEPTHS = "Blackrock Depths";
    public static final String BLACKROCK_SPIRE = "Blackrock Spire";
    public static final String MOLTEN_CORE = "Molten Core";
    public static final String BLACKWING_LAIR = "Blackwing Lair";
    public static final String HIDDEN_LABORATORY = "The Hidden Laboratory";

    public GalleryBRM(Context context, boolean isHeroic) {
        super(context, isHeroic);
    }

    @Override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(BLACKROCK_DEPTHS, 1, COREN_DIREBREW + HEROIC));
        list.add(getBoss(BLACKROCK_DEPTHS, 2, GRIMSTONE + HEROIC));
        list.add(getBoss(BLACKROCK_DEPTHS, 3, THAURISSAN + HEROIC));

        list.add(getBoss(MOLTEN_CORE, 4, GARR + HEROIC));
        list.add(getBoss(MOLTEN_CORE, 5, BARON_GEDDON + HEROIC));
        list.add(getBoss(MOLTEN_CORE, 6, MAJORDOMO + HEROIC));

        list.add(getBoss(BLACKROCK_SPIRE, 7, OMOKK + HEROIC));
        list.add(getBoss(BLACKROCK_SPIRE, 8, DRAKKISATH + HEROIC));
        list.add(getBoss(BLACKROCK_SPIRE, 9, REND_BLACKHAND + HEROIC));

        list.add(getBoss(BLACKWING_LAIR, 10, RAZORGORE + HEROIC));
        list.add(getBoss(BLACKWING_LAIR, 11, VAELASTRASZ + HEROIC));
        list.add(getBoss(BLACKWING_LAIR, 12, CHROMAGGUS + HEROIC));
        list.add(getBoss(BLACKWING_LAIR, 13, VICTOR_NEFARIUS + HEROIC));

        list.add(getBoss(HIDDEN_LABORATORY, 14, OMNOTRON + HEROIC));
        list.add(getBoss(HIDDEN_LABORATORY, 15, MALORIAK + HEROIC));
        list.add(getBoss(HIDDEN_LABORATORY, 16, ATRAMEDES + HEROIC));
        list.add(getBoss(HIDDEN_LABORATORY, 17, NEFARIAN + HEROIC));
    }

    @Override
    protected void setRelatedConditions(String name, ArrayList<String> list) {
        if (HEROIC.isEmpty()) {
            /*
            NORMAL
             */
            switch (name) {
                case COREN_DIREBREW:
                    list.add("Guzzler");
                    list.add("Get 'em!");
                    list.add("Dark Iron Bouncer");
                    break;
                case GRIMSTONE:
                    list.add("Dark Iron Spectator");
                    break;
                case THAURISSAN:
                    list.add("Moira Bronzebeard");
                    break;
                case GARR:
                    list.add("Rock Out");
                    list.add("Firesworn");
                    break;
                case BARON_GEDDON:
                    list.add("Living Bomb");
                    break;
                case MAJORDOMO:
                    list.add("Flamewaker Acolyte");
                    list.add("DIE INSECT!1");
                    list.add("Son of the Flame");
                    break;
                case OMOKK:
                    list.add("TIME FOR SMASH");
                    break;
                case DRAKKISATH:
                    list.add("Drakkisath's Command");
                    break;
                case REND_BLACKHAND:
                    list.add("Whelp2");
                    list.add("Gyth");
                    list.add("Old Horde Orc");
                    list.add("Dragonkin");
                    break;
                case RAZORGORE:
                    list.add("Corrupted Egg");
                    list.add("Razorgore's Claws");
                    list.add("Chromatic Drake");
                    break;
                case VAELASTRASZ:
                    list.add("Burning Adrenaline");
                    break;
                case CHROMAGGUS:
                    list.add("Brood Affliction: Black");
                    list.add("Brood Affliction: Blue");
                    list.add("Brood Affliction: Bronze");
                    list.add("Brood Affliction: Green");
                    list.add("Brood Affliction: Red");
                    list.add("Chromatic Mutation");
                    list.add("Chromatic Dragonkin");
                    break;
                case VICTOR_NEFARIUS:
                    break;
                case OMNOTRON:
                    list.add("Arcanotron");
                    list.add("Toxitron");
                    list.add("Electron");
                    list.add("Magmatron");
                    list.add("Recharge");
                    list.add("Magmaw");
                    break;
                case MALORIAK:
                    list.add("Release the Aberrations!");
                    list.add("Aberration");
                    break;
                case ATRAMEDES:
                    list.add("Dragonteeth");
                    list.add("Sonic Breath");
                    list.add("Reverberating Gong");
                    break;
                case NEFARIAN:
                    list.add("Onyxiclaw");
                    list.add("Bone Construct");
                    list.add("LAVA!");
                    list.add("Chromatic Prototype");
                    break;
            }
        } else {
            name = name.replace(HEROIC, "");
            /*
            HEROIC
             */
            switch (name) {
                case COREN_DIREBREW:
                    list.add("Guzzler");
                    list.add("Get 'em!");
                    list.add("Dark Iron Bouncer");
                    break;
                case GRIMSTONE:
                    list.add("Dark Iron Spectator");
                    break;
                case THAURISSAN:
                    list.add("Moira Bronzebeard1");
                    break;
                case GARR:
                    list.add("Rock Out");
                    list.add("Firesworn1");
                    break;
                case BARON_GEDDON:
                    list.add("Living Bomb1");
                    break;
                case MAJORDOMO:
                    list.add("Flamewaker Acolyte1");
                    list.add("Son of the Flame");
                    break;
                case OMOKK:
                    list.add("TIME FOR SMASH");
                    break;
                case DRAKKISATH:
                    list.add("Drakkisath's Command");
                    break;
                case REND_BLACKHAND:
                    list.add("Whelp3");
                    list.add("Gyth1");
                    list.add("Old Horde Orc1");
                    list.add("Dragonkin1");
                    break;
                case RAZORGORE:
                    list.add("Corrupted Egg1");
                    list.add("Razorgore's Claws");
                    list.add("Chromatic Drake1");
                    break;
                case VAELASTRASZ:
                    list.add("Burning Adrenaline");
                    break;
                case CHROMAGGUS:
                    list.add("Brood Affliction: Black1");
                    list.add("Brood Affliction: Blue1");
                    list.add("Brood Affliction: Bronze1");
                    list.add("Brood Affliction: Green1");
                    list.add("Brood Affliction: Red1");
                    list.add("Chromatic Mutation");
                    list.add("Chromatic Dragonkin1");
                    break;
                case VICTOR_NEFARIUS:
                    break;
                case OMNOTRON:
                    list.add("Arcanotron");
                    list.add("Toxitron1");
                    list.add("Electron1");
                    list.add("Magmatron1");
                    list.add("Recharge");
                    list.add("Magmaw");
                    break;
                case MALORIAK:
                    list.add("Release the Aberrations!");
                    list.add("Aberration");
                    break;
                case ATRAMEDES:
                    list.add("Dragonteeth");
                    list.add("Sonic Breath");
                    list.add("Reverberating Gong");
                    break;
                case NEFARIAN:
                    list.add("Onyxiclaw");
                    list.add("Bone Construct1");
                    list.add("LAVA!");
                    list.add("Chromatic Prototype");
                    break;
            }
        }
    }
}
