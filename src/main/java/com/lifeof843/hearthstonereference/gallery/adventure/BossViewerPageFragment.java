package com.lifeof843.hearthstonereference.gallery.adventure;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.gallery.GalleryTutorial;
import com.lifeof843.hearthstonereference.utility.FileManager;

public class BossViewerPageFragment extends Fragment {

    private View view;
    private FileManager fileManager;
    private Boss boss;
    private boolean isHeroic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_boss_viewer_slider, container, false);

        init();
        initScrollView();
        displayContent();

        return view;
    }

    private void init() {
        this.fileManager = new FileManager(view.getContext());

        if (getArguments() != null) {
            boss = getArguments().getParcelable("boss");
        }
    }

    private void initScrollView() {
        ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    private void displayContent() {
        displayTitle(boss.getRoom());
        displayBoss(boss.getName());

        for (String name : boss.getRelated()) {
            displayImage(name);
        }
    }

    private void displayTitle(String room) {
        if (room != null) {
            TextView textView = (TextView) view.findViewById(R.id.title);
            textView.setText(room);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void displayImage(String name) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.imageLayout);

        ImageView imageView = new ImageView(view.getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imageView.setAdjustViewBounds(true);
        imageView.setImageDrawable(fileManager.getDrawableFromName(name, true));

        layout.addView(imageView);
    }

    private void displayImage2(String name, String heroic) {
        if (heroic.equals("1")) {
            displayImage(name + "2");
        } else {
            displayImage(name + "1");
        }
    }

    private void displayBoss(String name) {
        String heroic = "";

        if (name.endsWith("_HEROIC")) {
            name = name.replace("_HEROIC", "");
            heroic = "1";
            isHeroic = true;
        } else {
            isHeroic = false;
        }

        switch (name) {
            /*
            Tutorial
             */
            case GalleryTutorial.HOGGER:
                displayImage(GalleryTutorial.HOGGER + "1");
                break;
            case GalleryTutorial.KING_MUKLA:
                displayImage(GalleryTutorial.KING_MUKLA + "1");
                break;
            case GalleryTutorial.HEMET_NESINGWARY:
                displayImage(GalleryTutorial.HEMET_NESINGWARY + "1");
                displayImage("Shotgun Blast");
                break;
            case GalleryTutorial.ILLIDAN_STORMRAGE:
                displayImage(GalleryTutorial.ILLIDAN_STORMRAGE + "1");
                displayImage("Flames of Azzinoth");
                break;
            case GalleryTutorial.LOREWALKER_CHO:
                displayImage(GalleryTutorial.LOREWALKER_CHO + "1");
                break;
            case GalleryTutorial.MILLHOUSE_MANASTORM:
                displayImage(GalleryTutorial.MILLHOUSE_MANASTORM + "1");
                break;

            /*
            Curse of Naxxramas
             */
            case GalleryNAXX.ANUBREKHAN:
                displayImage(GalleryNAXX.ANUBREKHAN + heroic);
                displayImage("Skitter" + heroic);
                break;
            case GalleryNAXX.GRAND_WIDOW_FAERLINA:
                displayImage(GalleryNAXX.GRAND_WIDOW_FAERLINA + heroic);
                displayImage("Rain of Fire" + heroic);
                break;
            case GalleryNAXX.MAEXXNA:
                displayImage2(GalleryNAXX.MAEXXNA, heroic);
                displayImage("Web Wrap" + heroic);
                break;
            case GalleryNAXX.NOTH_THE_PLAGUEBRINGER:
                displayImage(GalleryNAXX.NOTH_THE_PLAGUEBRINGER + heroic);
                displayImage("Raise Dead" + heroic);
                break;
            case GalleryNAXX.HEIGAN_THE_UNCLEAN:
                displayImage(GalleryNAXX.HEIGAN_THE_UNCLEAN + heroic);
                displayImage("Eruption" + heroic);
                break;
            case GalleryNAXX.LOATHEB:
                displayImage2(GalleryNAXX.LOATHEB, heroic);
                displayImage("Necrotic Aura" + heroic);
                break;
            case GalleryNAXX.INSTRUCTOR_RAZUVIOUS:
                displayImage(GalleryNAXX.INSTRUCTOR_RAZUVIOUS + heroic);
                displayImage("Unbalancing Strike" + heroic);
                break;
            case GalleryNAXX.GOTHIK_THE_HARVESTER:
                displayImage(GalleryNAXX.GOTHIK_THE_HARVESTER + heroic);
                displayImage("Harvest" + heroic);
                break;
            case GalleryNAXX.THE_FOUR_HORSEMEN:
                displayImage2("Baron Rivendare", heroic);
                displayImage("Unholy Shadow" + heroic);
                break;
            case GalleryNAXX.PATCHWERK:
                displayImage(GalleryNAXX.PATCHWERK + heroic);
                displayImage("Hateful Strike" + heroic);
                break;
            case GalleryNAXX.GROBBULUS:
                displayImage(GalleryNAXX.GROBBULUS + heroic);
                displayImage("Poison Cloud" + heroic);
                break;
            case GalleryNAXX.GLUTH:
                displayImage(GalleryNAXX.GLUTH + heroic);
                displayImage("Decimate" + heroic);
                break;
            case GalleryNAXX.THADDIUS:
                displayImage2(GalleryNAXX.THADDIUS, heroic);
                displayImage("Polarity Shift");
                break;
            case GalleryNAXX.SAPPHIRON:
                displayImage(GalleryNAXX.SAPPHIRON + heroic);
                displayImage("Frost Breath" + heroic);
                break;
            case GalleryNAXX.KELTHUZAD:
                displayImage2(GalleryNAXX.KELTHUZAD, heroic);
                displayImage("Frost Blast" + heroic);
                break;

            /*
            Blackrock Mountain
             */
            case GalleryBRM.COREN_DIREBREW:
                displayImage(GalleryBRM.COREN_DIREBREW);
                displayImage("Pile On!" + heroic);
                break;
            case GalleryBRM.GRIMSTONE:
                displayImage(GalleryBRM.GRIMSTONE);
                displayImage("Jeering Crowd" + heroic);
                break;
            case GalleryBRM.THAURISSAN:
                displayImage(GalleryBRM.THAURISSAN + "1");
                displayImage("Power of the Firelord");
                break;
            case GalleryBRM.GARR:
                displayImage(GalleryBRM.GARR + heroic);
                displayImage("Magma Pulse");
                break;
            case GalleryBRM.BARON_GEDDON:
                displayImage2(GalleryBRM.BARON_GEDDON, heroic);
                displayImage("Ignite Mana" + heroic);
                break;
            case GalleryBRM.MAJORDOMO:
                displayImage(GalleryBRM.MAJORDOMO + "1");
                displayImage("The Majordomo" + heroic);
                if (heroic.isEmpty()) {
                    displayImage("DIE INSECT!");
                } else {
                    displayImage("DIE INSECTS!");
                }
                break;
            case GalleryBRM.OMOKK:
                displayImage(GalleryBRM.OMOKK);
                displayImage("ME SMASH" + heroic);
                break;
            case GalleryBRM.DRAKKISATH:
                displayImage(GalleryBRM.DRAKKISATH);
                displayImage("Intense Gaze" + heroic);
                break;
            case GalleryBRM.REND_BLACKHAND:
                displayImage(GalleryBRM.REND_BLACKHAND + "1");
                displayImage("Open the Gates" + heroic);
                displayImage("Dismount");
                displayImage("Old Horde" + heroic);
                displayImage("Blackwing" + heroic);
                break;
            case GalleryBRM.RAZORGORE:
                displayImage(GalleryBRM.RAZORGORE);
                displayImage("The Rookery" + heroic);
                break;
            case GalleryBRM.VAELASTRASZ:
                displayImage(GalleryBRM.VAELASTRASZ);
                displayImage("Essence of the Red" + heroic);
                break;
            case GalleryBRM.CHROMAGGUS:
                displayImage2(GalleryBRM.CHROMAGGUS, heroic);
                displayImage("Brood Affliction");
                displayImage("Mutation");
                break;
            case GalleryBRM.VICTOR_NEFARIUS:
                displayImage(GalleryBRM.VICTOR_NEFARIUS);
                displayImage("True Form");
                displayImage("Wild Magic");
                break;
            case GalleryBRM.OMNOTRON:
                displayImage(GalleryBRM.OMNOTRON);
                displayImage("Activate!" + heroic);
                break;
            case GalleryBRM.MALORIAK:
                displayImage(GalleryBRM.MALORIAK);
                displayImage("The Alchemist" + heroic);
                break;
            case GalleryBRM.ATRAMEDES:
                displayImage(GalleryBRM.ATRAMEDES);
                displayImage("Echolocate" + heroic);
                break;
            case GalleryBRM.NEFARIAN:
                displayImage2(GalleryBRM.NEFARIAN, heroic);
                displayImage2("Onyxia", heroic);
                displayImage("Nefarian Strikes!");
                displayImage("Bone Minions" + heroic);
                break;

            /*
            League of Explorers
             */
            case GalleryLOE.ZINAAR:
                displayImageLOE("Zinaar");
                displayNilAnd1("Djinn's Intuition");
                break;
            case GalleryLOE.PHAERIX:
                displayImageLOE(GalleryLOE.PHAERIX, "Phaerix");
                displayNilAnd1("Blessings of the Sun");
                break;
            case GalleryLOE.ESCAPE:
                displayImage("Temple Escape");
                displayImage("Escape!");
                displayImage("Pit of Spikes");
                displayImage("Swing Across");
                displayImage("Walk Across Gingerly");
                displayImage("A Glowing Pool");
                displayImage("Drink Deeply");
                displayImage("Wade Through");
                displayImage("The Eye");
                displayImage("Touch It");
                displayImage("Investigate the Runes");
                displayImage("The Darkness");
                displayImage("Take the Shortcut");
                displayImage("No Way!");
                break;
            case GalleryLOE.SCARVASH:
                displayImageLOE(GalleryLOE.SCARVASH, "Scarvash");
                displayNilAnd1("Trogg Hate Minions!");
                displayNilAnd1("Trogg Hate Spells!");
                break;
            case GalleryLOE.CART:
                displayImageLOE("Mine Shaft");
                displayImage("Flee the Mine!");
                displayImage("Mine Cart");
                displayImage("Throw Rocks");
                break;
            case GalleryLOE.ARCHAEDAS:
                displayImageLOE("Archaedas");
                displayNilAnd1("Stonesculpting");
                break;
            case GalleryLOE.SLITHERSPEAR:
                displayImageLOE(GalleryLOE.SLITHERSPEAR, "Slitherspear");
                if (isHeroic) {
                    displayImage("Endless Hunger");
                } else {
                    displayImage("Getting Hungry");
                }
                displayNilAnd1("Enraged!");
                break;
            case GalleryLOE.GIANTFIN:
                displayImageLOE("Giantfin");
                displayNilAnd1("Mrglmrgl MRGL!");
                break;
            case GalleryLOE.NAZJAR:
                displayImageLOE(GalleryLOE.NAZJAR, "Nazjar");
                displayNilAnd1("Pearl of the Tides");
                break;
            case GalleryLOE.SKELESAURUS:
                displayImageLOE(GalleryLOE.SKELESAURUS, "Skelesaurus");
                displayNilAnd1("Ancient Power");
                break;
            case GalleryLOE.SENTINEL:
                displayImageLOE(GalleryLOE.SENTINEL, "Sentinel");
                break;
            case GalleryLOE.RAFAAM1:
                displayImage("Rafaam");
                if (isHeroic) {
                    displayImage("Unstable Portal2");
                } else {
                    displayImage("Unstable Portal1");
                }
                break;
            case GalleryLOE.RAFAAM2:
                displayImage("Rafaam1");
                displayNilAnd1("Staff of Origination");
                displayNilAnd1("Rummage");

                if (isHeroic) {
                    displayImage(GalleryLOE.ZINAAR);
                    displayImage(GalleryLOE.PHAERIX);
                    displayImage(GalleryLOE.SCARVASH);
                    displayImage(GalleryLOE.ARCHAEDAS);
                    displayImage(GalleryLOE.SLITHERSPEAR);
                    displayImage(GalleryLOE.GIANTFIN + "1");
                    displayImage(GalleryLOE.NAZJAR);
                    displayImage(GalleryLOE.SKELESAURUS);
                    displayImage(GalleryLOE.SENTINEL);
                } else {
                    displayImage(GalleryLOE.ZINAAR + "1");
                    displayImage(GalleryLOE.PHAERIX + "1");
                    displayImage(GalleryLOE.SCARVASH + "1");
                    displayImage(GalleryLOE.ARCHAEDAS + "1");
                    displayImage(GalleryLOE.SLITHERSPEAR + "1");
                    displayImage(GalleryLOE.GIANTFIN);
                    displayImage(GalleryLOE.NAZJAR + "1");
                    displayImage(GalleryLOE.SKELESAURUS + "1");
                    displayImage(GalleryLOE.SENTINEL + "1");
                }
                break;

            /*
            One Night in Karazhan
             */
            case GalleryKAR.MALCHEZAAR:
                displayImage("Prince Malchezaar1");
                displayImage("Legion");
                displayImage("Abyssal");
                displayImage("Brilliance");
                displayNilAnd1("Looming Presence");
                displayImage("Arcane Power");
                displayImage("Archmage's Insight");
                displayImage("Astral Portal");
                displayImage("Evocation");
                displayImage("Mage Armor");
                displayImage("Mysterious Rune");
                displayImage("Archmage Apprentice");
                break;
            case GalleryKAR.SILVERWARE:
                displayImage("Silverware Golem1");
                displayNilAnd1("Be Our Guest");
                displayImage("Pour a Round");
                displayImage("Tossing Plates");
                displayNilAnd1("Set the Table");
                displayNilAnd1("Plate");
                displayNilAnd1("Cup");
                displayNilAnd1("Knife");
                displayNilAnd1("Fork");
                displayNilAnd1("Spoon");
                if (isHeroic) displayImage("Pitcher");
                break;
            case GalleryKAR.MAGIC_MIRROR:
                displayImage("Magic Mirror");
                displayNilAnd1("Reflections");
                break;
            case GalleryKAR.BLACK_KING:
                displayImage("Black King");
                displayImage("Cheat");
                displayNilAnd1("Castle");
                displayImage("White King");
                displayImage("Black Pawn");
                displayImage("Black Bishop");
                displayImage("Black Knight");
                displayImage("Black Rook");
                displayImage("Black Queen");
                displayImage("White Pawn");
                displayImage("White Bishop");
                displayImage("White Knight");
                displayImage("White Rook");
                displayImage("White Queen");
                break;
            case GalleryKAR.ROMULO:
                displayImage("Julianne");
                displayNilAnd1("Romulo");
                displayNilAnd1("True Love");
                break;
            case GalleryKAR.BIG_BAD_WOLF:
                if (isHeroic) displayImage("Big Bad Wolf1");
                else displayImage("Kindly Grandmother1");
                displayNilAnd1("Trembling");
                displayImage("Big Bad Wolf");
                displayImage("Kindly Grandmother");
                displayNilAnd1("Big Bad Claws");
                break;
            case GalleryKAR.CRONE:
                displayNilAnd1("The Crone");
                displayImage("Twister");
                displayImage("Dorothee");
                displayImage("Looming Presence");
                displayNilAnd1("Flying Monkey");
                break;
            case GalleryKAR.CURATOR:
                displayNilAnd1("Curator");
                displayImage("Gallery Protection");
                displayImage("Haywire Mech!");
                displayNilAnd1("Stampeding Beast!");
                displayNilAnd1("Demons Loose!");
                displayNilAnd1("Dragons Free!");
                if (isHeroic) displayImage("Murlocs Escaping!");
                else displayImage("Murloc Escaping!");
                break;
            case GalleryKAR.NIGHTBANE:
                displayImage("Nightbane");
                if (isHeroic) displayImage("Manastorm");
                break;
            case GalleryKAR.ILLHOOF:
                displayNilAnd1("Terestian Illhoof");
                displayImage("Dark Pact");
                displayImage("Many Imps!");
                displayNilAnd1("Icky Imp");
                displayImage("Summon Kil'rek");
                displayNilAnd1("Kil'rek");
                displayNilAnd1("Shadow Volley");
                displayNilAnd1("Steal Life");
                break;
            case GalleryKAR.ARAN:
                displayImage("Shade of Aran");
                displayNilAnd1("Ley Lines");
                displayNilAnd1("Flame Wreath");
                break;
            case GalleryKAR.NETHERSPITE:
                displayImage("Netherspite");
                displayNilAnd1("Nether Rage");
                displayImage("Red Portal");
                displayImage("Blue Portal");
                displayNilAnd1("Nether Breath");
                displayNilAnd1("Terrifying Roar");
                break;
            case GalleryKAR.UNKNOWN:
                displayImage("Nazra Wildaxe");
                displayNilAnd1("The Horde");
                displayNilAnd1("Orc Warrior");
                displayImage("Prince Malchezaar2");
                displayImage("Legion1");
                displayImage("Abyssal");
                displayImage("Shadow Bolt Volley");
                displayNilAnd1("Demonic Presence");
                displayImage("Medivh1");
                break;
        }
    }

    private void displayNilAnd1(String name) {
        if (isHeroic) {
            displayImage(name + "1");
        } else {
            displayImage(name);
        }
    }

    private void displayImageLOE(String name) {
        if (!isHeroic) {
            displayImage(name + "2");
        } else {
            displayImage("Heroic " + name);
        }
    }

    private void displayImageLOE(String name, String heroicName) {
        if (!isHeroic) {
            displayImage(name + "2");
        } else {
            displayImage("Heroic " + heroicName);
        }
    }
}
