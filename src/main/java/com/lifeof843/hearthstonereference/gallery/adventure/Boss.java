package com.lifeof843.hearthstonereference.gallery.adventure;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Boss implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Boss> CREATOR = new Parcelable.Creator<Boss>() {
        @Override
        public Boss createFromParcel(Parcel in) {
            return new Boss(in);
        }

        @Override
        public Boss[] newArray(int size) {
            return new Boss[size];
        }
    };
    private String name;
    private int order;
    private ArrayList<String> related;
    private String room;

    Boss(String name, int order, ArrayList<String> related) {
        this.name = name;
        this.order = order;
        this.related = related;
    }

    Boss(String name, int order, ArrayList<String> related, String room) {
        this.name = name;
        this.order = order;
        this.related = related;
        this.room = room;
    }

    protected Boss(Parcel in) {
        name = in.readString();
        order = in.readInt();
        if (in.readByte() == 0x01) {
            related = new ArrayList<>();
            in.readList(related, String.class.getClassLoader());
        } else {
            related = null;
        }
        room = in.readString();
    }

    public String getName() {
        return name;
    }

    public int getOrder() {
        return order;
    }

    public ArrayList<String> getRelated() {
        return related;
    }

    public String getRoom() {
        return room;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(order);
        if (related == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(related);
        }
        dest.writeString(room);
    }
}
