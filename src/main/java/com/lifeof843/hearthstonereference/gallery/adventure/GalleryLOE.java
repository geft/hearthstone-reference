package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;

import java.util.ArrayList;

public class GalleryLOE extends BossGallery {

    public static final String ZINAAR = "Zinaar";
    public static final String PHAERIX = "Sun Raider Phaerix";
    public static final String ESCAPE = "Temple Escape";

    public static final String SCARVASH = "Chieftain Scarvash";
    public static final String CART = "Mine Cart";
    public static final String ARCHAEDAS = "Archaedas";

    public static final String SLITHERSPEAR = "Lord Slitherspear";
    public static final String GIANTFIN = "Giantfin";
    public static final String NAZJAR = "Lady Nazj'ar";

    public static final String SKELESAURUS = "Skelesaurus Hex";
    public static final String SENTINEL = "The Steel Sentinel";
    public static final String RAFAAM1 = "Arch-Thief Rafaam";
    public static final String RAFAAM2 = "Rafaam Unleashed";

    public static final String ORSIS = "Temple of Orsis";
    public static final String ULDAMAN = "Uldaman";
    public static final String RUIN = "The Ruined City";
    public static final String HALL = "Hall of Explorers";

    public GalleryLOE(Context context, boolean isHeroic) {
        super(context, isHeroic);
    }

    @Override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(ORSIS, 1, ZINAAR + HEROIC));
        list.add(getBoss(ORSIS, 2, PHAERIX + HEROIC));
        list.add(getBoss(ORSIS, 3, ESCAPE + HEROIC));

        list.add(getBoss(ULDAMAN, 4, SCARVASH + HEROIC));
        list.add(getBoss(ULDAMAN, 5, CART + HEROIC));
        list.add(getBoss(ULDAMAN, 6, ARCHAEDAS + HEROIC));

        list.add(getBoss(RUIN, 7, SLITHERSPEAR + HEROIC));
        list.add(getBoss(RUIN, 8, GIANTFIN + HEROIC));
        list.add(getBoss(RUIN, 9, NAZJAR + HEROIC));

        list.add(getBoss(HALL, 10, SKELESAURUS + HEROIC));
        list.add(getBoss(HALL, 11, SENTINEL + HEROIC));
        list.add(getBoss(HALL, 12, RAFAAM1 + HEROIC));
        list.add(getBoss(HALL, 13, RAFAAM2 + HEROIC));
    }

    @Override
    protected void setRelatedConditions(String name, ArrayList<String> list) {
        if (name.startsWith(ZINAAR)) {
            list.add("Wish for Power");
            list.add("Wish for Valor");
            list.add("Wish for Glory");
            list.add("Wish for More Wishes");
            list.add("Wish for Companionship");
            list.add("Leokk1");
            list.add("Misha1");
        } else if (name.startsWith(PHAERIX)) {
            list.add("Blessing of the Sun");
            list.add("Rod of the Sun");
        } else if (name.startsWith(ESCAPE)) {
            list.add("Animated Statue");
            list.add("Rolling Boulder");
        } else if (name.startsWith(SCARVASH)) {
            list.add("Chasing Trogg");
            list.add("Earthen Pursuer");
            list.add("Lumbering Golem");
        } else if (name.startsWith(CART)) {
            list.add("Chasing Trogg");
            list.add("Earthen Pursuer");
            list.add("Lumbering Golem");
            list.add("Rock");
            list.add("Dynamite");
            list.add("Boom!");
            list.add("Barrel Forward");
            list.add("Spiked Decoy");
            list.add("Mechanical Parrot");
            list.add("Consult Brann");
            list.add("Repairs");
        } else if (name.startsWith(ARCHAEDAS)) {
            list.add("Earthen Statue");
        } else if (name.startsWith(SLITHERSPEAR)) {
            list.add("Rare Spear");
        } else if (name.startsWith(RAFAAM1)) {
            list.add("Rare Spear");
        } else if (name.startsWith(RAFAAM2)) {
            list.add("Hakkari Blood Goblet");
            list.add("Crown of Kael'thas");
            list.add("Medivh's Locket");
            list.add("Eye of Orsis");
            list.add("Khadgar's Pipe");
            list.add("Ysera's Tear");
            list.add("Lantern of Power");
            list.add("Timepiece of Horror");
            list.add("Mirror of Doom");
            list.add("Shard of Sulfuras");
            list.add("Benediction Splinter");
            list.add("Putress' Vial");
            list.add("Lothar's Left Greave");
        }

        /*
        NORMAL
        */
        if (HEROIC.isEmpty()) {
            switch (name) {
                case PHAERIX:
                    list.add("Looming Presence");
                    list.add("Tol'vir Hoplite");
                    break;
                case ESCAPE:
                    list.add("Orsis Guard");
                    list.add("Anubisath Temple Guard");
                    list.add("Seething Statue");
                    list.add("Giant Insect");
                    break;
                case CART:
                    list.add("Debris");
                    break;
                case ARCHAEDAS:
                    list.add("Animate Earthen");
                    list.add("Shattering Spree");
                    break;
                case SLITHERSPEAR:
                    list.add("Getting Hungry1");
                    list.add("Getting Hungry2");
                    list.add("Getting Hungry3");
                    list.add("Hungry Naga");
                    list.add("Hungry Naga1");
                    list.add("Hungry Naga3");
                    list.add("Hungry Naga4");
                    list.add("Hungry Naga5");
                    list.add("Slithering Archer");
                    list.add("Slithering Guard");
                    list.add("Naga Repellent");
                    list.add("Cauldron");
                    break;
                case GIANTFIN:
                    list.add("Mrgl Mrgl Nyah Nyah");
                    break;
                case SENTINEL:
                    list.add("Platemail Armor");
                    break;
                case RAFAAM1:
                    list.add("Boneraptor");
                    break;
            }
        } else {
            name = name.replace(HEROIC, "");
            /*
            HEROIC
             */
            switch (name) {
                case PHAERIX:
                    list.add("Looming Presence1");
                    list.add("Tol'vir Hoplite1");
                    break;
                case ESCAPE:
                    list.add("Orsis Guard1");
                    list.add("Anubisath Temple Guard1");
                    list.add("Seething Statue1");
                    list.add("Giant Insect1");
                    break;
                case ARCHAEDAS:
                    list.add("Earthen Statue1");
                    list.add("Animate Earthen1");
                    list.add("Shattering Spree1");
                    list.add("Looming Presence1");
                    break;
                case SLITHERSPEAR:
                    list.add("Hungry Naga2");
                    list.add("Slithering Archer1");
                    list.add("Cauldron1");
                    list.add("Slithering Guard1");
                    list.add("Naga Repellent1");
                    break;
                case GIANTFIN:
                    list.add("Mrgl Mrgl Nyah Nyah1");
                    break;
                case NAZJAR:
                    list.add("Looming Presence1");
                    break;
                case SENTINEL:
                    list.add("Platemail Armor1");
                    break;
                case RAFAAM1:
                    list.add("Boneraptor");
                    break;
            }
        }
    }
}
