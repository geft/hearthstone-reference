package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BossViewer extends FragmentActivity {

    private int numPages;
    private ArrayList<Boss> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boss_viewer);

        if (savedInstanceState != null) {
            list = savedInstanceState.getParcelableArrayList("list");
        }

        initialize();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initialize() {
        list = getIntent().getExtras().getParcelableArrayList("list");

        if (list != null) {
            numPages = list.size();
        } else numPages = 0;

        setPagerAdapter();
    }

    private void setPagerAdapter() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new BossViewerPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setFadeAnimation();
    }

    private void setFadeAnimation() {
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    private Fragment getPageWithPosition(int position, BossViewerPageFragment page) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("boss", list.get(position));
        page.setArguments(bundle);

        return page;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putParcelableArrayList("list", list);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private class BossViewerPagerAdapter extends FragmentStatePagerAdapter {
        public BossViewerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            BossViewerPageFragment page = new BossViewerPageFragment();
            return getPageWithPosition(position, page);
        }

        @Override
        public int getCount() {
            return numPages;
        }
    }
}
