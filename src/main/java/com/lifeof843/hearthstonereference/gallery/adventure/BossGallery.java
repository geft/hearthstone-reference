package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;
import android.content.Intent;

import com.lifeof843.hearthstonereference.gallery.GalleryTutorial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class BossGallery {

    public String HEROIC;

    public BossGallery(Context context, boolean isHeroic) {
        if (isHeroic) {
            HEROIC = "_HEROIC";
        } else {
            HEROIC = "";
        }

        startGalleryViewer(context);
    }

    private void startGalleryViewer(Context context) {
        Intent intent = new Intent(context, BossViewer.class);
        intent.putParcelableArrayListExtra("list", getHeroList());
        context.startActivity(intent);
    }

    private ArrayList<Boss> getHeroList() {
        ArrayList<Boss> list = new ArrayList<>();

        addBossToList(list);
        sortByOrder(list);

        return list;
    }

    private void sortByOrder(ArrayList<Boss> list) {
        Collections.sort(list, new Comparator<Boss>() {
            @Override
            public int compare(Boss lhs, Boss rhs) {
                return lhs.getOrder() < rhs.getOrder() ? -1 : 1;
            }
        });
    }

    protected Boss getBoss(int order, String name) {
        return new Boss(name, order, getRelatedCards(name));
    }

    protected Boss getBoss(String quarter, int order, String name) {
        return new Boss(name, order, getRelatedCards(name), quarter);
    }

    private ArrayList<String> getRelatedCards(String name) {
        ArrayList<String> list = new ArrayList<>();

        setRelatedConditions(name, list);
        Collections.sort(list);

        return list;
    }

    // must override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(1, GalleryTutorial.HOGGER));
    }

    // must override
    protected void setRelatedConditions(String name, ArrayList<String> list) {
        switch (name) {
            case GalleryTutorial.HOGGER:
                list.add("Gnoll");
                break;
        }
    }
}
