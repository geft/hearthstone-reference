package com.lifeof843.hearthstonereference.gallery.adventure;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Gerry on 27/09/2016.
 */

public class GalleryKAR extends BossGallery {

    public static final String MALCHEZAAR = "Malchezaar";
    public static final String SILVERWARE = "Silverware";
    public static final String MAGIC_MIRROR = "Magic Mirror";
    public static final String BLACK_KING = "Black King";
    public static final String ROMULO = "Romulo & Julianne";
    public static final String BIG_BAD_WOLF = "Big Bad Wolf";
    public static final String CRONE = "The Crone";
    public static final String CURATOR = "The Curator";
    public static final String NIGHTBANE = "Nightbane";
    public static final String ILLHOOF = "Illhoof";
    public static final String ARAN = "Shade of Aran";
    public static final String NETHERSPITE = "Netherspite";
    public static final String UNKNOWN = "Unknown";

    public static final String PROLOGUE = "The Prologue";
    public static final String PARLOR = "The Parlor";
    public static final String OPERA = "The Opera";
    public static final String MENAGERIE = "The Menagerie";
    public static final String SPIRE = "The Spire";

    public GalleryKAR(Context context, boolean isHeroic) {
        super(context, isHeroic);
    }

    @Override
    protected void addBossToList(ArrayList<Boss> list) {
        list.add(getBoss(PROLOGUE, 1, MALCHEZAAR + HEROIC));
        list.add(getBoss(PARLOR, 2, SILVERWARE + HEROIC));
        list.add(getBoss(PARLOR, 3, MAGIC_MIRROR + HEROIC));
        list.add(getBoss(PARLOR, 4, BLACK_KING + HEROIC));
        list.add(getBoss(OPERA, 5, ROMULO + HEROIC));
        list.add(getBoss(OPERA, 6, BIG_BAD_WOLF + HEROIC));
        list.add(getBoss(OPERA, 7, CRONE + HEROIC));
        list.add(getBoss(MENAGERIE, 8, CURATOR + HEROIC));
        list.add(getBoss(MENAGERIE, 9, NIGHTBANE + HEROIC));
        list.add(getBoss(MENAGERIE, 10, ILLHOOF + HEROIC));
        list.add(getBoss(SPIRE, 11, ARAN + HEROIC));
        list.add(getBoss(SPIRE, 12, NETHERSPITE + HEROIC));
        list.add(getBoss(SPIRE, 13, UNKNOWN + HEROIC));
    }
}
