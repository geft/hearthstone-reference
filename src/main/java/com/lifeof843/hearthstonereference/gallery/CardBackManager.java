package com.lifeof843.hearthstonereference.gallery;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

public class CardBackManager {

    private ArrayList<CardBack> cardBacks;

    CardBackManager() {
        cardBacks = new ArrayList<>();
    }

    public ArrayList<CardBack> get2014() {
        cardBacks = new ArrayList<>();

        addCardBack(1, "Pandaria");
        addCardBack(2, "Black Temple");
        addCardBack(3, "Rainbow");
        addCardBack(4, "Naxxramas");
        addCardBack(5, "Icecrown");
        addCardBack(6, "Pirates!");
        addCardBack(7, "Hallow's End");
        addCardBack(8, "Goblins");
        addCardBack(9, "Gnomes");

        return cardBacks;
    }

    public ArrayList<CardBack> get2015() {
        cardBacks = new ArrayList<>();

        addCardBack(10, "Maraad");
        addCardBack(11, "Lunar New Year");
        addCardBack(12, "Ragnaros");
        addCardBack(13, "Cupcake");
        addCardBack(14, "Ninja");
        addCardBack(15, "Darnassus");
        addCardBack(16, "Darkspear");
        addCardBack(17, "Tournament Grounds");
        addCardBack(18, "Exodar");
        addCardBack(19, "Highmaul");
        addCardBack(20, "Explorer's Map");
        addCardBack(21, "Shaman Thrall");

        return cardBacks;
    }

    public ArrayList<CardBack> get2016() {
        cardBacks = new ArrayList<>();

        addCardBack(22, "Love is in the Air");
        addCardBack(23, "Tauren Thunderbluff");
        addCardBack(24, "Hogger");
        addCardBack(25, "Clutch of Yogg Saron");
        addCardBack(26, "Shadowmoon Valley");
        addCardBack(27, "Zul'drak");
        addCardBack(28, "Tinyfin Beach");
        addCardBack(29, "Medivh's Invitation");
        addCardBack(30, "Legion");
        addCardBack(31, "Pie");
        addCardBack(32, "Halfhill");
        addCardBack(33, "Jade Lotus");

        return cardBacks;
    }

    public ArrayList<CardBack> get2017() {
        cardBacks = new ArrayList<>();

        addCardBack(34, "Grimy Goons");
        addCardBack(35, "Kabal");

        return cardBacks;
    }

    ArrayList<CardBack> getPromo() {
        cardBacks = new ArrayList<>();

        addCardBack("Dalaran Flame", "Available to active members of TeSPA" +
                "(The eSports Association) Local Chapters with at least 25 members.");
        addCardBack("Golden Celebration", "Awarded to the top 128 ranked players in each of 8 GOLD Open Tournament");
        addCardBack("Heroes of the Storm", "Get to level 12 in Heroes of the Storm");
        addCardBack("Power Core", "Available to players who attend select Hearthstone eSports tournaments");
        addCardBack("Samsung Galaxy", "Play Hearthstone on a Samsung Galaxy S6 series phone");

        addCardBack("Blizzard 2014", "Attend BlizzCon 2014 or buy a virtual ticket");
        addCardBack("Blizzard 2015", "Attend Blizzcon 2015 or buy a virtual ticket");
        addCardBack("Welcome Inn", "Attend Blizzcon 2016 or buy a virtual ticket");

        return cardBacks;
    }

    ArrayList<CardBack> getRewards() {
        cardBacks = new ArrayList<>();

        addCardBack("Classic", "Available to all players");
        addCardBack("Legend", "Achieve Legend rank in Ranked play");
        addCardBack("Fireside", "Play three matches against another player on the same subnet, " +
                "with at last three players total on that subnet (limited period)", "Fireside Friends");
        addCardBack("Winter Veil Wreath", "Participate in Tavern Brawl during 2015 Winter Veil");

        addCardBack("Heroic Naxxramas", "Complete Naxxramas on Heroic mode", "The Fall of Naxxramas");
        addCardBack("Nefarian", "Complete Blackrock Mountain on Heroic mode");
        addCardBack("Staff of Origination", "Complete League of Explorers on Heroic Mode");
        addCardBack("The Blue Portal", "Complete One Night in Karazhan in Heroic mode");
        addCardBack("Diablo", "Acquired from The Dark Wanderer Tavern Brawl, 2017");

        return cardBacks;
    }

    ArrayList<CardBack> getMoney() {
        cardBacks = new ArrayList<>();

        addCardBack("The Grand Tournament", "Pre-order 50 The Grand Tournament packs");
        addCardBack("Molten Core", "Pre-order the Blackrock Mountain adventure");
        addCardBack("Eyes of C'Thun", "Pre-order 50 Whisper of the Gods packs");
        addCardBack("Karazhan Nights", "Awarded by purchasing all wings of One Night in Karazhan within one week of launch");

        addCardBack("Warlords", "Purchase the World of Warcraft: Warlords of Draenor CE/DDE");
        addCardBack("Legacy of the Void", "Purchase Starcraft II: Legacy of the Void CE/DDE");
        addCardBack("Overwatch", "Buy Overwatch Origins or Collector's edition");

        addCardBack("Magni", "Purchase Magni Bronzebeard");
        addCardBack("Alleria", "Purchase Alleria Windrunner");
        addCardBack("Medivh", "Purchase Medivh");
        addCardBack("Tyrande", "Purchase Tyrande Whisperwind");

        return cardBacks;
    }

    private String getSeasonalString(int season) {
        return "Achieve Rank 20 in Ranked Season " + Integer.toString(season) +
                "\n(" + getFormattedSeason(season) + ")";
    }

    private String getFormattedSeason(int season) {
        int year = 2014 + (3 + season) / 12;
        int month = (2 + season) % 12;

        String monthString = new DateFormatSymbols().getMonths()[month];
        return monthString + " " + Integer.toString(year);
    }

    private void addCardBack(int season, String name) {
        cardBacks.add(new CardBack(name, getSeasonalString(season), ""));
    }

    private void addCardBack(String name, String obtain) {
        cardBacks.add(new CardBack(name, obtain, ""));
    }

    private void addCardBack(String name, String obtain, String quest) {
        cardBacks.add(new CardBack(name, obtain, quest));
    }
}
