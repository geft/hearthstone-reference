package com.lifeof843.hearthstonereference.gallery;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.lifeof843.hearthstonereference.GlobalState;
import com.lifeof843.hearthstonereference.R;

public class GalleryViewerPageFragment extends Fragment {

    private final String AUTHORITY = "com.lifeof843.hearthstonereference.utility.ZipFileContentProvider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_gallery_viewer, container, false);

        if (getArguments() != null) {
            initialize();
        }

        return view;
    }

    private void initialize() {
        String cardID = getArguments().getString("id");
        addCardAnimation(cardID);
    }

    private void addCardAnimation(String cardID) {
        String filePath = "card_animated/" + cardID + ".mp4";

        if (doesAnimationExist(filePath)) {
            Uri uri = getURI(filePath);
            displayVideo(uri);
        } else {
            Toast.makeText(
                    view.getContext(),
                    getString(R.string.failed_to_load_animation),
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private Uri getURI(String filePath) {
        return Uri.parse(CONTENT_URI + "/" + filePath);
    }

    private boolean doesAnimationExist(String filePath) {
        ZipResourceFile zipResourceFile = GlobalState.getZipResourceFile();
        AssetFileDescriptor afd = null;

        if (zipResourceFile != null) {
            afd = zipResourceFile.getAssetFileDescriptor(filePath);
        }

        return afd != null;
    }

    private void displayVideo(Uri uri) {
        final VideoView videoView = getVideoView();
        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });
    }

    private VideoView getVideoView() {
        return (VideoView) view.findViewById(R.id.videoView);
    }

    @Override
    public void onPause() {
        getVideoView().stopPlayback();
        super.onPause();
    }
}
