package com.lifeof843.hearthstonereference.gallery;

import android.os.Parcel;
import android.os.Parcelable;

public class CardBack implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CardBack> CREATOR = new Parcelable.Creator<CardBack>() {
        @Override
        public CardBack createFromParcel(Parcel in) {
            return new CardBack(in);
        }

        @Override
        public CardBack[] newArray(int size) {
            return new CardBack[size];
        }
    };
    private String name;
    private String fileName;
    private String obtain;
    private String quest;

    public CardBack(String name, String obtain, String quest) {
        this.name = name;
        this.fileName = formatName(name);
        this.obtain = obtain;
        this.quest = quest;
    }

    protected CardBack(Parcel in) {
        name = in.readString();
        fileName = in.readString();
        obtain = in.readString();
        quest = in.readString();
    }

    private String formatName(String name) {
        String fileName = name.toLowerCase()
                .replace(" ", "_")
                .replace("-", "")
                .replace("!", "")
                .replace("'", "")
                .replace(",", "");
        return "card_backs/" + fileName + ".png";
    }

    public String getName() {
        return name;
    }

    public String getFileName() {
        return fileName;
    }

    public String getObtain() {
        return obtain;
    }

    public String getQuest() {
        return quest;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(fileName);
        dest.writeString(obtain);
        dest.writeString(quest);
    }
}
