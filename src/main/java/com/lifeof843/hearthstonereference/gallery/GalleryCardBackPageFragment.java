package com.lifeof843.hearthstonereference.gallery;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.utility.FileManager;

public class GalleryCardBackPageFragment extends Fragment {

    protected View view;
    protected FileManager fileManager;
    private CardBack cardBack;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_gallery_card_back_slider, container, false);

        init();
        initScrollView();
        displayContent();

        return view;
    }

    private void init() {
        this.fileManager = new FileManager(view.getContext());

        getBundle();
    }

    protected void getBundle() {
        if (getArguments() != null) {
            cardBack = getArguments().getParcelable("cardBack");
        }
    }

    private void initScrollView() {
        ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        scrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        scrollView.setVerticalScrollBarEnabled(false);
    }

    protected void displayContent() {
        setName(cardBack.getName());
        setImage(cardBack.getFileName());
        setDesc(cardBack.getObtain());
        setQuest(cardBack.getQuest());
    }

    private void setImage(String fileName) {
        Drawable drawable = fileManager.getDrawableFromPath(fileName);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageCardBack);
        imageView.setImageDrawable(drawable);
    }

    private void setName(String name) {
        if (!name.isEmpty()) {
            TextView textView = (TextView) view.findViewById(R.id.textName);
            textView.setText(name);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void setDesc(String obtain) {
        TextView textView = (TextView) view.findViewById(R.id.textDesc);
        textView.setText(obtain);
    }

    private void setQuest(String quest) {
        if (!quest.isEmpty()) {
            TextView textView = (TextView) view.findViewById(R.id.textQuest);
            textView.setText("Quest: " + quest);
        }
    }
}
