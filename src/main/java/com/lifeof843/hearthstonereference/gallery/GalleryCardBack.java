package com.lifeof843.hearthstonereference.gallery;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifeof843.hearthstonereference.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GalleryCardBack extends FragmentActivity {

    int numPages;
    private ArrayList<CardBack> cardBacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_card_back);

        if (savedInstanceState != null) {
            cardBacks = savedInstanceState.getParcelableArrayList("cardBacks");
        }

        initialize();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        setFadeAnimation();
    }

    private void setFadeAnimation() {
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initialize() {
        cardBacks = getIntent().getExtras().getParcelableArrayList("cardBacks");

        if (cardBacks != null) {
            numPages = cardBacks.size();
        } else {
            numPages = 0;
        }
        setPagerAdapter();
    }

    private void setPagerAdapter() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter adapter = new CardBackPagerPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    private Fragment getPageWithPosition(int position, GalleryCardBackPageFragment page) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("cardBack", cardBacks.get(position));
        page.setArguments(bundle);

        return page;
    }

    private class CardBackPagerPagerAdapter extends FragmentStatePagerAdapter {
        public CardBackPagerPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            GalleryCardBackPageFragment page = new GalleryCardBackPageFragment();
            return getPageWithPosition(position, page);
        }

        @Override
        public int getCount() {
            return numPages;
        }

    }
}
