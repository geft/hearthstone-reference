package com.lifeof843.hearthstonereference;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Gerry on 29/05/2016.
 */
public class Utils {

    public static boolean isNumeric(String s) {
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    public static String removeLastCharacterIfNumeric(String cardName) {
        if (cardName.length() > 1) {
            String lastChar = cardName.substring(cardName.length() - 1);

            if (isNumeric(lastChar)) {
                cardName = cardName.substring(0, cardName.length() - 1);
            }
        }

        return cardName;
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(
                context,
                text,
                Toast.LENGTH_SHORT)
                .show();
    }
}
