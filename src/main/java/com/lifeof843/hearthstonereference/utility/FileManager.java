package com.lifeof843.hearthstonereference.utility;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.lifeof843.hearthstonereference.GlobalState;
import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.cards.AllCardDatabase;
import com.lifeof843.hearthstonereference.cards.CardSound;
import com.lifeof843.hearthstonereference.decks.DeckDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class FileManager {

    private Context context;
    private AssetManager assetManager;
    private ZipResourceFile zipResourceFile;

    public FileManager(Context context) {

        this.context = context;
        this.assetManager = context.getResources().getAssets();
        this.zipResourceFile = GlobalState.getZipResourceFile();
    }

    public Drawable getDrawableFromName(String cardName, boolean isFull) {
        return getDrawableFromPath(getPathFromCardName(cardName, isFull));
    }

    public Drawable getDrawableFromPath(String path) {
        InputStream inputStream = null;

        if (zipResourceFile != null) {
            try {
                inputStream = zipResourceFile.getInputStream(path);
            } catch (IOException e) {
                Log.e(this.toString(), "Error loading " + path);
            }
        }

        return getDrawableFromStream(inputStream);
    }

    private Drawable getDrawableFromStream(InputStream inputStream) {
        try {
            return Drawable.createFromStream(inputStream, null);
        } catch (OutOfMemoryError e) {
            Toast.makeText(context,
                    context.getString(R.string.error_out_of_memory),
                    Toast.LENGTH_SHORT).show();
        }

        return new ColorDrawable(Color.TRANSPARENT);
    }

    public Drawable GetDeckPortrait(String deckName) {
        DeckDatabase database = DeckDatabase.getInstance(context);
        String playerClass = database.getPlayerClassFromDeck(deckName).toLowerCase();
        InputStream inputStream = getPortraitStream(playerClass);

        return getDrawableFromStream(inputStream);
    }

    private InputStream getPortraitStream(String playerClass) {
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(FormatManager.getPortraitPath(playerClass));
        } catch (IOException e) {
            Log.e(this.toString(), "Error loading portrait " + playerClass);
        }
        return inputStream;
    }

    public String getPathFromCardName(String cardName, boolean isFull) {
        cardName = FormatManager.formatCardName(cardName);
        if (isFull) {
            return FormatManager.getImagePath(cardName);
        } else {
            return FormatManager.getImageCropPath(cardName);
        }
    }

    public List<CardSound> getAudioList(String cardName) {
        List<CardSound> list = new ArrayList<>();
        String cardId = AllCardDatabase.getInstance(context).getCardId(cardName);
        String audioPath = "card_sounds/" + cardId + "_";

        if (!cardId.isEmpty()) {
            TreeSet<String> zipEntries = GlobalState.getZipEntries();
            SortedSet<String> sortedSet = zipEntries.subSet(audioPath, audioPath + "\uffff");

            if (sortedSet.size() > 0) {
                for (String path : sortedSet) {
                    CardSound cardSound = createSound(path);

                    if (cardSound != null) {
                        list.add(cardSound);
                    }
                }
            }
        }

        return list;
    }

    private CardSound createSound(String path) {
        CardSound cardSound = null;

        if (zipResourceFile != null) {
            AssetFileDescriptor afd = zipResourceFile.getAssetFileDescriptor(path);

            if (afd != null) {
                cardSound = new CardSound(path, afd);
            }
        }

        return cardSound;
    }
}
