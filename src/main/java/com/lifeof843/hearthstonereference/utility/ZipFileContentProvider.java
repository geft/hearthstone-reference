package com.lifeof843.hearthstonereference.utility;

import com.android.vending.expansion.zipfile.APEZProvider;

public class ZipFileContentProvider extends APEZProvider {
    @Override
    public String getAuthority() {
        return "com.lifeof843.hearthstonereference.utility.ZipFileContentProvider";
    }
}
