package com.lifeof843.hearthstonereference.utility;

public class FormatManager {

    public static String formatCardName(String cardName) {
        return cardName.toLowerCase()
                .replaceAll("(: )|[: ]", "-")
                .replaceAll("[,.'!]", "");
    }

    public static String getImageCropPath(String cardName) {
        return "card_images_crop/" + cardName + ".png";
    }

    public static String getImagePath(String cardName) {
        return "card_images/" + cardName + ".png";
    }

    public static String getPortraitPath(String playerClass) {
        return "portraits/" + playerClass + ".png";
    }

    public static String getCreditsPath(String name) {
        return "credits_images/" + name + ".png";
    }
}
