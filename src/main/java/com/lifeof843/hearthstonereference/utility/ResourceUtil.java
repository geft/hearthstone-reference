package com.lifeof843.hearthstonereference.utility;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;

/**
 * Created by Gerry on 13/11/2016.
 */

public class ResourceUtil {

    public static Drawable getDrawable(Context context, @DrawableRes int drawableRes) {
        return ContextCompat.getDrawable(context, drawableRes);
    }
}
