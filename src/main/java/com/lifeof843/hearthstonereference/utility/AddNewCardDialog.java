package com.lifeof843.hearthstonereference.utility;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.lifeof843.hearthstonereference.R;
import com.lifeof843.hearthstonereference.decks.DeckBuilder;
import com.lifeof843.hearthstonereference.decks.DeckDatabase;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddNewCardDialog extends Activity {

    private String cardName;
    private String deckName;
    private DeckBuilder deckBuilder;
    private Spinner spinner;
    private Button button;
    private RadioButton radio1, radio2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_card_dialog);

        setBackgroundTransparent();

        if (savedInstanceState == null) {
            cardName = getIntent().getStringExtra("cardName");
            deckName = getIntent().getStringExtra("deckName");
        } else {
            cardName = savedInstanceState.getString("cardName");
            deckName = savedInstanceState.getString("deckName");
        }

        deckBuilder = new DeckBuilder(this, deckName, cardName);
        initLayout();
    }

    private void setBackgroundTransparent() {
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    private void initLayout() {
        assignViews();
        setTitleToCardName(cardName);
        setLegendaryCountConstraint();
        initSpinnerDeckList();
        initAddCardButton();
    }

    private void assignViews() {
        spinner = (Spinner) findViewById(R.id.spinnerDecks);
        button = (Button) findViewById(R.id.buttonAddCard);
        radio1 = (RadioButton) findViewById(R.id.radioCount1);
        radio2 = (RadioButton) findViewById(R.id.radioCount2);
    }

    private void setTitleToCardName(String cardName) {
        ((TextView) findViewById(R.id.textTitle)).setText(cardName);
    }

    private void setLegendaryCountConstraint() {
        if (deckBuilder.isCardLegendary(cardName)) {
            radio2.setEnabled(false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("cardName", cardName);
        outState.putString("deckName", deckName);

        super.onSaveInstanceState(outState);
    }

    private void initAddCardButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinner.getAdapter() == null) {
                    finish();
                } else {
                    String deckName = spinner.getSelectedItem().toString();
                    int count = getSelectedCardCount();
                    deckBuilder.addCardToDeck(deckName, cardName, count);
                    finish();
                }
            }
        });
    }

    private int getSelectedCardCount() {
        int count;
        if (radio1.isChecked()) {
            count = 1;
        } else {
            count = 2;
        }
        return count;
    }

    private void initSpinnerDeckList() {
        ArrayList<String> decks = new ArrayList<>();
        String playerClass = deckBuilder.getPlayerClass();

        if (deckName == null) {
            decks = getValidDecks(playerClass);
        } else if (deckBuilder.isCardUsableInDeck(playerClass, deckName)) {
            decks.add(deckName);
        }

        if (decks.isEmpty()) {
            button.setText(getString(R.string.add_new_card_dialog_empty));
        } else {
            SpinnerAdapter adapter = new CustomSpinnerAdapter(
                    this, R.layout.spinner_dropdown, R.id.textView, decks);
            spinner.setAdapter(adapter);
        }
    }

    private ArrayList<String> getValidDecks(String playerClass) {
        DeckDatabase deckDatabase = DeckDatabase.getInstance(this);
        ArrayList<String> allDecks = deckDatabase.GetListOfDecks();
        ArrayList<String> decks = new ArrayList<>();

        for (String deckName : allDecks) {
            if (deckBuilder.isCardUsableInDeck(playerClass, deckName)) {
                decks.add(deckName);
            }
        }
        return decks;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
